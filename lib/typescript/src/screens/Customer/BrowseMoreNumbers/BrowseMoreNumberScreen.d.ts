import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { FC } from 'react';
import type { CustomerNavigatorParamList } from '../../../navigator';
import type { Msisdn, Order } from '@du-greenfield/du-commons';
import type { MicroAppsProps } from '@du-greenfield/du-microapps-core';
export declare type BrowseMoreNumberScreenProps = NativeStackScreenProps<CustomerNavigatorParamList, 'BrowseMoreNumberScreen'> & MicroAppsProps & {
    numberSelectionComplete: (order: Order, selectedMsisdn: Msisdn) => void;
};
declare const BrowseMoreNumberScreen: FC<BrowseMoreNumberScreenProps>;
export default BrowseMoreNumberScreen;
