/// <reference types="react" />
declare type AmountDockItemProp = {
    title: string;
    titleValue: string;
    caption?: string;
    captionValue: string;
};
export declare const AmountDockItem: (props: AmountDockItemProp) => JSX.Element;
export {};
