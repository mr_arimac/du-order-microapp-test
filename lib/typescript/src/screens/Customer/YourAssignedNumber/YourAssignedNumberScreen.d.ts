import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { FC } from 'react';
import type { CustomerNavigatorParamList } from '../../../navigator';
import type { Msisdn, Order } from '@du-greenfield/du-commons';
import type { MicroAppsProps } from '@du-greenfield/du-microapps-core';
export declare type CustomerYourAssignedNumberScreenProps = NativeStackScreenProps<CustomerNavigatorParamList, 'YourAssignedNumberScreen'> & MicroAppsProps & {
    numberSelectionComplete: (order: Order, selectedMsisdn: Msisdn) => void;
    onBrowseMoreTap: (isEsim: boolean | undefined) => void;
};
declare const CustomerYourAssignedNumberScreen: FC<CustomerYourAssignedNumberScreenProps>;
export default CustomerYourAssignedNumberScreen;
