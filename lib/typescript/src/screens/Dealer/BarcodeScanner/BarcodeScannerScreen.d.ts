import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { FC } from 'react';
import type { DealerNavigatorParamList } from '../../../navigator';
export declare type BarcodeScannerScreenScreenProps = NativeStackScreenProps<DealerNavigatorParamList, 'BarcodeScannerScreen'> & {
    onBarcodeScanned: (barcode: string, simScanned: boolean) => void;
    gotoDashboardClicked: () => void;
};
declare const BarcodeScannerScreen: FC<BarcodeScannerScreenScreenProps>;
export default BarcodeScannerScreen;
