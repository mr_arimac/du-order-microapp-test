import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { FC } from 'react';
import type { DealerNavigatorParamList } from '../../../navigator';
import type { Msisdn, Order } from '@du-greenfield/du-commons';
export declare type DealerYourAssignedNumberScreenProps = NativeStackScreenProps<DealerNavigatorParamList, 'YourAssignedNumberScreen'> & {
    onBrowseMoreTap: (isEsim: boolean | undefined) => void;
    numberSelectionComplete: (order: Order, msisdn: Msisdn) => void;
    gotoDashboardClicked: () => void;
};
declare const DealerYourAssignedNumberScreen: FC<DealerYourAssignedNumberScreenProps>;
export default DealerYourAssignedNumberScreen;
