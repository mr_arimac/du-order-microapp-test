import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { FC } from 'react';
import type { DealerNavigatorParamList } from '../../../navigator';
export declare type ActivatingSIMScreenProps = NativeStackScreenProps<DealerNavigatorParamList, 'ActivatingSIMScreen'> & {
    params: any;
    cancel: () => void;
    onSimActivateSuccess: (value: any) => void;
    gotoDashboardClicked: () => void;
};
declare const ActivatingSIMScreen: FC<ActivatingSIMScreenProps>;
export default ActivatingSIMScreen;
