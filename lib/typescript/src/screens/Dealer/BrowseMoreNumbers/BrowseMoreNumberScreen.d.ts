import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { FC } from 'react';
import type { CustomerNavigatorParamList } from '../../../navigator';
import type { Msisdn, Order } from '@du-greenfield/du-commons';
export declare type BrowseMoreNumberScreenProps = NativeStackScreenProps<CustomerNavigatorParamList, 'BrowseMoreNumberScreen'> & {
    params: any;
    numberSelectionComplete: (order: Order, selectedMsisdn: Msisdn) => void;
    gotoDashboardClicked: () => void;
};
declare const BrowseMoreNumberScreen: FC<BrowseMoreNumberScreenProps>;
export default BrowseMoreNumberScreen;
