import { FC } from 'react';
export declare type EnterICCIDManuallyModalProps = {
    isVisible?: boolean;
    onPressClose?: () => void;
    verifyAndContinue?: (iccid: any) => void;
};
declare const EnterICCIDManuallyModal: FC<EnterICCIDManuallyModalProps>;
export default EnterICCIDManuallyModal;
