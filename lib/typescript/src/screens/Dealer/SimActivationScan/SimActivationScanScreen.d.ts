import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { FC } from 'react';
import type { DealerNavigatorParamList } from '../../../navigator';
export declare type DealerSimActivationScreenProps = NativeStackScreenProps<DealerNavigatorParamList, 'SimActivationScanScreen'> & {
    onBarcodeButtonTap: () => void;
    gotoDashboardClicked: () => void;
};
declare const SimActivationScanScreen: FC<DealerSimActivationScreenProps>;
export default SimActivationScanScreen;
