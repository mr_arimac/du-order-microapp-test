import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { FC } from 'react';
import type { DealerNavigatorParamList } from '../../../navigator';
export declare type SimCapturedScreenProps = NativeStackScreenProps<DealerNavigatorParamList, 'SimCapturedScreen'> & {
    params: any;
    onRescanTap: () => void;
    onSimActivateTap: (barcode: string | undefined) => void;
    gotoDashboardClicked: () => void;
};
declare const SimCapturedScreen: FC<SimCapturedScreenProps>;
export default SimCapturedScreen;
