/// <reference types="react" />
import type { Msisdn, Order } from '@du-greenfield/du-commons';
import type { MicroAppsProps } from '@du-greenfield/du-microapps-core';
export declare type DealerNavigatorParamList = {
    YourAssignedNumberScreen: undefined;
    BrowseMoreNumberScreen: undefined;
    SimActivationScanScreen: undefined;
    ActivatingSIMScreen: undefined;
    BarcodeScannerScreen: undefined;
    SimCapturedScreen: undefined;
};
export declare enum DuOrderDealerInitialScreen {
    YOUR_ASSIGNED_NUMBER = "YourAssignedNumberScreen",
    BROWSE_MORE_NUMBERS = "BrowseMoreNumberScreen",
    SIM_ACTIVATION_SCAN = "SimActivationScanScreen",
    BARCODE_SCANNER = "BarcodeScannerScreen",
    ACTIVATING_SIM = "ActivatingSIMScreen",
    SIM_CAPTURED = "SimCapturedScreen"
}
declare type DealerAppStackProps = MicroAppsProps & {
    onBrowseMoreTap: (isEsim: boolean | undefined) => void;
    numberSelectionComplete: (order: Order, selectedMsisdn: Msisdn) => void;
    initalScreen?: DuOrderDealerInitialScreen;
    onBarcodeButtonTap: () => void;
    gotoDashboardClicked: () => void;
    onBarcodeScanned: (barcode: string, simScanned: boolean) => void;
    onRescanTap: () => void;
    onSimActivateTap: (barcode: string | undefined) => void;
    onSimActivateSuccess: (value: any) => void;
    customerId?: string;
    params: any;
};
export declare const DealerAppStack: (props: DealerAppStackProps) => JSX.Element;
export {};
