/// <reference types="react" />
import type { Msisdn, Order } from '@du-greenfield/du-commons';
import type { MicroAppsProps } from '@du-greenfield/du-microapps-core';
export declare type CustomerNavigatorParamList = {
    YourAssignedNumberScreen: undefined;
    BrowseMoreNumberScreen: undefined;
};
export declare enum CustomerInitialScreen {
    YOUR_ASSIGNED_NUMBER = "YourAssignedNumberScreen",
    BROWSE_MORE_NUMBERS = "BrowseMoreNumberScreen"
}
declare type CustomerAppStackProps = MicroAppsProps & {
    onBrowseMoreTap: (isEsim: boolean | undefined) => void;
    numberSelectionComplete: (order: Order, selectedMsisdn: Msisdn) => void;
    initialScreen?: CustomerInitialScreen;
    params: any;
};
export declare const CustomerAppStack: (stackProps: CustomerAppStackProps) => JSX.Element;
export {};
