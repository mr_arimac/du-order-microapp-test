import React from 'react';
import { InitialStateType } from './context-types';
export declare const OrderAppContext: React.Context<{
    state: InitialStateType;
    dispatch: React.Dispatch<any>;
}>;
export declare function getAppState(): Promise<InitialStateType>;
export declare function setAppState(state: InitialStateType): Promise<void>;
export declare const mainReducer: (action: {
    type: string;
    payload: {};
}) => {
    browseMore: {
        checkedNumbers: {};
    };
};
