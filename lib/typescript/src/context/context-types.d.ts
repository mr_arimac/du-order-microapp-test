import type { PhoneNumber } from '../utils/gql/models';
export declare type BrowseMoreState = {
    checkedNumbers: PhoneNumber[];
};
export declare type InitialStateType = {
    browseMore: BrowseMoreState;
};
export declare const initialState: InitialStateType;
