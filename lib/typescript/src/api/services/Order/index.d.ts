import type { OrderScreenDynamics } from '../../models';
declare class Eligibility {
    getScreenDynamics(): Promise<OrderScreenDynamics | undefined>;
}
declare const screen: Eligibility;
export default screen;
