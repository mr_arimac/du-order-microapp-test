/// <reference types="react" />
import type { MicroAppContext, MicroAppsProps } from '@du-greenfield/du-microapps-core';
import { CustomerInitialScreen, DuOrderDealerInitialScreen } from './navigator';
import type { PhoneNumber } from './utils/gql/models';
import type { Msisdn, Order } from '@du-greenfield/du-commons';
export { DuOrderDealerInitialScreen, CustomerInitialScreen, PhoneNumber };
export declare let isDealer: boolean;
export declare type DuOrderProps = MicroAppsProps & {
    salesOrder: Order;
    onBrowseMoreTap: (isEsim: boolean | undefined) => void;
    numberSelectionComplete: (order: Order, selectedMsisdn: Msisdn) => void;
    dealerInitialScreen?: DuOrderDealerInitialScreen;
    customerInitialScreen?: CustomerInitialScreen;
    onBarcodeButtonTap: () => void;
    gotoDashboardClicked: () => void;
    onBarcodeScanned: (barcode: string, simScanned: boolean) => void;
    onRescanTap: () => void;
    onSimActivateTap: (barcode: string | undefined) => void;
    onSimActivateSuccess: (value: any) => void;
    params: any;
};
export declare function getMicroAppContext(): MicroAppContext;
export declare function getSalesOrder(): Order;
declare const DuOrder: (props: DuOrderProps) => JSX.Element;
export default DuOrder;
