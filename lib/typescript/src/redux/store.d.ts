declare const _default: import("@reduxjs/toolkit").EnhancedStore<{
    phoneNumberSlice: {
        phoneNumber: string;
        phoneNumbers: never[];
    };
}, import("redux").AnyAction, [import("redux-thunk").ThunkMiddleware<{
    phoneNumberSlice: {
        phoneNumber: string;
        phoneNumbers: never[];
    };
}, import("redux").AnyAction, undefined>]>;
export default _default;
