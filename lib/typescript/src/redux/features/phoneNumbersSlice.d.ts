export declare const phoneNumberSelected: import("@reduxjs/toolkit").ActionCreatorWithPayload<any, string>;
declare const _default: import("redux").Reducer<{
    phoneNumber: string;
    phoneNumbers: never[];
}, import("redux").AnyAction>;
export default _default;
