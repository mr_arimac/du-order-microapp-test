import { AxiosRequestConfig } from '@du-greenfield/du-rest';
export declare function request<T>(config: AxiosRequestConfig): Promise<T | undefined>;
