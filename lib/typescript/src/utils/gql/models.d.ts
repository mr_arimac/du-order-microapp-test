import type { GraphQLResponse } from '@du-greenfield/du-commons';
export interface EligibilityCheckPayload {
    eligibilityCheck: EligibilityCheck;
}
export interface EligibilityCheck {
    errorCodes: ErrorCode[];
    failureReason: string;
    FMSPayload: FMSPayload;
    requestedDate: Date;
    retrievedDate: Date;
    simCapPayload: SimCapPayload;
    status: string;
}
export interface FMSPayload {
    systemDecision: string;
}
export interface ErrorCode {
    code?: string;
    type?: string;
    message: string;
}
export declare enum StepIdentifier {
    KYC = "KYC",
    Courier = "Courier"
}
export interface SimCapPayload {
    linesAtDU: number;
    linesAtGF: number;
    remainingLines: number;
    totalActiveLines: number;
}
export interface EligibilityCheckInput {
    simCapCheck: SimCapCheckInput;
    fraudCheck: FraudCheckInput;
    stepIdentifier: StepIdentifier;
    documentId: ProofIdInput;
    nationality: string;
}
export interface ProofIdInput {
    type: string;
    number: string;
    issuedDate?: string;
    expiryDate?: string;
}
export interface FraudCheckInput {
    transactionId: string;
    firstName: string;
    middleName: string;
    lastName: string;
    segment: string;
    dob: string;
    contactInfo?: ContactInfoInput;
    productType: string;
    localResellerName?: string;
    serviceEndDate?: string;
    IMSI?: string;
    userId: string;
    sourceSystem: string;
    salesChannel?: string;
    companyName?: string;
    registrationDate?: string;
    ESTABCardNumber?: string;
}
export interface ContactInfoInput {
    emailId?: string;
    primaryContactNumber?: string;
    secondaryContactNumber?: string;
    permanentAddress?: string;
    address1?: string;
    address2?: string;
    address3?: string;
    city?: string;
    state?: string;
    postCode?: string;
}
export interface SimCapCheckInput {
    requestedMSISDNList: string[];
    activeMSISDNs?: string;
    source: string;
}
export interface LockPhoneNumberInput {
    quantity: number;
    customerId: string;
    isLockRequired: boolean;
}
export interface LockPhoneNumberPayload {
    lockPhoneNumber: LockPhoneNumber;
}
export interface LockPhoneNumber {
    phoneNumber: PhoneNumber[];
    status: string;
    errorCodes: ErrorCode[];
}
export interface ReleasePhoneNumbersResponcePayload {
    status: string;
    errorCodes: ErrorCode[];
}
export interface PhoneNumber {
    id: string;
    phoneNumber: string;
    reservationDate: Date;
    status: string;
    reservationPeriod: string;
    lockDate: string;
}
export interface ExistingOrderItemResponse extends GraphQLResponse {
    salesOrder: {
        salesOrderId: string;
        orderItems: OrderItem[];
    };
}
export interface OrderItem {
    orderItemId: string;
    offer: {
        isRoot: boolean;
    };
    orderItems: OrderItem[];
}
export interface ProductOffering {
    id: string | undefined;
    characteristics?: Characteristic[];
}
export interface Characteristic {
    name: string;
    value: string;
}
