import type { GraphQLClient } from '@du-greenfield/du-microapps-core';
export declare function gqlClient(): GraphQLClient;
