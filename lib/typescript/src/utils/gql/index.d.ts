import type { ExistingOrderItemResponse, LockPhoneNumber, ReleasePhoneNumbersResponcePayload } from './models';
import type { DocumentNode } from 'graphql';
export declare function lockPhoneNumber(customerId: string, qty: number, excludeNumberIds: string[]): Promise<LockPhoneNumber | undefined>;
export declare function reservePhoneNumber(customerId: string, reserveNumberIds: string[], contextId: string): Promise<any | undefined>;
export declare function releasePhoneNumbers(customerId: string, salesOrderId: string, releasePhoneNumberId: string): Promise<ReleasePhoneNumbersResponcePayload | undefined>;
export declare function addExistingOrderItem(parentSalesOrderId: string, locationId: string, productOfferingId: string): Promise<any | undefined>;
export declare function addExistingOrderItemUnderParent(orderItemId: string, productOfferingId: string, value: string): Promise<ExistingOrderItemResponse | undefined>;
export declare function getSalesOrder(id: string, reloadShopCart: boolean): Promise<any | undefined>;
export declare function updateOrderItems(salesOrderId: string, simCardOrderId: string): Promise<any | undefined>;
export declare function getBssListProductOfferings(): Promise<any | undefined>;
export declare function updateOrderItem(salesOrderId: string, orderItemId: string | undefined, msisdn: string): Promise<any | undefined>;
export declare function submitSalesOrder(input: {
    salesOrderId: string;
    [key: string]: string;
}, gql?: DocumentNode): Promise<any | undefined>;
export declare function getCustomer(id: string, gql: DocumentNode): Promise<any>;
export declare function getListProductOffering(gql: DocumentNode): Promise<any>;
