export declare const LOCK_PHONE_NUMBER: import("graphql").DocumentNode;
export declare const RELEASE_PHONE_NUMBERS: import("graphql").DocumentNode;
export declare const RESERVE_PHONE_NUMBER: import("graphql").DocumentNode;
export declare const ADD_EXISTING_ORDER_ITEM: import("graphql").DocumentNode;
export declare const ADD_EXISTING_ORDER_ITEM_UNDER_PARENT: import("graphql").DocumentNode;
export declare const UPDATE_ORDER_ITEM: import("graphql").DocumentNode;
export declare const SUBMIT_SALES_ORDER: import("graphql").DocumentNode;
export declare const SUBMIT_SALES_ORDER_WITH_BILLING: (fieldName: string) => import("graphql").DocumentNode;
export declare const UPDATE_SALES_ORDER: import("graphql").DocumentNode;
