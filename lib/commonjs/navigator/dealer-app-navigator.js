"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DuOrderDealerInitialScreen = exports.DealerAppStack = void 0;

var _nativeStack = require("@react-navigation/native-stack");

var _react = _interopRequireDefault(require("react"));

var _BrowseMoreNumberScreen = _interopRequireDefault(require("../screens/Dealer/BrowseMoreNumbers/BrowseMoreNumberScreen"));

var _ActivatingSIMScreen = _interopRequireDefault(require("../screens/Dealer/ActivatingSIM/ActivatingSIMScreen"));

var _SimActivationScanScreen = _interopRequireDefault(require("../screens/Dealer/SimActivationScan/SimActivationScanScreen"));

var _YourAssignedNumberScreen = _interopRequireDefault(require("../screens/Dealer/YourAssignedNumber/YourAssignedNumberScreen"));

var _BarcodeScannerScreen = _interopRequireDefault(require("../screens/Dealer/BarcodeScanner/BarcodeScannerScreen"));

var _SimCapturedScreen = _interopRequireDefault(require("../screens/Dealer/SimCapruredScreen/SimCapturedScreen"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

let DuOrderDealerInitialScreen;
exports.DuOrderDealerInitialScreen = DuOrderDealerInitialScreen;

(function (DuOrderDealerInitialScreen) {
  DuOrderDealerInitialScreen["YOUR_ASSIGNED_NUMBER"] = "YourAssignedNumberScreen";
  DuOrderDealerInitialScreen["BROWSE_MORE_NUMBERS"] = "BrowseMoreNumberScreen";
  DuOrderDealerInitialScreen["SIM_ACTIVATION_SCAN"] = "SimActivationScanScreen";
  DuOrderDealerInitialScreen["BARCODE_SCANNER"] = "BarcodeScannerScreen";
  DuOrderDealerInitialScreen["ACTIVATING_SIM"] = "ActivatingSIMScreen";
  DuOrderDealerInitialScreen["SIM_CAPTURED"] = "SimCapturedScreen";
})(DuOrderDealerInitialScreen || (exports.DuOrderDealerInitialScreen = DuOrderDealerInitialScreen = {}));

const Stack = (0, _nativeStack.createNativeStackNavigator)();

const DealerAppStack = props => {
  return /*#__PURE__*/_react.default.createElement(Stack.Navigator, {
    screenListeners: {
      state: e => {
        props.globalEventListener && props.globalEventListener('screen_state_changes', e);
      }
    },
    screenOptions: {
      headerShown: false
    },
    initialRouteName: props.initalScreen
  }, /*#__PURE__*/_react.default.createElement(Stack.Screen, {
    name: "YourAssignedNumberScreen"
  }, screenProps => /*#__PURE__*/_react.default.createElement(_YourAssignedNumberScreen.default, _extends({}, props, screenProps))), /*#__PURE__*/_react.default.createElement(Stack.Screen, {
    name: "BrowseMoreNumberScreen"
  }, screenProps => /*#__PURE__*/_react.default.createElement(_BrowseMoreNumberScreen.default, _extends({}, props, screenProps))), /*#__PURE__*/_react.default.createElement(Stack.Screen, {
    name: "SimActivationScanScreen"
  }, screenProps => /*#__PURE__*/_react.default.createElement(_SimActivationScanScreen.default, _extends({}, props, screenProps))), /*#__PURE__*/_react.default.createElement(Stack.Screen, {
    name: "ActivatingSIMScreen"
  }, screenProps => /*#__PURE__*/_react.default.createElement(_ActivatingSIMScreen.default, _extends({}, props, screenProps, {
    cancel: () => {//do nothing
    }
  }))), /*#__PURE__*/_react.default.createElement(Stack.Screen, {
    name: "BarcodeScannerScreen"
  }, screenProps => /*#__PURE__*/_react.default.createElement(_BarcodeScannerScreen.default, _extends({}, props, screenProps))), /*#__PURE__*/_react.default.createElement(Stack.Screen, {
    name: "SimCapturedScreen"
  }, screenProps => /*#__PURE__*/_react.default.createElement(_SimCapturedScreen.default, _extends({}, props, screenProps))));
};

exports.DealerAppStack = DealerAppStack;
//# sourceMappingURL=dealer-app-navigator.js.map