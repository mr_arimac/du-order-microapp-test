"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CustomerInitialScreen = exports.CustomerAppStack = void 0;

var _nativeStack = require("@react-navigation/native-stack");

var _react = _interopRequireDefault(require("react"));

var _BrowseMoreNumberScreen = _interopRequireDefault(require("../screens/Customer/BrowseMoreNumbers/BrowseMoreNumberScreen"));

var _YourAssignedNumberScreen = _interopRequireDefault(require("../screens/Customer/YourAssignedNumber/YourAssignedNumberScreen"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

let CustomerInitialScreen;
exports.CustomerInitialScreen = CustomerInitialScreen;

(function (CustomerInitialScreen) {
  CustomerInitialScreen["YOUR_ASSIGNED_NUMBER"] = "YourAssignedNumberScreen";
  CustomerInitialScreen["BROWSE_MORE_NUMBERS"] = "BrowseMoreNumberScreen";
})(CustomerInitialScreen || (exports.CustomerInitialScreen = CustomerInitialScreen = {}));

const Stack = (0, _nativeStack.createNativeStackNavigator)();

const CustomerAppStack = stackProps => {
  return /*#__PURE__*/_react.default.createElement(Stack.Navigator, {
    screenListeners: {
      state: e => {
        stackProps.globalEventListener && stackProps.globalEventListener('screen_state_changes', e);
      }
    },
    screenOptions: {
      headerShown: false
    },
    initialRouteName: stackProps.initialScreen
  }, /*#__PURE__*/_react.default.createElement(Stack.Screen, {
    name: "YourAssignedNumberScreen"
  }, screenProps => /*#__PURE__*/_react.default.createElement(_YourAssignedNumberScreen.default, _extends({}, stackProps, screenProps))), /*#__PURE__*/_react.default.createElement(Stack.Group, {
    screenOptions: {
      presentation: 'modal'
    }
  }, /*#__PURE__*/_react.default.createElement(Stack.Screen, {
    name: "BrowseMoreNumberScreen"
  }, screenProps => /*#__PURE__*/_react.default.createElement(_BrowseMoreNumberScreen.default, _extends({}, stackProps, screenProps)))));
};

exports.CustomerAppStack = CustomerAppStack;
//# sourceMappingURL=customer-app-navigator.js.map