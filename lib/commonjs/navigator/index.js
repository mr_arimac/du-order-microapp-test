"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _customerAppNavigator = require("./customer-app-navigator");

Object.keys(_customerAppNavigator).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (key in exports && exports[key] === _customerAppNavigator[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _customerAppNavigator[key];
    }
  });
});

var _dealerAppNavigator = require("./dealer-app-navigator");

Object.keys(_dealerAppNavigator).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (key in exports && exports[key] === _dealerAppNavigator[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _dealerAppNavigator[key];
    }
  });
});
//# sourceMappingURL=index.js.map