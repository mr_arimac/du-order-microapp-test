"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  addZero: true
};
exports.addZero = addZero;

var _gql = require("./gql");

Object.keys(_gql).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _gql[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _gql[key];
    }
  });
});

var _storage = require("./storage");

Object.keys(_storage).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  if (key in exports && exports[key] === _storage[key]) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function () {
      return _storage[key];
    }
  });
});

function addZero(elemet, size) {
  var s = String(elemet);

  while (s.length < (size || 2)) {
    s = '0' + s;
  }

  return s;
}
//# sourceMappingURL=index.js.map