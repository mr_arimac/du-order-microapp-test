"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.request = request;

var _duRest = require("@du-greenfield/du-rest");

async function request(config) {
  return new Promise(resolve => {
    (0, _duRest.duRequest)(config).then(value => {
      console.log('data', value.data);
      return resolve(value.data);
    }).catch(e => {
      console.log(e);
      return resolve(undefined);
    });
  });
}
//# sourceMappingURL=api_controller.js.map