"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.clear = clear;
exports.load = load;
exports.loadString = loadString;
exports.remove = remove;
exports.save = save;
exports.saveString = saveString;

var _asyncStorage = _interopRequireDefault(require("@react-native-async-storage/async-storage"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Loads a string from storage.
 *
 * @param key The key to fetch.
 */
async function loadString(key) {
  try {
    return await _asyncStorage.default.getItem(key);
  } catch {
    // not sure why this would fail... even reading the RN docs I'm unclear
    return null;
  }
}
/**
 * Saves a string to storage.
 *
 * @param key The key to fetch.
 * @param value The value to store.
 */


async function saveString(key, value) {
  try {
    await _asyncStorage.default.setItem(key, value);
    return true;
  } catch {
    return false;
  }
}
/**
 * Loads something from storage and runs it thru JSON.parse.
 *
 * @param key The key to fetch.
 */


async function load(key) {
  try {
    var _await$AsyncStorage$g;

    const almostThere = (_await$AsyncStorage$g = await _asyncStorage.default.getItem(key)) !== null && _await$AsyncStorage$g !== void 0 ? _await$AsyncStorage$g : '';
    return JSON.parse(almostThere);
  } catch {
    return null;
  }
}
/**
 * Saves an object to storage.
 *
 * @param key The key to fetch.
 * @param value The value to store.
 */


async function save(key, value) {
  try {
    await _asyncStorage.default.setItem(key, JSON.stringify(value));
    return true;
  } catch {
    return false;
  }
}
/**
 * Removes something from storage.
 *
 * @param key The key to kill.
 */


async function remove(key) {
  try {
    await _asyncStorage.default.removeItem(key);
  } catch {}
}
/**
 * Burn it all to the ground.
 */


async function clear() {
  try {
    await _asyncStorage.default.clear();
  } catch {}
}
//# sourceMappingURL=storage.js.map