"use strict";

var _asyncStorage = _interopRequireDefault(require("@react-native-async-storage/async-storage"));

var _storage = require("./storage");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// fixtures
const VALUE_OBJECT = {
  x: 1
};
const VALUE_STRING = JSON.stringify(VALUE_OBJECT);
beforeEach(() => _asyncStorage.default.getItem.mockReturnValue(Promise.resolve(VALUE_STRING)));
afterEach(() => jest.clearAllMocks());
test('load', async () => {
  const value = await (0, _storage.load)('something');
  expect(value).toEqual(JSON.parse(VALUE_STRING));
});
test('loadString', async () => {
  const value = await (0, _storage.loadString)('something');
  expect(value).toEqual(VALUE_STRING);
});
test('save', async () => {
  await (0, _storage.save)('something', VALUE_OBJECT);
  expect(_asyncStorage.default.setItem).toHaveBeenCalledWith('something', VALUE_STRING);
});
test('saveString', async () => {
  await (0, _storage.saveString)('something', VALUE_STRING);
  expect(_asyncStorage.default.setItem).toHaveBeenCalledWith('something', VALUE_STRING);
});
test('remove', async () => {
  await (0, _storage.remove)('something');
  expect(_asyncStorage.default.removeItem).toHaveBeenCalledWith('something');
});
test('clear', async () => {
  await (0, _storage.clear)();
  expect(_asyncStorage.default.clear).toHaveBeenCalledWith();
});
//# sourceMappingURL=storage.test.js.map