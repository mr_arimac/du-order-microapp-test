"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StepIdentifier = void 0;
let StepIdentifier;
exports.StepIdentifier = StepIdentifier;

(function (StepIdentifier) {
  StepIdentifier["KYC"] = "KYC";
  StepIdentifier["Courier"] = "Courier";
})(StepIdentifier || (exports.StepIdentifier = StepIdentifier = {}));
//# sourceMappingURL=models.js.map