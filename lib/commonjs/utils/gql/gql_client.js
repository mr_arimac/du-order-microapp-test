"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.gqlClient = gqlClient;

var _ = require("../..");

function gqlClient() {
  return (0, _.getMicroAppContext)().graphQLClinet();
}
//# sourceMappingURL=gql_client.js.map