"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UPDATE_SALES_ORDER = exports.UPDATE_ORDER_ITEM = exports.SUBMIT_SALES_ORDER_WITH_BILLING = exports.SUBMIT_SALES_ORDER = exports.RESERVE_PHONE_NUMBER = exports.RELEASE_PHONE_NUMBERS = exports.LOCK_PHONE_NUMBER = exports.ADD_EXISTING_ORDER_ITEM_UNDER_PARENT = exports.ADD_EXISTING_ORDER_ITEM = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const LOCK_PHONE_NUMBER = (0, _graphqlTag.default)`
  mutation LockPhoneNumber($input: PhoneNumberLockInput!) {
    lockPhoneNumber(input: $input) {
      phoneNumber {
        id
        phoneNumber
        reservationDate
        status
        reservationPeriod
        lockDate
      }
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;
exports.LOCK_PHONE_NUMBER = LOCK_PHONE_NUMBER;
const RELEASE_PHONE_NUMBERS = (0, _graphqlTag.default)`
  mutation releasePhoneNumbers($input: PhoneNumbersReleaseInput!) {
    releasePhoneNumbers(input: $input) {
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;
exports.RELEASE_PHONE_NUMBERS = RELEASE_PHONE_NUMBERS;
const RESERVE_PHONE_NUMBER = (0, _graphqlTag.default)`
  mutation ReservePhoneNumber($input: PhoneNumberReservationInput!) {
    reservePhoneNumber(input: $input) {
      phoneNumber {
        id
        phoneNumber
        reservationDate
        status
        reservationPeriod
      }
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;
exports.RESERVE_PHONE_NUMBER = RESERVE_PHONE_NUMBER;
const ADD_EXISTING_ORDER_ITEM = (0, _graphqlTag.default)`
  mutation addExistingOrderItem($input: ExistingOrderItemInput!) {
    addExistingOrderItem(input: $input) {
      salesOrder {
        salesOrderId
      }
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;
exports.ADD_EXISTING_ORDER_ITEM = ADD_EXISTING_ORDER_ITEM;
const ADD_EXISTING_ORDER_ITEM_UNDER_PARENT = (0, _graphqlTag.default)`
  mutation addOrderItemUnderParent($input: OrderItemUnderParentInput!) {
    addOrderItemUnderParent(input: $input) {
      salesOrder {
        salesOrderId
        orderItems {
          orderItemId
          offer {
            isRoot
          }
          orderItems {
            orderItemId
            offer {
              name
            }
          }
        }
      }
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`; //change

exports.ADD_EXISTING_ORDER_ITEM_UNDER_PARENT = ADD_EXISTING_ORDER_ITEM_UNDER_PARENT;
const UPDATE_ORDER_ITEM = (0, _graphqlTag.default)`
  mutation updateOrderItems($input: OrderItemInput!) {
    updateOrderItems(input: $input) {
      salesOrder {
        salesOrderId
      }
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;
exports.UPDATE_ORDER_ITEM = UPDATE_ORDER_ITEM;
const SUBMIT_SALES_ORDER = (0, _graphqlTag.default)`
  mutation submitSalesOrder($input: SubmitSalesOrderInput!) {
    submitSalesOrder(input: $input) {
      salesOrder {
        salesOrderId
      }
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;
exports.SUBMIT_SALES_ORDER = SUBMIT_SALES_ORDER;

const SUBMIT_SALES_ORDER_WITH_BILLING = fieldName => (0, _graphqlTag.default)`
  mutation submitSalesOrder($input: SubmitSalesOrderInput!) {
    submitSalesOrder(input: $input) {
      salesOrder {
        salesOrderId
        ${fieldName}
      }
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;

exports.SUBMIT_SALES_ORDER_WITH_BILLING = SUBMIT_SALES_ORDER_WITH_BILLING;
const UPDATE_SALES_ORDER = (0, _graphqlTag.default)`
  mutation updateOrderItems($input: OrderItemInput!) {
    updateOrderItems(input: $input) {
      salesOrder {
        salesOrderId
      }
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;
exports.UPDATE_SALES_ORDER = UPDATE_SALES_ORDER;
//# sourceMappingURL=mutations.js.map