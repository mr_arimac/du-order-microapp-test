"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.GET_SALSE_ORDER = exports.ELIGIBILITY_CHECK = exports.BSS_LIST_PRODUCT_OFFERINGS = exports.BSS_LIST_PRODUCT_OFFERING = exports.BSS_GET_CUSTOMER = void 0;

var _graphqlTag = _interopRequireDefault(require("graphql-tag"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ELIGIBILITY_CHECK = (0, _graphqlTag.default)`
  query EligibilityCheck($input: EligibilityCheckInput!) {
    eligibilityCheck(input: $input) {
      errorCodes {
        errorCode
        errormessage
      }
      failureReason
      FMSPayload {
        systemDecision
      }
      requestedDate
      retrievedDate
      simCapPayload {
        linesAtDU
        linesAtGF
        remainingLines
        totalActiveLines
      }
      status
    }
  }
`;
exports.ELIGIBILITY_CHECK = ELIGIBILITY_CHECK;
const GET_SALSE_ORDER = (0, _graphqlTag.default)`
  query BssGetSalesOrder($input: BssGetSalesOrderInput!) {
    bssGetSalesOrder(input: $input) {
      orderItems {
        id
        chars
      }
      errorCodes {
        errorCode
        errormessage
      }
      status
    }
  }
`;
exports.GET_SALSE_ORDER = GET_SALSE_ORDER;
const BSS_LIST_PRODUCT_OFFERINGS = (0, _graphqlTag.default)`
  query bssListProductOfferings {
    bssListProductOfferings
      @filter(filters: ["isRoot=false", "name.eq=Phone number"]) {
      id
    }
  }
`;
exports.BSS_LIST_PRODUCT_OFFERINGS = BSS_LIST_PRODUCT_OFFERINGS;
const BSS_GET_CUSTOMER = (0, _graphqlTag.default)`
  query bssGetCustomer($id: String!) {
    bssGetCustomer(id: $id) {
      billingAccounts {
        id
      }
    }
  }
`;
exports.BSS_GET_CUSTOMER = BSS_GET_CUSTOMER;
const BSS_LIST_PRODUCT_OFFERING = (0, _graphqlTag.default)`
  query bssListProductOfferings {
    bssListProductOfferings
      @filter(filters: ["isRoot=true", "name.eq=In Store"]) {
      id
    }
  }
`;
exports.BSS_LIST_PRODUCT_OFFERING = BSS_LIST_PRODUCT_OFFERING;
//# sourceMappingURL=queries.js.map