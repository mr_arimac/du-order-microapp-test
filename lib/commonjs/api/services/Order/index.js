"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _duRest = require("@du-greenfield/du-rest");

class Eligibility {
  async getScreenDynamics() {
    console.log('Eligibility getScreenDynamics ======================>');
    return new Promise(resolve => {
      (0, _duRest.duGet)('https://firestore.googleapis.com/v1/projects/gf-du-customer-mobile-app-dev/databases/(default)/documents/microapp/005').then(value => {
        return resolve(value.data);
      }).catch(e => {
        console.log(e);
        return resolve(undefined);
      });
    });
  }

}

const screen = new Eligibility();
var _default = screen;
exports.default = _default;
//# sourceMappingURL=index.js.map