"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _duUiToolkit = require("@du-greenfield/du-ui-toolkit");

var _reactNativeMaskText = require("react-native-mask-text");

var _react = _interopRequireWildcard(require("react"));

var _reactNative = require("react-native");

var _duAnalyticsPluginCore = require("@du-greenfield/du-analytics-plugin-core");

var _duFirebaseAnalytics = _interopRequireDefault(require("@du-greenfield/du-firebase-analytics"));

var _reactNativeSafeAreaContext = require("react-native-safe-area-context");

var _utils = require("../../../utils");

var _ = require("../../../");

var _reactRedux = require("react-redux");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const BrowseMoreNumberScreen = _ref => {
  var _phoneNumberState$pho;

  let {
    navigation,
    numberSelectionComplete
  } = _ref;
  const timeout = 720;
  const [overlayVisibility, setOverlayVisibility] = (0, _react.useState)(false);
  const intervalRef = (0, _react.useRef)(null);
  const [seconds, setSeconds] = (0, _react.useState)(timeout);
  const [expired, setExpired] = (0, _react.useState)(false);
  const safeAreaInsets = (0, _reactNativeSafeAreaContext.useSafeAreaInsets)();
  const [showSheet, setShowSheet] = (0, _react.useState)(false);
  const [checkedPhoneNumber, setCheckedPhoneNumber] = (0, _react.useState)({
    id: '',
    number: ''
  });
  const phoneNumberState = (0, _reactRedux.useSelector)(state => state.phoneNumberSlice);
  const [phoneNumbersPayload, setPhoneNumbersPayload] = (0, _react.useState)(undefined);

  async function fetchNumbers() {
    const response = await (0, _utils.lockPhoneNumber)((0, _.getSalesOrder)().customer.id, 5, []);

    if ((response === null || response === void 0 ? void 0 : response.status) === 'SUCCESS') {
      setShowSheet(response.phoneNumber.length === 0);
      setPhoneNumbersPayload(response);
      setCheckedPhoneNumber({
        id: '',
        number: ''
      });
    }
  }

  (0, _react.useEffect)(() => {
    fetchNumbers(); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  (0, _react.useEffect)(() => {
    if (seconds <= 0) {
      fetchNumbers();
      setExpired(true);
      setTimeout(() => {
        refreshNumbers();
      }, 500);
      return;
    }

    const timer = setTimeout(() => setSeconds(seconds - 1), 1000);
    intervalRef.current = timer;
    return () => {
      clearInterval(intervalRef.current);
    }; // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [seconds]); // const props: DuMobileNumberProps = {
  //   incomplete: false,
  //   disableClick: true,
  // };

  function refreshNumbers() {
    setPhoneNumbersPayload(undefined);
    setExpired(false);
    setSeconds(timeout);
    fetchNumbers();
  }

  function handleNumberSelection(id) {
    for (let number of phoneNumbersPayload === null || phoneNumbersPayload === void 0 ? void 0 : phoneNumbersPayload.phoneNumber) {
      if (id === number.id) {
        setCheckedPhoneNumber({
          number: number.phoneNumber,
          id: number.id
        }); // dispatch(
        //   phoneNumberSelected({
        //     phoneNumber: number.phoneNumber,
        //   })
        // );
      }
    }
  }

  const headerProps = {
    left: 'back',
    leftPressed: () => {
      _reactNative.Platform.OS === 'android' && _reactNative.StatusBar.setBackgroundColor('white');

      if (_reactNative.Platform.OS === 'android') {
        _reactNative.StatusBar.setBarStyle('dark-content', true);
      }

      if (navigation.canGoBack()) {
        navigation.goBack();
      }
    },
    statusBar: {
      barStyle: 'dark-content',
      backgroundColor: 'white'
    },
    title: 'Browse more number',
    background: 'white'
  };

  const _getTime = _seconds => {
    _seconds = Number(_seconds);
    var m = Math.floor(_seconds % 3600 / 60);
    var s = Math.floor(_seconds % 3600 % 60);
    return `${(0, _utils.addZero)(m, 2)}:${(0, _utils.addZero)(s, 2)}`;
  };

  const sheetProps = {
    isVisible: showSheet,
    onClose: () => setShowSheet(false),
    standard: {
      title: "We're sorry, there are no available numbers",
      body: 'We’re sorry, there are no available numbers\n  Please try again later.',
      icon: {
        iconName: 'search-off',
        iconColor: '#BA0023',
        artWorkHeight: 36,
        artWorkWidth: 36
      },
      buttons: [{
        text: 'Try again',
        type: 'primary',
        onPress: () => {
          setShowSheet(false);
          refreshNumbers();
        }
      }]
    }
  };
  const analyticLogScreenView = (0, _react.useCallback)(() => {
    let analytics = new _duFirebaseAnalytics.default();
    let tagBuilder = new _duAnalyticsPluginCore.DuAnalyticsTagBuilder();
    let params = tagBuilder.pageName('Browse more free numbers').journeyName('GF Explore').pageChannel('Customer App').previousScreenName('Assign random number').screenType('pdp').screenHierarchy('Homepage: pdp: Family plan: Assign free number: Browse more free numbers').screenBreadcrumb('Assign free number: Browse more free numbers').customerType('new').visitorStatus('returning').recency('5d').customerLoginStatus('logged in').customerEligibility('GF eligible').customerSegment('consumer').productTargetSegment('Prospect').build();
    analytics.logScreenView('GF explore', 'Browse more free numbers', params);
  }, []);
  (0, _react.useEffect)(() => {
    analyticLogScreenView();
  }, [analyticLogScreenView]);

  const analyticLogPressShuffle = () => {
    let analytics = new _duFirebaseAnalytics.default();
    analytics.logEvent('Shuffle_Button_Click', {
      event_category: 'Shuffle button',
      event_label: 'Shuffle',
      event_name: 'Browse more numbers: Shuffle numbers',
      event_action: 'Shuffle button click'
    });
  };

  const analyticLogPressConfirmAndProceed = () => {
    let analytics = new _duFirebaseAnalytics.default();
    analytics.logEvent('Confirm_And_Proceed_Button_Click', {
      event_category: 'Confirm and proceed button',
      event_label: 'Confirm and proceed ',
      event_name: 'Browse more numbers: Confirm and proceed',
      event_action: 'Confirm and proceed  button click'
    });
  };

  if (!phoneNumbersPayload) {
    return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
      style: {
        width: '100%',
        height: '100%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
      }
    }, /*#__PURE__*/_react.default.createElement(_reactNative.Text, null, "..."));
  }

  const proceedWithSelectedNumber = async () => {
    const releasePhoneNumbersResponse = await (0, _utils.releasePhoneNumbers)((0, _.getSalesOrder)().customer.id, (0, _.getSalesOrder)().salesOrderId, phoneNumberState.phoneNumber.id);

    if ((releasePhoneNumbersResponse === null || releasePhoneNumbersResponse === void 0 ? void 0 : releasePhoneNumbersResponse.status) === 'SUCCESS') {
      const reservePhoneNumberResponse = await (0, _utils.reservePhoneNumber)((0, _.getSalesOrder)().customer.id, [checkedPhoneNumber.id], (0, _.getSalesOrder)().salesOrderId);
      console.log('reservePhoneNumberResponse:', reservePhoneNumberResponse);
      finishNumberSelection();
    }
  };

  const finishNumberSelection = async () => {
    const productIdRes = await (0, _utils.getBssListProductOfferings)();

    if (productIdRes) {
      var id = productIdRes.bssListProductOfferings[0].id;
      await (0, _utils.addExistingOrderItemUnderParent)((0, _.getSalesOrder)().orderItemId || '', id, checkedPhoneNumber.id);
      numberSelectionComplete && numberSelectionComplete((0, _.getSalesOrder)(), {
        msisdn: checkedPhoneNumber.number
      });
    }
  };

  return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.root
  }, _reactNative.Platform.OS === 'ios' ? /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.iosStackHeaderPadding
  }) : /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: {
      marginBottom: safeAreaInsets.top
    }
  }), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuHeader, headerProps), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.subContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: {
      alignItems: 'center'
    }
  }, /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: styles.selectedNumber
  }, phoneNumberState.phoneNumber.phoneNumber ? (0, _reactNativeMaskText.mask)(`0${(_phoneNumberState$pho = phoneNumberState.phoneNumber.phoneNumber) === null || _phoneNumberState$pho === void 0 ? void 0 : _phoneNumberState$pho.substring(3)}`, '999 999 99999') : '')), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.grayBanner
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: {
      flex: 3,
      paddingRight: 30
    }
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, null, expired ? /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: styles.grayBannerTitle
  }, "Numbers have expired") : /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: styles.grayBannerTitle
  }, "Numbers are locked for you for", ' ', /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: styles.purple
  }, _getTime(seconds)))), /*#__PURE__*/_react.default.createElement(_reactNative.View, null, /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: styles.grayBannerDecription
  }, expired ? 'The numbers have expired, shuffle to get a new set' : 'These numbers are reserved for you until the timer ends'))), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: {
      flex: 1,
      justifyContent: 'flex-end'
    }
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuButton, {
    type: "teritary",
    title: "Shuffle",
    size: "small",
    icon: {
      iconName: 'refresh'
    },
    containerStyle: [styles.shuffleButton],
    onPress: () => {
      console.log(checkedPhoneNumber);
      analyticLogPressShuffle();
      refreshNumbers();
    }
  }))), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.listContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNative.FlatList, {
    style: {
      marginBottom: 200
    },
    data: phoneNumbersPayload.phoneNumber,
    renderItem: _ref2 => {
      var _item$phoneNumber;

      let {
        item
      } = _ref2;
      return (
        /*#__PURE__*/
        //TODO::Update the tool kit
        _react.default.createElement(_reactNative.TouchableOpacity, {
          onPress: () => {
            handleNumberSelection(item.id);
          }
        }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuListItemStatic, {
          disabled: expired,
          key: item.id,
          title: (0, _reactNativeMaskText.mask)(`0${item === null || item === void 0 ? void 0 : (_item$phoneNumber = item.phoneNumber) === null || _item$phoneNumber === void 0 ? void 0 : _item$phoneNumber.substring(3)}`, '999 999 99999'),
          bottomDivider: true,
          onCheckListValueChange: () => {
            handleNumberSelection(item.id);
          },
          checkListValue: checkedPhoneNumber.number === item.phoneNumber ? true : false,
          enableCheckList: !expired,
          checkListType: 'radio'
        }))
      );
    }
  }))), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuSheet, sheetProps), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuButtonsDock, {
    shadow: true,
    items: [/*#__PURE__*/_react.default.createElement(_duUiToolkit.DuButton, {
      title: "Confirm and proceed",
      type: "primary",
      onPress: () => {
        analyticLogPressConfirmAndProceed();

        if (checkedPhoneNumber.number.length > 0) {
          if (phoneNumbersPayload) {
            proceedWithSelectedNumber();
          }
        }
      },
      disabled: expired || checkedPhoneNumber.number.length === 0
    })]
  }), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuOverlay, {
    isVisible: overlayVisibility,
    overlayStyle: styles.overlay,
    onBackdropPress: () => {
      setOverlayVisibility(false);
    }
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuDialog, {
    headline: ' ',
    body: ' ',
    primaryText: 'Ok',
    icon: {
      artWorkWidth: 29,
      artWorkHeight: 26,
      iconName: 'warning',
      iconColor: '#F5C311'
    },
    pressedPrimary: () => {
      setOverlayVisibility(false);
    }
  })));
};

var _default = BrowseMoreNumberScreen;
exports.default = _default;

const styles = _reactNative.StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: 'white'
  },
  subContainer: {
    flex: 1,
    backgroundColor: 'white'
  },
  iosStackHeaderPadding: {
    marginBottom: 14
  },
  grayBanner: {
    // marginTop: 24,
    padding: 16,
    backgroundColor: '#F5F6F7',
    borderRadius: 12,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 16
  },
  grayBannerTitle: {
    fontFamily: 'Moderat-Bold',
    fontSize: 16,
    textAlign: 'left',
    letterSpacing: -0.2,
    lineHeight: 20,
    color: '#041333'
  },
  grayBannerDecription: {
    marginTop: 8,
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    textAlign: 'left',
    lineHeight: 20,
    color: '#5E687D'
  },
  purple: {
    color: '#753BBD'
  },
  numberAndButtonContainer: {
    marginTop: 24,
    padding: 16,
    borderColor: '#D7D9DE',
    borderWidth: 1,
    borderRadius: 12
  },
  badgeAndButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 24,
    alignItems: 'center'
  },
  shuffleButton: {
    paddingHorizontal: 0
  },
  numberContainer: {
    marginTop: 8
  },
  number: {
    fontFamily: 'Moderat-Bold',
    fontSize: 22,
    textAlign: 'left',
    letterSpacing: -0.3,
    lineHeight: 26,
    color: '#041333'
  },
  browsMoreButton: {
    marginTop: 32,
    flexDirection: 'row'
  },
  overlay: {
    paddingHorizontal: 16,
    backgroundColor: 'transparent',
    elevation: 0
  },
  listContainer: {
    width: '100%',
    marginTop: 24,
    marginBottom: 50
  },
  selectedNumber: {
    fontSize: 40,
    marginVertical: 30,
    fontFamily: 'Moderat-Medium',
    color: '#000'
  }
});
//# sourceMappingURL=BrowseMoreNumberScreen.js.map