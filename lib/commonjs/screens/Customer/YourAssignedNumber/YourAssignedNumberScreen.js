"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _duUiToolkit = require("@du-greenfield/du-ui-toolkit");

var _reactNativeMaskText = require("react-native-mask-text");

var _react = _interopRequireWildcard(require("react"));

var _reactNative = require("react-native");

var _duAnalyticsPluginCore = require("@du-greenfield/du-analytics-plugin-core");

var _duFirebaseAnalytics = _interopRequireDefault(require("@du-greenfield/du-firebase-analytics"));

var _reactNativeSafeAreaContext = require("react-native-safe-area-context");

var _Order = _interopRequireDefault(require("../../../api/services/Order"));

var _utils = require("../../../utils");

var _reactRedux = require("react-redux");

var _phoneNumbersSlice = require("../../../redux/features/phoneNumbersSlice");

var _AmountDockItem = require("./AmountDockItem");

var _ = require("../../../");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const CustomerYourAssignedNumberScreen = _ref => {
  var _orderScreenDynamics$, _orderScreenDynamics$2, _orderScreenDynamics$3, _phoneNumbers$phoneNu, _phoneNumbers$phoneNu2, _phoneNumbers$phoneNu3, _phoneNumbers$phoneNu4;

  let {
    navigation,
    numberSelectionComplete,
    onBrowseMoreTap,
    rootNavigation
  } = _ref;
  const [overlayVisibility, setOverlayVisibility] = (0, _react.useState)(false);
  const [orderScreenDynamics, setOrderScreenDynamics] = (0, _react.useState)(undefined);
  const [phoneNumbersPayload, setPhoneNumbersPayload] = (0, _react.useState)(undefined);
  const [phoneNumberLocked, setPhoneNumberLocked] = (0, _react.useState)(false);
  const safeAreaInsets = (0, _reactNativeSafeAreaContext.useSafeAreaInsets)();
  const [showSheet, setShowSheet] = (0, _react.useState)(false);
  const [error, setError] = (0, _react.useState)();
  const phoneNumbers = (0, _reactRedux.useSelector)(state => state.phoneNumberSlice);
  const dispatch = (0, _reactRedux.useDispatch)();

  async function fetchData() {
    setPhoneNumbersPayload(undefined);
    setOrderScreenDynamics(await _Order.default.getScreenDynamics());
    await lockPhoneNumberOnScreen();
  }

  async function lockPhoneNumberOnScreen() {
    let response = await (0, _utils.lockPhoneNumber)((0, _.getSalesOrder)().customer.id, 1, []);

    if (response !== null && response !== void 0 && response.errorCodes && (response === null || response === void 0 ? void 0 : response.errorCodes.length) > 0) {
      setError(response === null || response === void 0 ? void 0 : response.errorCodes[0]);
      setOverlayVisibility(true);
      return;
    }

    if ((response === null || response === void 0 ? void 0 : response.status) === 'SUCCESS') {
      var _response$phoneNumber;

      setShowSheet(response.phoneNumber.length === 0);
      setPhoneNumbersPayload(response);
      const reservePhoneNumberResponse = await (0, _utils.reservePhoneNumber)((0, _.getSalesOrder)().customer.id, [response === null || response === void 0 ? void 0 : (_response$phoneNumber = response.phoneNumber[0]) === null || _response$phoneNumber === void 0 ? void 0 : _response$phoneNumber.id], (0, _.getSalesOrder)().salesOrderId);

      if ((reservePhoneNumberResponse === null || reservePhoneNumberResponse === void 0 ? void 0 : reservePhoneNumberResponse.status) === 'SUCCESS') {
        setPhoneNumbersPayload(response);
        dispatch((0, _phoneNumbersSlice.phoneNumberSelected)({
          id: response.phoneNumber[0].id,
          phoneNumber: response.phoneNumber[0].phoneNumber
        }));

        for (let phoneNumberObject of response.phoneNumber) {
          if (phoneNumberObject.status === 'LOCKED') {
            setPhoneNumberLocked(true);
          }
        }
      }
    }
  }

  const sheetProps = {
    isVisible: showSheet,
    onClose: () => setShowSheet(false),
    standard: {
      title: "We're sorry, there are no available numbers",
      body: 'We’re sorry, there are no available numbers\n  Please try again later.',
      icon: {
        iconName: 'search-off',
        iconColor: '#BA0023',
        artWorkHeight: 36,
        artWorkWidth: 36
      },
      buttons: [{
        text: 'Try again',
        type: 'primary',
        onPress: () => {
          setShowSheet(false);
          fetchData();
        }
      }]
    }
  };
  (0, _react.useEffect)(() => {
    fetchData(); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const analyticLogScreenView = (0, _react.useCallback)(() => {
    let analytics = new _duFirebaseAnalytics.default();
    let tagBuilder = new _duAnalyticsPluginCore.DuAnalyticsTagBuilder();
    let params = tagBuilder.pageName('Assign random number').journeyName('GF Explore').pageChannel('Customer App').previousScreenName('Lines section').screenType('pdp').screenHierarchy('Homepage: pdp: Family plan: Assign random number').screenBreadcrumb('Family plan: Assign random number').customerType('new').visitorStatus('returning').recency('5d').customerLoginStatus('logged in').customerEligibility('GF eligible').customerSegment('consumer').productTargetSegment('Prospect').build();
    analytics.logScreenView('GF explore', 'Assign random number', params);
  }, []);
  (0, _react.useEffect)(() => {
    analyticLogScreenView();
  }, [analyticLogScreenView]);

  const analyticLogPressEligibilityCheck = () => {
    let analytics = new _duFirebaseAnalytics.default();
    analytics.logEvent('Continue_To_Eligibility_Check_Button_Click', {
      event_category: 'Continue to eligibility check button',
      event_label: 'Continue to eligibility check',
      event_name: 'Assigned a number: Continue to eligibility check',
      event_action: 'Continue to eligibility check button Click'
    });
  };

  const analyticLogPressBrowseNumbers = () => {
    let analytics = new _duFirebaseAnalytics.default();
    analytics.logEvent('Browse_More_Numbers_Button_Click', {
      event_category: 'Browse more numbers button',
      event_label: 'Browse more numbers',
      event_name: 'Assigned a number: Browse more numbers',
      event_action: 'Browse more numbers button Click'
    });
  };

  const headerProps = {
    left: 'back',
    leftTertiaryText: 'Close',
    safeArea: true,
    leftPressed: () => {
      _reactNative.Platform.OS === 'android' && _reactNative.StatusBar.setBackgroundColor('white');

      if (_reactNative.Platform.OS === 'android') {
        _reactNative.StatusBar.setBarStyle('dark-content', true);
      }

      if (navigation.canGoBack()) {
        navigation.goBack();
      } else {
        if (rootNavigation.canGoBack()) {
          rootNavigation.goBack();
        }
      }
    },
    right: 'tertiary',
    rightTertiary: {
      text: 'Help',
      disable: true
    },
    statusBar: {
      barStyle: 'dark-content',
      backgroundColor: 'white'
    },
    background: 'white'
  };

  if (!phoneNumbersPayload) {
    return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
      style: {
        width: '100%',
        height: '100%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
      }
    }, /*#__PURE__*/_react.default.createElement(_reactNative.Text, null, "..."));
  }

  const finishNumberSelection = async () => {
    const productIdRes = await (0, _utils.getBssListProductOfferings)();

    if (productIdRes) {
      var id = productIdRes.bssListProductOfferings[0].id;
      await (0, _utils.addExistingOrderItemUnderParent)((0, _.getSalesOrder)().orderItemId || '', id, phoneNumbersPayload === null || phoneNumbersPayload === void 0 ? void 0 : phoneNumbersPayload.phoneNumber[0].id);
      numberSelectionComplete && numberSelectionComplete((0, _.getSalesOrder)(), {
        msisdn: phoneNumbersPayload.phoneNumber[0].phoneNumber
      });
    }
  };

  return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.root
  }, _reactNative.Platform.OS === 'ios' ? /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.iosStackHeaderPadding
  }) : /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: {
      marginBottom: safeAreaInsets.top
    }
  }), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuHeader, headerProps), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.jumbotronContainer
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuJumbotron, {
    alignment: 'left',
    mainJumbotron: orderScreenDynamics === null || orderScreenDynamics === void 0 ? void 0 : (_orderScreenDynamics$ = orderScreenDynamics.fields.assigned_number_model.mapValue.fields.heading.mapValue.fields.en.mapValue.fields.plain_text) === null || _orderScreenDynamics$ === void 0 ? void 0 : _orderScreenDynamics$.stringValue,
    contanierStyle: styles.jumbotronContainerStyle,
    size: 'xxlarge'
  })), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.subContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: [styles.numberAndButtonContainer // eslint-disable-next-line react-native/no-inline-styles
    ]
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.badgeAndButtonContainer
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuBadge, {
    title: orderScreenDynamics === null || orderScreenDynamics === void 0 ? void 0 : (_orderScreenDynamics$2 = orderScreenDynamics.fields.assigned_number_model.mapValue.fields.lines.arrayValue.values[0].mapValue.fields.label.mapValue.fields.en.mapValue.fields.plain_text) === null || _orderScreenDynamics$2 === void 0 ? void 0 : _orderScreenDynamics$2.stringValue // eslint-disable-next-line react-native/no-inline-styles
    // backgroundStyle={expired && { backgroundColor: 'transparent' }}

  }), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuButton, {
    type: "teritary",
    title: orderScreenDynamics === null || orderScreenDynamics === void 0 ? void 0 : (_orderScreenDynamics$3 = orderScreenDynamics.fields.assigned_number_model.mapValue.fields.lines.arrayValue.values[0].mapValue.fields.shuffle.mapValue.fields.en.mapValue.fields.plain_text) === null || _orderScreenDynamics$3 === void 0 ? void 0 : _orderScreenDynamics$3.stringValue,
    size: "small",
    disabled: true,
    icon: {
      iconName: 'refresh'
    },
    containerStyle: styles.shuffleButton,
    onPress: () => {
      // setExpired(false);
      lockPhoneNumberOnScreen();
    }
  })), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.numberContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNativeMaskText.MaskedText, {
    style: styles.number,
    mask: "999 999 9999"
  }, (phoneNumbers === null || phoneNumbers === void 0 ? void 0 : (_phoneNumbers$phoneNu = phoneNumbers.phoneNumber) === null || _phoneNumbers$phoneNu === void 0 ? void 0 : (_phoneNumbers$phoneNu2 = _phoneNumbers$phoneNu.phoneNumber) === null || _phoneNumbers$phoneNu2 === void 0 ? void 0 : _phoneNumbers$phoneNu2.length) > 0 ? `0${phoneNumbers === null || phoneNumbers === void 0 ? void 0 : (_phoneNumbers$phoneNu3 = phoneNumbers.phoneNumber) === null || _phoneNumbers$phoneNu3 === void 0 ? void 0 : (_phoneNumbers$phoneNu4 = _phoneNumbers$phoneNu3.phoneNumber) === null || _phoneNumbers$phoneNu4 === void 0 ? void 0 : _phoneNumbers$phoneNu4.substring(3)}` : '')), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.browsMoreButton
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuButton, {
    type: "secondary",
    title: "Browse numbers",
    size: "small",
    onPress: () => {
      analyticLogPressBrowseNumbers();
      onBrowseMoreTap && onBrowseMoreTap(false);
    }
  })))), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuSheet, sheetProps), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuButtonsDock, {
    shadow: true,
    items: [/*#__PURE__*/_react.default.createElement(_AmountDockItem.AmountDockItem, {
      title: 'Total',
      titleValue: (0, _.getSalesOrder)().product.plan.price.currencyCode + ' ' + (0, _.getSalesOrder)().product.plan.price.amount.toFixed(2),
      captionValue: 'Incl 5% VAT',
      caption: (0, _.getSalesOrder)().product.name
    }), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuButton, {
      title: "Continue to eligibility check",
      type: "primary",
      onPress: () => {
        analyticLogPressEligibilityCheck();

        if (phoneNumbersPayload) {
          finishNumberSelection();
        }
      },
      disabled: !phoneNumberLocked
    })]
  }), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuOverlay, {
    isVisible: overlayVisibility,
    overlayStyle: styles.overlay,
    onBackdropPress: () => {
      setOverlayVisibility(false);
    }
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuDialog, {
    headline: '',
    body: error === null || error === void 0 ? void 0 : error.message,
    primaryText: 'Ok',
    icon: {
      artWorkWidth: 29,
      artWorkHeight: 26,
      iconName: 'warning',
      iconColor: '#F5C311'
    },
    pressedPrimary: () => {
      setOverlayVisibility(false);
    }
  })));
};

var _default = CustomerYourAssignedNumberScreen;
exports.default = _default;

const styles = _reactNative.StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: 'white'
  },
  subContainer: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 16
  },
  iosStackHeaderPadding: {
    marginBottom: 14
  },
  grayBanner: {
    marginTop: 24,
    padding: 16,
    backgroundColor: '#F5F6F7',
    borderRadius: 12,
    flexDirection: 'row',
    alignItems: 'center'
  },
  grayBannerTitle: {
    fontFamily: 'Moderat-Bold',
    fontSize: 16,
    textAlign: 'left',
    letterSpacing: -0.2,
    lineHeight: 20,
    color: '#041333'
  },
  grayBannerDecription: {
    marginTop: 8,
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    textAlign: 'left',
    lineHeight: 20,
    color: '#5E687D'
  },
  purple: {
    color: '#753BBD'
  },
  numberAndButtonContainer: {
    // marginTop: 24,
    padding: 16,
    borderColor: '#D7D9DE',
    borderWidth: 1,
    borderRadius: 12
  },
  badgeAndButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 24,
    alignItems: 'center'
  },
  shuffleButton: {
    paddingHorizontal: 0
  },
  numberContainer: {
    marginTop: 8
  },
  number: {
    fontFamily: 'Moderat-Bold',
    fontSize: 22,
    textAlign: 'left',
    letterSpacing: -0.3,
    lineHeight: 26,
    color: '#041333'
  },
  browsMoreButton: {
    marginTop: 32,
    flexDirection: 'row'
  },
  overlay: {
    paddingHorizontal: 16,
    backgroundColor: 'transparent',
    elevation: 0
  },
  jumbotronContainer: {
    marginTop: 16,
    marginBottom: 18,
    width: '90%'
  },
  jumbotronContainerStyle: {
    padding: 16
  }
});
//# sourceMappingURL=YourAssignedNumberScreen.js.map