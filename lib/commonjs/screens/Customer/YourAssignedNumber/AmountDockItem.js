"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AmountDockItem = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactNative = require("react-native");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const AmountDockItem = props => {
  const {
    title,
    titleValue,
    caption,
    captionValue
  } = props;
  return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: amount.container
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: amount.subContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: amount.title
  }, title), /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: amount.title
  }, titleValue)), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: [amount.subContainer, // eslint-disable-next-line react-native/no-inline-styles
    {
      justifyContent: caption ? 'space-between' : 'flex-end'
    }]
  }, caption ? /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: amount.captionLeft
  }, caption) : null, /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: amount.captionRight
  }, captionValue)));
};

exports.AmountDockItem = AmountDockItem;

const amount = _reactNative.StyleSheet.create({
  container: {
    flexDirection: 'column',
    marginBottom: 8
  },
  subContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  title: {
    fontFamily: 'Moderat-Bold',
    fontSize: 16,
    fontWeight: '700',
    color: '#121212',
    lineHeight: 20,
    letterSpacing: -0.2
  },
  captionLeft: {
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    fontWeight: '400',
    color: '#5E687D',
    lineHeight: 20
  },
  captionRight: {
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    fontWeight: '400',
    color: '#737B8D',
    lineHeight: 20
  }
});
//# sourceMappingURL=AmountDockItem.js.map