"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _duUiToolkit = require("@du-greenfield/du-ui-toolkit");

var _react = _interopRequireWildcard(require("react"));

var _reactNativeMaskText = require("react-native-mask-text");

var _reactNative = require("react-native");

var _reactNativeSafeAreaContext = require("react-native-safe-area-context");

var _utils = require("../../../utils");

var _ = require("../../../");

var _reactRedux = require("react-redux");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const BrowseMoreNumberScreen = _ref => {
  var _phoneNumberState$pho, _phoneNumberState$pho2, _phoneNumberState$pho3, _phoneNumberState$pho4;

  let {
    navigation,
    numberSelectionComplete,
    gotoDashboardClicked,
    params
  } = _ref;
  console.log('Params from assign number screen: ', params);
  const timeout = 720;
  const intervalRef = (0, _react.useRef)(null);
  const [seconds, setSeconds] = (0, _react.useState)(timeout);
  const [expired, setExpired] = (0, _react.useState)(false);
  const safeAreaInsets = (0, _reactNativeSafeAreaContext.useSafeAreaInsets)();
  const [checkedPhoneNumber, setCheckedPhoneNumber] = (0, _react.useState)({
    id: '',
    number: ''
  });
  const [phoneNumbersPayload, setPhoneNumbersPayload] = (0, _react.useState)(undefined);
  const phoneNumberState = (0, _reactRedux.useSelector)(state => state.phoneNumberSlice);

  async function fetchNumbers() {
    if (!(0, _.getSalesOrder)().customer.id) {
      return;
    }

    const response = await (0, _utils.lockPhoneNumber)((0, _.getSalesOrder)().customer.id, 5, []);

    if ((response === null || response === void 0 ? void 0 : response.status) === 'SUCCESS') {
      setPhoneNumbersPayload(response); // Use this to test no numbers availbale senario
      // setPhoneNumbersPayload({
      //   phoneNumber: [],
      //   status: 'ss',
      //   errorCodes: [],
      // });

      setCheckedPhoneNumber({
        id: '',
        number: ''
      });
    }
  }

  (0, _react.useEffect)(() => {
    fetchNumbers();
  }, []);
  (0, _react.useEffect)(() => {
    if (seconds <= 0) {
      fetchNumbers();
      setExpired(true);
      setTimeout(() => {
        refreshNumbers();
      }, 500);
      return;
    }

    const timer = setTimeout(() => setSeconds(seconds - 1), 1000);
    intervalRef.current = timer;
    return () => {
      clearInterval(intervalRef.current);
    }; // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [seconds]);

  function refreshNumbers() {
    setPhoneNumbersPayload(undefined);
    setExpired(false);
    setSeconds(timeout);
    fetchNumbers();
  } // const props: DuMobileNumberProps = {
  //   incomplete: false,
  //   disableClick: true,
  // };


  function handleNumberSelection(id) {
    for (let number of phoneNumbersPayload === null || phoneNumbersPayload === void 0 ? void 0 : phoneNumbersPayload.phoneNumber) {
      if (id === number.id) {
        setCheckedPhoneNumber({
          number: number.phoneNumber,
          id: number.id
        }); // dispatch(
        //   phoneNumberSelected({
        //     phoneNumber: number.phoneNumber,
        //   })
        // );
      }
    }
  }

  const headerProps = {
    left: 'back',
    leftPressed: () => {
      _reactNative.Platform.OS === 'android' && _reactNative.StatusBar.setBackgroundColor('white');

      if (_reactNative.Platform.OS === 'android') {
        _reactNative.StatusBar.setBarStyle('dark-content', true);
      }

      if (navigation.canGoBack()) {
        navigation.goBack();
      }
    },
    navToDashBoard: true,
    rightTertiarySupport: {
      pressed: () => gotoDashboardClicked()
    },
    right: 'tertiary',
    rightTertiary: {
      text: 'Help',
      disable: true
    }
  };

  const _getTime = _seconds => {
    _seconds = Number(_seconds);
    var m = Math.floor(_seconds % 3600 / 60);
    var s = Math.floor(_seconds % 3600 % 60);
    return `${(0, _utils.addZero)(m, 2)}:${(0, _utils.addZero)(s, 2)}`;
  };

  async function proceedWithAddExistingOrderItemUnderParent(productIdRes, msisdnId, msisdn) {
    var id = productIdRes.bssListProductOfferings[0].id;
    const addExistingOrderItemUnderParentResponse = await (0, _utils.addExistingOrderItemUnderParent)((0, _.getSalesOrder)().orderItemId, id, msisdnId);

    if ((addExistingOrderItemUnderParentResponse === null || addExistingOrderItemUnderParentResponse === void 0 ? void 0 : addExistingOrderItemUnderParentResponse.status) === 'SUCCESS') {
      var _addExistingOrderItem;

      const simCardData = addExistingOrderItemUnderParentResponse === null || addExistingOrderItemUnderParentResponse === void 0 ? void 0 : (_addExistingOrderItem = addExistingOrderItemUnderParentResponse.salesOrder) === null || _addExistingOrderItem === void 0 ? void 0 : _addExistingOrderItem.orderItems[0].orderItems.find(orderItem => orderItem.offer.name === 'SIM Card');
      (0, _.getSalesOrder)().simCardOrderItemId = simCardData === null || simCardData === void 0 ? void 0 : simCardData.orderItemId;
      numberSelectionComplete && numberSelectionComplete((0, _.getSalesOrder)(), {
        msisdn: msisdn,
        isEsim: params.isEsim
      });
    }
  }

  const proceedWithSelectedNumber = async () => {
    const releasePhoneNumbersResponse = await (0, _utils.releasePhoneNumbers)((0, _.getSalesOrder)().customer.id, (0, _.getSalesOrder)().salesOrderId, phoneNumberState.phoneNumber.id);

    if ((releasePhoneNumbersResponse === null || releasePhoneNumbersResponse === void 0 ? void 0 : releasePhoneNumbersResponse.status) === 'SUCCESS') {
      await (0, _utils.reservePhoneNumber)((0, _.getSalesOrder)().customer.id, [checkedPhoneNumber.id], (0, _.getSalesOrder)().salesOrderId);
      const productIdRes = await (0, _utils.getBssListProductOfferings)();

      if (productIdRes) {
        if (params.isEsim) {
          const updateOrderItemsResponse = await (0, _utils.updateOrderItems)((0, _.getSalesOrder)().salesOrderId, (0, _.getSalesOrder)().simCardOrderItemId);

          if (updateOrderItemsResponse && productIdRes) {
            await proceedWithAddExistingOrderItemUnderParent(productIdRes, checkedPhoneNumber.id, checkedPhoneNumber.number);
          }
        } else {
          await proceedWithAddExistingOrderItemUnderParent(productIdRes, checkedPhoneNumber.id, checkedPhoneNumber.number);
        }
      }
    }
  };

  if (!phoneNumbersPayload) {
    return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
      style: {
        width: '100%',
        height: '100%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
      }
    }, /*#__PURE__*/_react.default.createElement(_reactNative.Text, null, "..."));
  }

  const RenderUserData = () => {
    return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
      style: {
        alignItems: 'center',
        marginTop: 76
      }
    }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
      style: {
        backgroundColor: '#F5F6F7',
        borderRadius: 200,
        padding: 22
      }
    }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuIcon, {
      iconName: "search-off",
      iconSize: 35
    })), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuJumbotron, {
      mainJumbotron: 'We’re sorry, there are no available numbers',
      alignment: 'center',
      description: 'Please try again later!'
    }));
  };

  return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.root
  }, _reactNative.Platform.OS === 'ios' ? /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.iosStackHeaderPadding
  }) : /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: {
      marginBottom: safeAreaInsets.top
    }
  }), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuHeader, headerProps), /*#__PURE__*/_react.default.createElement(_reactNative.ScrollView, {
    contentContainerStyle: styles.scrollViewConntainerStyle
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuJumbotron, {
    size: "xxlarge",
    alignment: "center",
    mainJumbotron: "Browse more numbers"
  }), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.subContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: {
      alignItems: 'center'
    }
  }, /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: styles.selectedNumber
  }, (0, _reactNativeMaskText.mask)((phoneNumberState === null || phoneNumberState === void 0 ? void 0 : (_phoneNumberState$pho = phoneNumberState.phoneNumber) === null || _phoneNumberState$pho === void 0 ? void 0 : (_phoneNumberState$pho2 = _phoneNumberState$pho.phoneNumber) === null || _phoneNumberState$pho2 === void 0 ? void 0 : _phoneNumberState$pho2.length) > 0 ? `0${(_phoneNumberState$pho3 = phoneNumberState.phoneNumber) === null || _phoneNumberState$pho3 === void 0 ? void 0 : (_phoneNumberState$pho4 = _phoneNumberState$pho3.phoneNumber) === null || _phoneNumberState$pho4 === void 0 ? void 0 : _phoneNumberState$pho4.substring(3)}` : '', '999 99999999'))), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.grayBanner
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: {
      flex: 3,
      paddingRight: 30
    }
  }, expired ? /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: styles.grayBannerTitle
  }, "Numbers have expired") : /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.bannerInner
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuIcon, {
    iconName: "warning",
    iconSize: 16
  }), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.bannerDescriptionContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: styles.grayBannerTitle
  }, "Numbers are locked for you for", ' ', /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: styles.purple
  }, _getTime(seconds))), /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: styles.grayBannerDecription
  }, expired ? 'The numbers have expired, shuffle to get a new set' : 'These numbers are reserved for you until the timer ends')))), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: {
      flex: 1,
      alignItems: 'flex-end'
    }
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuButton, {
    type: "teritary",
    title: "Shuffle",
    size: "small",
    icon: {
      iconName: 'refresh'
    },
    containerStyle: [styles.shuffleButton],
    onPress: () => {
      refreshNumbers();
    }
  }))), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.listContainer
  }, phoneNumbersPayload.phoneNumber.length === 0 ? RenderUserData() : /*#__PURE__*/_react.default.createElement(_reactNative.FlatList, {
    data: phoneNumbersPayload.phoneNumber,
    renderItem: _ref2 => {
      var _item$phoneNumber;

      let {
        item
      } = _ref2;
      return /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuListItemStatic, {
        disabled: expired,
        key: item.id,
        title: (0, _reactNativeMaskText.mask)(`0${item === null || item === void 0 ? void 0 : (_item$phoneNumber = item.phoneNumber) === null || _item$phoneNumber === void 0 ? void 0 : _item$phoneNumber.substring(3)}`, '999 999 99999'),
        bottomDivider: true,
        onCheckListValueChange: () => {
          handleNumberSelection(item.id);
        },
        checkListValue: checkedPhoneNumber.number === item.phoneNumber ? true : false,
        enableCheckList: !expired,
        checkListType: 'radio'
      });
    }
  })))), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuButtonsDock, {
    shadow: true,
    items: [/*#__PURE__*/_react.default.createElement(_duUiToolkit.DuButton, {
      title: phoneNumbersPayload.phoneNumber.length === 0 ? 'Try again' : 'Confirm and proceed',
      type: "primary",
      onPress: () => {
        if (checkedPhoneNumber.number.length > 0) {
          proceedWithSelectedNumber();
        } else {
          fetchNumbers();
        }
      },
      disabled: phoneNumbersPayload.phoneNumber.length === 0 ? false : expired || checkedPhoneNumber.number.length === 0
    })]
  }));
};

var _default = BrowseMoreNumberScreen;
exports.default = _default;

const styles = _reactNative.StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: 'white'
  },
  subContainer: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    marginTop: 24
  },
  iosStackHeaderPadding: {
    marginBottom: 14
  },
  grayBanner: {
    marginTop: 24,
    padding: 16,
    backgroundColor: '#F5F6F7',
    borderRadius: 12,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 16,
    maxWidth: 869
  },
  bannerInner: {
    flexDirection: 'row'
  },
  bannerDescriptionContainer: {
    marginLeft: 16
  },
  grayBannerTitle: {
    fontFamily: 'Moderat-Bold',
    fontSize: 16,
    textAlign: 'left',
    letterSpacing: -0.2,
    lineHeight: 20,
    color: '#041333'
  },
  grayBannerDecription: {
    marginTop: 8,
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    textAlign: 'left',
    lineHeight: 20,
    color: '#5E687D'
  },
  purple: {
    color: '#753BBD'
  },
  numberAndButtonContainer: {
    marginTop: 24,
    padding: 16,
    borderColor: '#D7D9DE',
    borderWidth: 1,
    borderRadius: 12
  },
  badgeAndButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 24,
    alignItems: 'center'
  },
  shuffleButton: {
    paddingHorizontal: 0
  },
  numberContainer: {
    marginTop: 8
  },
  number: {
    fontFamily: 'Moderat-Bold',
    fontSize: 22,
    textAlign: 'left',
    letterSpacing: -0.3,
    lineHeight: 26,
    color: '#041333'
  },
  browsMoreButton: {
    marginTop: 32,
    flexDirection: 'row'
  },
  overlay: {
    paddingHorizontal: 16,
    backgroundColor: 'transparent',
    elevation: 0
  },
  listContainer: {
    width: '100%',
    maxWidth: 869,
    marginTop: 24,
    marginBottom: 50
  },
  selectedNumber: {
    fontSize: 40,
    marginVertical: 30,
    fontFamily: 'Moderat-Medium',
    color: '#000'
  },
  scrollViewConntainerStyle: {
    flexGrow: 1
  }
});
//# sourceMappingURL=BrowseMoreNumberScreen.js.map