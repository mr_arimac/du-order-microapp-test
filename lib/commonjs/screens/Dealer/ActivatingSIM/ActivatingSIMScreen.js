"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _duUiToolkit = require("@du-greenfield/du-ui-toolkit");

var _react = _interopRequireWildcard(require("react"));

var _reactNative = require("react-native");

var _reactNativeSafeAreaContext = require("react-native-safe-area-context");

var _gql = require("../../../utils/gql");

var _ = require("../../../");

var _mutations = require("./../../../utils/gql/mutations");

var _queries = require("./../../../utils/gql/queries");

var _firebaseData = _interopRequireDefault(require("../../../../firebaseData.json"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const ActivatingSIMScreen = _ref => {
  var _getSalesOrder$msisdn3;

  let {
    params,
    navigation,
    onSimActivateSuccess,
    gotoDashboardClicked
  } = _ref;
  const [progressValue, setProgressValue] = (0, _react.useState)(0.0);
  const {
    scannedBarcode
  } = params;
  const intervalRef = (0, _react.useRef)(null);
  const spinnerProps = {
    state: 'in-progress',
    size: 'large',
    progressValue: progressValue,
    loadingMessage: null
  };
  const safeAreaInsets = (0, _reactNativeSafeAreaContext.useSafeAreaInsets)();
  const headerProps = {
    left: 'back',
    right: 'tertiary',
    rightTertiary: {
      disable: true,
      text: 'Help'
    },
    navToDashBoard: true,
    rightTertiarySupport: {
      pressed: () => {
        gotoDashboardClicked();
      }
    },
    leftPressed: () => {
      if (_reactNative.Platform.OS === 'android') {
        _reactNative.StatusBar.setBarStyle('dark-content', true);

        _reactNative.StatusBar.setBackgroundColor('white');
      }

      if (navigation.canGoBack()) {
        navigation.goBack();
      }
    },
    statusBar: {
      barStyle: 'dark-content',
      backgroundColor: 'white'
    },
    background: 'white'
  };
  (0, _react.useEffect)(() => {
    _reactNative.StatusBar.setBackgroundColor('white');

    _reactNative.StatusBar.setBarStyle('dark-content', true);
  }, []); //TODO: remove after testing the flow
  // async function proceedToSubmitSalesOrder(
  //   dynamics: any | undefined,
  //   sumbitSalesOrder: () => Promise<void>
  // ) {
  //   if (dynamics?.fields.in_store_delivery.booleanValue) {
  //     if (!getSalesOrder().msisdn?.isEsim) {
  //       const productOfferingResponse = await getListProductOffering(
  //         BSS_LIST_PRODUCT_OFFERING
  //       );
  //       if (
  //         productOfferingResponse.code === 200 &&
  //         productOfferingResponse.jsonBody.data.bssListProductOfferings
  //           .length !== 0
  //       ) {
  //         let inStoreOfferingId =
  //           productOfferingResponse.jsonBody.data.bssListProductOfferings[0].id;
  //         let addExistingOrderRes = await addExistingOrderItemOld(
  //           getSalesOrder().salesOrderId,
  //           getSalesOrder().customer.customerLocations[0].id,
  //           inStoreOfferingId
  //         );
  //         if (addExistingOrderRes?.addExistingOrderItem.status === 'SUCCESS') {
  //           await sumbitSalesOrder();
  //         }
  //       }
  //     }
  //   } else {
  //     await sumbitSalesOrder();
  //   }
  // }
  // async function updateOrderItemWithBarcodeOld() {
  //   //TODO::REMOVE redundant API calls :: extract to redux store
  //   const dynamics = await screen.getScreenDynamics();
  //   // Call updateOrderItem for pSIM
  //   if (!getSalesOrder().msisdn?.isEsim) {
  //     const updateOrderItemResponse = await updateOrderItem(
  //       getSalesOrder().salesOrderId,
  //       //8997103122044565973 TODO::use this value as a pre hard coded one
  //       getSalesOrder().simCardOrderItemId!,
  //       scannedBarcode
  //     );
  //     if (updateOrderItemResponse.updateOrderItems.status == 'SUCCESS') {
  //       await proceedToSubmitSalesOrder(dynamics, sumbitSalesOrder);
  //     }
  //   } else {
  //     await proceedToSubmitSalesOrder(dynamics, sumbitSalesOrder);
  //   }
  //   async function sumbitSalesOrder() {
  //     let customer: any = null;
  //     let input: any = {
  //       salesOrderId: getSalesOrder().salesOrderId,
  //     };
  //     let gql: DocumentNode = SUBMIT_SALES_ORDER;
  //     if (dynamics?.fields.associate_billing_account.booleanValue) {
  //       customer = await getCustomer(
  //         getSalesOrder().customer.id,
  //         BSS_GET_CUSTOMER
  //       );
  //       input[`${dynamics?.fields.billing_account_field.stringValue}`] =
  //         customer?.billingAccounts[0]?.id;
  //       gql = SUBMIT_SALES_ORDER_WITH_BILLING(
  //         dynamics?.fields.billing_account_field.stringValue
  //       );
  //     }
  //     const submitRes = await submitSalesOrder(input, gql);
  //     if (submitRes.submitSalesOrder.status == 'SUCCESS') {
  //       setProgressValue(1);
  //       //TODO: Pass required data
  //       onSimActivateSuccess && onSimActivateSuccess({});
  //     }
  //   }
  // }
  // Submit sales order

  async function sumbitSalesOrder(dynamics) {
    let customer = null;
    let input = {
      salesOrderId: (0, _.getSalesOrder)().salesOrderId
    };
    let gql = _mutations.SUBMIT_SALES_ORDER;

    if (dynamics !== null && dynamics !== void 0 && dynamics.fields.associate_billing_account.booleanValue) {
      var _customer, _customer$billingAcco;

      customer = await (0, _gql.getCustomer)((0, _.getSalesOrder)().customer.id, _queries.BSS_GET_CUSTOMER);
      input[`${dynamics === null || dynamics === void 0 ? void 0 : dynamics.fields.billing_account_field.stringValue}`] = (_customer = customer) === null || _customer === void 0 ? void 0 : (_customer$billingAcco = _customer.billingAccounts[0]) === null || _customer$billingAcco === void 0 ? void 0 : _customer$billingAcco.id;
      gql = (0, _mutations.SUBMIT_SALES_ORDER_WITH_BILLING)(dynamics === null || dynamics === void 0 ? void 0 : dynamics.fields.billing_account_field.stringValue);
    }

    const submitRes = await (0, _gql.submitSalesOrder)(input, gql);

    if (submitRes.submitSalesOrder.status == 'SUCCESS') {
      setProgressValue(1); //TODO: Pass required data

      onSimActivateSuccess && onSimActivateSuccess({});
    }
  } // Update orderitem


  async function updateOrderItemWithBarcode() {
    const updateOrderItemResponse = await (0, _gql.updateOrderItem)((0, _.getSalesOrder)().salesOrderId, //8997103122044565973 TODO::use this value as a pre hard coded one
    (0, _.getSalesOrder)().simCardOrderItemId, scannedBarcode);
    return updateOrderItemResponse;
  } // List product offerings


  async function listProductOfferings() {
    const productOfferingResponse = await (0, _gql.getListProductOffering)(_queries.BSS_LIST_PRODUCT_OFFERING);
    return productOfferingResponse;
  } // Add existing order item


  async function addIngExistingOrderItem(inStoreOfferingId) {
    let addExistingOrderResponse = await (0, _gql.addExistingOrderItem)((0, _.getSalesOrder)().salesOrderId, (0, _.getSalesOrder)().customer.customerLocations[0].id, inStoreOfferingId);
    return addExistingOrderResponse;
  } //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //


  const proceedWithSimActivation = async () => {
    // const dynamics = await screen.getScreenDynamics();
    const dynamics = _firebaseData.default;
    console.log(11, dynamics);

    if ((0, _.getSalesOrder)().type === 'payg') {
      var _getSalesOrder$msisdn;

      if ((_getSalesOrder$msisdn = (0, _.getSalesOrder)().msisdn) !== null && _getSalesOrder$msisdn !== void 0 && _getSalesOrder$msisdn.isEsim) {
        // sumbitSalesOrder
        sumbitSalesOrder(dynamics);
      } else {
        // updateOrderItem
        const updateOrderItemResponse = await updateOrderItemWithBarcode();

        if (updateOrderItemResponse.updateOrderItems.status == 'SUCCESS') {
          // getListProductOffering
          const listProductOfferingResponse = await listProductOfferings(); // addExistingOrderItem

          if (listProductOfferingResponse.code === 200 && listProductOfferingResponse.jsonBody.data.bssListProductOfferings.length !== 0) {
            const addExistingOrderItemResponse = await addIngExistingOrderItem(listProductOfferingResponse.jsonBody.data.bssListProductOfferings[0].id); // sumbitSalesOrder

            if ((addExistingOrderItemResponse === null || addExistingOrderItemResponse === void 0 ? void 0 : addExistingOrderItemResponse.addExistingOrderItem.status) === 'SUCCESS') {
              await sumbitSalesOrder(dynamics);
            }
          }
        }
      }
    } else if ((0, _.getSalesOrder)().type === 'predefined') {
      var _getSalesOrder$msisdn2;

      if ((_getSalesOrder$msisdn2 = (0, _.getSalesOrder)().msisdn) !== null && _getSalesOrder$msisdn2 !== void 0 && _getSalesOrder$msisdn2.isEsim) {
        // sumbitSalesOrder
        sumbitSalesOrder(dynamics);
      } else {
        // updateOrderItem
        const updateOrderItemResponse = await updateOrderItemWithBarcode();

        if (updateOrderItemResponse.updateOrderItems.status == 'SUCCESS') {
          // getListProductOffering
          const listProductOfferingResponse = await listProductOfferings(); // addExistingOrderItem

          if (listProductOfferingResponse.code === 200 && listProductOfferingResponse.jsonBody.data.bssListProductOfferings.length !== 0) {
            const addExistingOrderItemResponse = await addIngExistingOrderItem(listProductOfferingResponse.jsonBody.data.bssListProductOfferings[0].id); // sumbitSalesOrder

            if ((addExistingOrderItemResponse === null || addExistingOrderItemResponse === void 0 ? void 0 : addExistingOrderItemResponse.addExistingOrderItem.status) === 'SUCCESS') {
              await sumbitSalesOrder(dynamics);
            }
          }
        }
      }
    }
  };

  (0, _react.useEffect)(() => {
    proceedWithSimActivation(); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  (0, _react.useEffect)(() => {
    const id = setInterval(() => {
      if (progressValue >= 1) {
        clearInterval(intervalRef.current);
      } else {
        setProgressValue(progressValue + 0.005);
      }
    }, 1);
    intervalRef.current = id;
    return () => {
      clearInterval(intervalRef.current);
    };
  });

  const getScreenMiddleContent = () => {
    return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
      style: styles.mainView
    }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
      style: styles.spinnerContainer
    }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
      style: styles.positioning
    }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuSpinner, spinnerProps), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
      style: styles.jumbotronContainer
    }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuJumbotron, {
      alignment: 'center',
      mainJumbotron: 'Activating SIM...',
      size: 'large'
    })))));
  };

  return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.root
  }, _reactNative.Platform.OS === 'ios' ? /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.iosStackHeaderPadding
  }) : /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: {
      marginBottom: safeAreaInsets.top
    }
  }), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuHeader, headerProps), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.midContainer
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuJumbotron, {
    mainJumbotron: (_getSalesOrder$msisdn3 = (0, _.getSalesOrder)().msisdn) !== null && _getSalesOrder$msisdn3 !== void 0 && _getSalesOrder$msisdn3.isEsim ? '' : 'Scan SIM barcode ',
    size: 'xlarge'
  }), getScreenMiddleContent()));
};

const styles = _reactNative.StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: 4
  },
  mainView: {
    flex: 1,
    backgroundColor: 'white',
    padding: 16
  },
  spinnerContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    justifyContent: 'center'
  },
  jumbotronContainerStyle: {
    padding: 6,
    marginBottom: 18
  },
  positioning: {
    bottom: '10%',
    alignItems: 'center',
    justifyContent: 'center'
  },
  iosStackHeaderPadding: {
    marginBottom: 14
  },
  dockedButton: {
    width: '100%'
  },
  midContainer: {
    paddingHorizontal: 105,
    flex: 1,
    marginTop: 36
  },
  jumbotronContainer: {
    marginTop: 32
  },
  jumbotronContainerWithIcon: {
    marginTop: 21
  }
});

var _default = ActivatingSIMScreen;
exports.default = _default;
//# sourceMappingURL=ActivatingSIMScreen.js.map