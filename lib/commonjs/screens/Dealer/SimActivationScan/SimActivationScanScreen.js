"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _duUiToolkit = require("@du-greenfield/du-ui-toolkit");

var _react = _interopRequireWildcard(require("react"));

var _reactNative = require("react-native");

var _ = require("../../../");

var _EnterICCIDManuallyModal = _interopRequireDefault(require("./EnterICCIDManuallyModal"));

var _reactNativeMaskText = require("react-native-mask-text");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const SimActivationScanScreen = _ref => {
  var _getSalesOrder$msisdn, _getSalesOrder$msisdn2, _getSalesOrder$msisdn3;

  let {
    navigation,
    onBarcodeButtonTap,
    gotoDashboardClicked
  } = _ref;
  const [isModalVisible, setIsModalVisible] = (0, _react.useState)(false);

  const takeAPhotoButton = () => {
    return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
      style: styles.takeAPhotoButtonContainer
    }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuButton, {
      title: "Take a photo of the SIM barcode",
      type: "primary",
      onPress: onBarcodeButtonTap
    }));
  };

  return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.root
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuHeader, {
    safeArea: true,
    left: "back",
    leftPressed: () => {
      if (navigation.canGoBack()) {
        navigation.goBack();
      }
    },
    navToDashBoard: true,
    rightTertiarySupport: {
      pressed: () => gotoDashboardClicked()
    },
    right: 'tertiary',
    rightTertiary: {
      disable: true,
      text: 'Help'
    },
    statusBar: {
      backgroundColor: 'white',
      barStyle: 'dark-content'
    },
    background: "white"
  }), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: [styles.subContainer]
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuJumbotron, {
    mainJumbotron: "SIM barcode"
  }), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.middleContainer
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuJumbotron, {
    mainJumbotron: (_getSalesOrder$msisdn = (0, _.getSalesOrder)().msisdn) !== null && _getSalesOrder$msisdn !== void 0 && _getSalesOrder$msisdn.msisdn ? (0, _reactNativeMaskText.mask)(`0${(_getSalesOrder$msisdn2 = (0, _.getSalesOrder)().msisdn) === null || _getSalesOrder$msisdn2 === void 0 ? void 0 : (_getSalesOrder$msisdn3 = _getSalesOrder$msisdn2.msisdn) === null || _getSalesOrder$msisdn3 === void 0 ? void 0 : _getSalesOrder$msisdn3.substring(3)}`, '999 999 99999') : '',
    overLine: "SIM 1",
    alignment: "center"
  }), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.simCardPlaceholderContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNative.Image, {
    source: require('./sim-card-placeholder.png')
  }), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.simCardOverlayContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, null, /*#__PURE__*/_react.default.createElement(_reactNative.Image, {
    source: require('./du-logo.png')
  })), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.simCardOverlayContentContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, null, /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: styles.iccidNumberTitle
  }, "ICCID Number")), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.iccidNumberContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: styles.iccidNumber
  }, "N/A")), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.barcodeContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNative.Image, {
    source: require('./barcode.png')
  })), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.barcodeNumberContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: styles.barcodeNumber
  }, "1248326122483261"))))), takeAPhotoButton()), /*#__PURE__*/_react.default.createElement(_EnterICCIDManuallyModal.default, {
    isVisible: isModalVisible,
    onPressClose: () => {
      setIsModalVisible(false);
    },
    verifyAndContinue: value => {
      setIsModalVisible(false);
      navigation.push('ActivatingSIMScreen', {
        scannedBarcode: value
      });
    }
  })));
};

const styles = _reactNative.StyleSheet.create({
  root: {
    flex: 1
  },
  subContainer: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 105,
    paddingVertical: 32
  },
  middleContainer: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  simCardPlaceholderContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  simCardOverlayContainer: {
    position: 'absolute',
    width: 300,
    height: 180
  },
  simCardOverlayContentContainer: {
    width: 180,
    marginTop: 17,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  iccidNumberTitle: {
    fontFamily: 'Moderat-Medium',
    fontSize: 11,
    textAlign: 'center',
    color: '#5E687D',
    lineHeight: 20
  },
  iccidNumberContainer: {
    marginTop: 8
  },
  iccidNumber: {
    fontFamily: 'Moderat-Medium',
    fontSize: 18,
    textAlign: 'center',
    color: '#041333',
    lineHeight: 24,
    letterSpacing: -0.15
  },
  barcodeContainer: {
    marginTop: 24
  },
  barcodeNumberContainer: {
    marginTop: 7
  },
  barcodeNumber: {
    fontFamily: 'Moderat-Regular',
    fontSize: 11,
    textAlign: 'center',
    color: '#233659',
    lineHeight: 20
  },
  enterManuallyButtonContainer: {
    width: 340,
    marginTop: 16
  },
  takeAPhotoButtonContainer: {
    width: 340
  }
});

var _default = SimActivationScanScreen;
exports.default = _default;
//# sourceMappingURL=SimActivationScanScreen.js.map