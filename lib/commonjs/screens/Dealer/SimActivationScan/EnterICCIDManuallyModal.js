"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _duUiToolkit = require("@du-greenfield/du-ui-toolkit");

var _react = _interopRequireWildcard(require("react"));

var _reactNative = require("react-native");

var _reactNativeModal = _interopRequireDefault(require("react-native-modal"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const EnterICCIDManuallyModal = _ref => {
  let {
    isVisible,
    onPressClose,
    verifyAndContinue
  } = _ref;
  const [ICCID, setICCID] = (0, _react.useState)('');
  const headerProps = {
    left: 'back',
    leftPressed: onPressClose,
    statusBar: _reactNative.Platform.OS === 'ios' ? {
      barStyle: 'default',
      backgroundColor: 'white'
    } : {
      barStyle: 'dark-content',
      backgroundColor: 'white'
    },
    background: 'white',
    handler: true
  };
  return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.centeredView
  }, /*#__PURE__*/_react.default.createElement(_reactNativeModal.default, {
    isVisible: isVisible,
    backdropColor: 'rgba(4, 19, 51, 1)',
    propagateSwipe: true,
    onSwipeComplete: onPressClose,
    swipeDirection: "down",
    onBackdropPress: onPressClose,
    onBackButtonPress: onPressClose
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.modalMainView
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuHeader, headerProps), /*#__PURE__*/_react.default.createElement(_reactNative.ScrollView, {
    contentContainerStyle: styles.scrollViewConntainerStyle
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuJumbotron, {
    alignment: 'center',
    mainJumbotron: 'Enter ICCID manually',
    description: 'Please fill in the details below',
    size: 'large'
  }), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.inputText
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuTextInput, {
    title: "ICCID number",
    onChangeText: text => {
      setICCID(text);
    }
  })), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.verifyAndContinueButtonContainer
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuButton, {
    title: "Verify and continue",
    onPress: () => verifyAndContinue && verifyAndContinue(ICCID),
    containerStyle: styles.dockedButton
  }))))));
};

const styles = _reactNative.StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalMainView: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: 12,
    paddingTop: 4,
    marginVertical: 30,
    marginHorizontal: 50,
    height: '100%',
    overflow: 'hidden'
  },
  dockedButton: {
    width: '100%'
  },
  inputText: {
    paddingHorizontal: 90,
    marginTop: 32
  },
  scrollViewConntainerStyle: {
    flexGrow: 1
  },
  verifyAndContinueButtonContainer: {
    paddingHorizontal: 28,
    paddingVertical: 32,
    width: '100%',
    height: '100%',
    flex: 1,
    flexGrow: 1,
    justifyContent: 'flex-end'
  }
});

var _default = EnterICCIDManuallyModal;
exports.default = _default;
//# sourceMappingURL=EnterICCIDManuallyModal.js.map