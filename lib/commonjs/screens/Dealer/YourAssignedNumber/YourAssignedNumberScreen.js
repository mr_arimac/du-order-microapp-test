"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _ = require("../../../");

var _duUiToolkit = require("@du-greenfield/du-ui-toolkit");

var _react = _interopRequireWildcard(require("react"));

var _reactNativeMaskText = require("react-native-mask-text");

var _reactNative = require("react-native");

var _reactNativeSafeAreaContext = require("react-native-safe-area-context");

var _reactRedux = require("react-redux");

var _phoneNumbersSlice = require("../../../redux/features/phoneNumbersSlice");

var _utils = require("../../../utils");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const DealerYourAssignedNumberScreen = _ref => {
  var _phoneNumbers$phoneNu, _phoneNumbers$phoneNu2, _phoneNumbers$phoneNu3, _phoneNumbers$phoneNu4;

  let {
    navigation,
    numberSelectionComplete,
    onBrowseMoreTap,
    gotoDashboardClicked
  } = _ref;
  const safeAreaInsets = (0, _reactNativeSafeAreaContext.useSafeAreaInsets)();
  const phoneNumbers = (0, _reactRedux.useSelector)(state => state.phoneNumberSlice);
  const dispatch = (0, _reactRedux.useDispatch)();
  const [isEsim, setIsEsim] = (0, _react.useState)(false);
  const [error, setError] = (0, _react.useState)();
  const [overlayVisibility, setOverlayVisibility] = (0, _react.useState)(false);
  const [errorHeader, setErrorHeader] = (0, _react.useState)('');
  const [phoneNumbersPayload, setPhoneNumbersPayload] = (0, _react.useState)(undefined);
  const [phoneNumberLocked, setPhoneNumberLocked] = (0, _react.useState)(false);
  const [noNoPhoneNumbersAvailable, setnoNoPhoneNumbersAvailable] = (0, _react.useState)(false);
  const headerProps = {
    left: 'back',
    leftPressed: () => {
      _reactNative.Platform.OS === 'android' && _reactNative.StatusBar.setBackgroundColor('white');

      if (_reactNative.Platform.OS === 'android') {
        _reactNative.StatusBar.setBarStyle('dark-content', true);
      }

      if (navigation.canGoBack()) {
        navigation.goBack();
      }
    },
    right: 'tertiary',
    rightTertiary: {
      text: 'Help',
      disable: true
    },
    navToDashBoard: true,
    rightTertiarySupport: {
      pressed: () => gotoDashboardClicked()
    },
    statusBar: {
      barStyle: 'dark-content',
      backgroundColor: 'white'
    },
    background: 'white'
  };

  function fetchData() {
    lockPhoneNumberOnScreen();
  }

  async function proceedWithAddExistingOrderItemUnderParent(productIdRes, msisdnId, msisdn) {
    var id = productIdRes.bssListProductOfferings[0].id;
    await (0, _utils.addExistingOrderItemUnderParent)((0, _.getSalesOrder)().orderItemId, id, msisdnId);
    numberSelectionComplete && numberSelectionComplete((0, _.getSalesOrder)(), {
      msisdn: msisdn,
      isEsim: isEsim
    });
  }

  async function lockPhoneNumberOnScreen() {
    setOverlayVisibility(false);

    if (!(0, _.getSalesOrder)().customer.id) {
      return;
    }

    const response = await (0, _utils.lockPhoneNumber)((0, _.getSalesOrder)().customer.id, 1, []);

    if (response !== null && response !== void 0 && response.errorCodes && (response === null || response === void 0 ? void 0 : response.errorCodes.length) > 0) {
      setError(response === null || response === void 0 ? void 0 : response.errorCodes[0]);
      setOverlayVisibility(true);
      return;
    }

    if ((response === null || response === void 0 ? void 0 : response.status) === 'SUCCESS') {
      if (response.phoneNumber.length === 0) {
        setnoNoPhoneNumbersAvailable(true);
        setPhoneNumbersPayload(response);
        setErrorHeader('There are no available numbers');
        setError({
          message: 'Please try again'
        });
        setOverlayVisibility(true);
        return;
      }

      setnoNoPhoneNumbersAvailable(false);
      setErrorHeader('');
      setError({
        message: ''
      });
      const reservePhoneNumberResponse = await (0, _utils.reservePhoneNumber)((0, _.getSalesOrder)().customer.id, [response.phoneNumber[0].id], (0, _.getSalesOrder)().salesOrderId);

      if ((reservePhoneNumberResponse === null || reservePhoneNumberResponse === void 0 ? void 0 : reservePhoneNumberResponse.status) === 'SUCCESS') {
        setPhoneNumbersPayload(response);
        dispatch((0, _phoneNumbersSlice.phoneNumberSelected)({
          id: response.phoneNumber[0].id,
          phoneNumber: response.phoneNumber[0].phoneNumber
        }));

        for (let phoneNumberObject of response.phoneNumber) {
          if (phoneNumberObject.status === 'LOCKED') {
            setPhoneNumberLocked(true);
          }
        }
      }
    }
  }

  (0, _react.useEffect)(() => {
    fetchData(); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (!phoneNumbersPayload) {
    return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
      style: {
        width: '100%',
        height: '100%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
      }
    }, /*#__PURE__*/_react.default.createElement(_reactNative.Text, null, "..."));
  }

  const finishNumberSelection = async () => {
    const productIdRes = await (0, _utils.getBssListProductOfferings)();

    if (isEsim) {
      const updateOrderItemsResponse = await (0, _utils.updateOrderItems)((0, _.getSalesOrder)().salesOrderId, (0, _.getSalesOrder)().simCardOrderItemId);
      console.log(555, updateOrderItemsResponse);

      if (updateOrderItemsResponse && productIdRes) {
        await proceedWithAddExistingOrderItemUnderParent(productIdRes, phoneNumbersPayload === null || phoneNumbersPayload === void 0 ? void 0 : phoneNumbersPayload.phoneNumber[0].id, phoneNumbersPayload === null || phoneNumbersPayload === void 0 ? void 0 : phoneNumbersPayload.phoneNumber[0].phoneNumber);
      }
    } else {
      console.log('proceedWithAddExistingOrderItemUnderParent');
      await proceedWithAddExistingOrderItemUnderParent(productIdRes, phoneNumbersPayload === null || phoneNumbersPayload === void 0 ? void 0 : phoneNumbersPayload.phoneNumber[0].id, phoneNumbersPayload === null || phoneNumbersPayload === void 0 ? void 0 : phoneNumbersPayload.phoneNumber[0].phoneNumber);
    }
  };

  return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.root
  }, _reactNative.Platform.OS === 'ios' ? /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.iosStackHeaderPadding
  }) : /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: {
      marginBottom: safeAreaInsets.top
    }
  }), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuHeader, headerProps), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.subContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View // eslint-disable-next-line react-native/no-inline-styles
  , {
    style: {
      marginTop: 32
    }
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuJumbotron, {
    size: "xxlarge",
    alignment: "center",
    mainJumbotron: "Your assigned number"
  })), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: [styles.numberAndButtonContainer // eslint-disable-next-line react-native/no-inline-styles
    // { opacity: expired ? 0.2 : 1 },
    ]
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, null, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.simCardIconContainer
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuIcon, {
    iconName: "chat-bubble",
    gradient: true
  }))), /*#__PURE__*/_react.default.createElement(_reactNative.View // eslint-disable-next-line react-native/no-inline-styles
  , {
    style: {
      flex: 1,
      flexDirection: 'column',
      marginLeft: 16
    }
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
    }
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.numberContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNativeMaskText.MaskedText, {
    style: styles.number,
    mask: "999 999 9999"
  }, (phoneNumbers === null || phoneNumbers === void 0 ? void 0 : (_phoneNumbers$phoneNu = phoneNumbers.phoneNumber) === null || _phoneNumbers$phoneNu === void 0 ? void 0 : (_phoneNumbers$phoneNu2 = _phoneNumbers$phoneNu.phoneNumber) === null || _phoneNumbers$phoneNu2 === void 0 ? void 0 : _phoneNumbers$phoneNu2.length) > 0 ? `0${phoneNumbers === null || phoneNumbers === void 0 ? void 0 : (_phoneNumbers$phoneNu3 = phoneNumbers.phoneNumber) === null || _phoneNumbers$phoneNu3 === void 0 ? void 0 : (_phoneNumbers$phoneNu4 = _phoneNumbers$phoneNu3.phoneNumber) === null || _phoneNumbers$phoneNu4 === void 0 ? void 0 : _phoneNumbers$phoneNu4.substring(3)}` : '')), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuButton, {
    type: "teritary",
    title: "Shuffle",
    size: "small",
    disabled: true,
    icon: {
      iconName: 'refresh'
    },
    containerStyle: styles.shuffleButton,
    onPress: () => {
      lockPhoneNumberOnScreen();
    }
  })), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.browsMoreButton
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuButton, {
    type: "secondary",
    title: "Browse numbers",
    size: "small",
    onPress: () => {
      onBrowseMoreTap && onBrowseMoreTap(isEsim);
    }
  }), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuSwitch, {
    title: "eSIM",
    onValueChange: () => setIsEsim(previousState => !previousState)
  })))), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuOverlay, {
    isVisible: overlayVisibility,
    overlayStyle: styles.overlay,
    onBackdropPress: () => {
      if (noNoPhoneNumbersAvailable) {
        lockPhoneNumberOnScreen();
      } else {
        setOverlayVisibility(false);
      }
    }
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuDialog, {
    headline: errorHeader,
    body: error === null || error === void 0 ? void 0 : error.message,
    primaryText: noNoPhoneNumbersAvailable ? 'Try again' : 'Ok',
    icon: {
      artWorkWidth: 29,
      artWorkHeight: 26,
      iconName: 'warning',
      iconColor: '#F5C311'
    },
    pressedPrimary: () => {
      if (noNoPhoneNumbersAvailable) {
        lockPhoneNumberOnScreen();
      } else {
        setOverlayVisibility(false);
      }
    }
  }))), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuButtonsDock, {
    shadow: true,
    items: [/*#__PURE__*/_react.default.createElement(_reactNative.View, {
      style: styles.buttonDockContainer
    }, /*#__PURE__*/_react.default.createElement(_reactNative.View // eslint-disable-next-line react-native/no-inline-styles
    , {
      style: {
        width: '30%'
      }
    }, /*#__PURE__*/_react.default.createElement(AmountDockItem, {
      title: (0, _.getSalesOrder)().product.name,
      titleValue: (0, _.getSalesOrder)().product.plan.price.currencyCode + ' ' + (0, _.getSalesOrder)().product.plan.price.amount.toFixed(2),
      captionValue: 'Incl. VAT',
      caption: 'Your selected plan'
    })), /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuButton, {
      title: "Continue to eligibility check",
      type: "primary",
      onPress: () => {
        if (phoneNumbersPayload) {
          finishNumberSelection();
        }
      },
      disabled: !phoneNumberLocked
    }))]
  }));
};

var _default = DealerYourAssignedNumberScreen;
exports.default = _default;

const styles = _reactNative.StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: 'white'
  },
  subContainer: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: '20%'
  },
  iosStackHeaderPadding: {
    marginBottom: 14
  },
  grayBanner: {
    marginTop: 24,
    padding: 16,
    backgroundColor: '#F5F6F7',
    borderRadius: 12,
    flexDirection: 'row',
    alignItems: 'center'
  },
  grayBannerTitle: {
    fontFamily: 'Moderat-Bold',
    fontSize: 16,
    textAlign: 'left',
    letterSpacing: -0.2,
    lineHeight: 20,
    color: '#041333'
  },
  grayBannerDecription: {
    marginTop: 8,
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    textAlign: 'left',
    letterSpacing: -0.2,
    lineHeight: 20,
    color: '#5E687D'
  },
  purple: {
    color: '#753BBD'
  },
  numberAndButtonContainer: {
    flexDirection: 'row',
    marginTop: 32,
    padding: 16,
    borderColor: '#D7D9DE',
    borderWidth: 1,
    borderRadius: 12
  },
  badgeAndButtonContainerBase: {},
  badgeAndButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 24,
    alignItems: 'center'
  },
  shuffleButton: {
    paddingHorizontal: 0
  },
  numberContainer: {
    marginTop: 8
  },
  number: {
    fontFamily: 'Moderat-Bold',
    fontSize: 22,
    textAlign: 'left',
    letterSpacing: -0.3,
    lineHeight: 26,
    color: '#041333'
  },
  browsMoreButton: {
    marginTop: 32,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  simCardIconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 48,
    height: 48,
    backgroundColor: '#F5F6F7',
    borderRadius: 24
  },
  buttonDockContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  overlay: {
    paddingHorizontal: 16,
    backgroundColor: 'transparent',
    elevation: 0
  }
});

const AmountDockItem = props => {
  const {
    title,
    titleValue,
    caption,
    captionValue
  } = props;
  return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: amount.container
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: amount.subContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: amount.title
  }, title), /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: amount.title
  }, titleValue)), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: [amount.subContainer, // eslint-disable-next-line react-native/no-inline-styles
    {
      justifyContent: caption ? 'space-between' : 'flex-end'
    }]
  }, caption ? /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: amount.captionLeft
  }, caption) : null, /*#__PURE__*/_react.default.createElement(_reactNative.Text, {
    style: amount.captionRight
  }, captionValue)));
};

const amount = _reactNative.StyleSheet.create({
  container: {
    flexDirection: 'column',
    marginBottom: 8
  },
  subContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  title: {
    fontFamily: 'Moderat-Bold',
    fontSize: 16,
    color: '#121212',
    lineHeight: 20,
    letterSpacing: -0.2
  },
  captionLeft: {
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    color: '#5E687D',
    lineHeight: 20
  },
  captionRight: {
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    color: '#737B8D',
    lineHeight: 20
  }
});
//# sourceMappingURL=YourAssignedNumberScreen.js.map