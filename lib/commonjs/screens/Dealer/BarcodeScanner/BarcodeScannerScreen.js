"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _duUiToolkit = require("@du-greenfield/du-ui-toolkit");

var _react = _interopRequireDefault(require("react"));

var _reactNative = require("react-native");

var _reactNativeCameraKit = require("react-native-camera-kit");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const BarcodeScannerScreen = _ref => {
  let {
    navigation,
    onBarcodeScanned,
    gotoDashboardClicked
  } = _ref;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  // const [scannedBarcode, setScannedBarcode] = useState<string>('');
  // useEffect(() => {
  //   console.log('scannedBarcode', scannedBarcode);
  //   if (scannedBarcode !== '' && scannedBarcode !== undefined) {
  //     onBarcodeScanned && onBarcodeScanned(scannedBarcode, true);
  //   }
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [scannedBarcode]);
  // const cameraTimeout = (timeoutValue: string) =>
  //   (timer = setTimeout(function () {
  //     onBarcodeScanned && onBarcodeScanned(scannedBarcode, false);
  //   }, parseInt(timeoutValue, 10)));
  // let timer: any;
  // screen.getScreenDynamics().then((scannedBarcodeConfig) => {
  //   const timeoutValue: string | undefined =
  //     scannedBarcodeConfig?.fields.barcode_scanner_timer.stringValue;
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  //   timer = setTimeout(() => {
  //     console.log('This will run after 1 second!');
  //     onBarcodeScanned && onBarcodeScanned('', false);
  //   }, parseInt(timeoutValue as string, 10));
  // });
  var sampleVar;

  function sampleFunction() {
    sampleVar = setTimeout(() => {
      console.log('--- Timeout');
      onBarcodeScanned && onBarcodeScanned('', false);
    }, 15000);
  }

  function sampleStopFunction() {
    clearTimeout(sampleVar);
  }

  sampleFunction(); // useEffect(() => {
  //   screen.getScreenDynamics().then((scannedBarcodeConfig) => {
  //     const timeoutValue: string | undefined =
  //       scannedBarcodeConfig?.fields.barcode_scanner_timer.stringValue;
  //     // eslint-disable-next-line react-hooks/exhaustive-deps
  //     timer = setTimeout(() => {
  //       console.log('This will run after 1 second!');
  //       onBarcodeScanned && onBarcodeScanned('', false);
  //     }, parseInt(timeoutValue as string, 10));
  //     return () => clearTimeout(timer);
  //   });
  //   // .then((scannedBarcodeConfig) => {
  //   //   const timeoutValue: string | undefined =
  //   //     scannedBarcodeConfig?.fields.barcode_scanner_timer.stringValue;
  //   //   if (scannedBarcode.length > 0) clearTimeout(timer);
  //   //   if (timeoutValue) {
  //   //     cameraTimeout(timeoutValue);
  //   //   }
  //   // });
  // }, []);

  return /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: [styles.root, {
      zIndex: 100
    }]
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: [styles.subContainer]
  }, /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.midContanier
  }, /*#__PURE__*/_react.default.createElement(_duUiToolkit.DuHeader, {
    safeArea: true,
    left: "back",
    leftPressed: () => {
      if (navigation.canGoBack()) {
        navigation.goBack();
      }
    },
    right: 'tertiary',
    rightTertiary: {
      disable: true,
      text: 'Help'
    },
    statusBar: {
      backgroundColor: '#041333',
      barStyle: 'light-content'
    },
    navToDashBoard: true,
    rightTertiarySupport: {
      pressed: () => gotoDashboardClicked()
    },
    background: "#041333"
  })), /*#__PURE__*/_react.default.createElement(_reactNative.View, {
    style: styles.cameraContainer
  }, /*#__PURE__*/_react.default.createElement(_reactNativeCameraKit.CameraScreen, {
    onBottomButtonPressed: () => {},
    scanBarcode: true,
    showFrame: true,
    allowCaptureRetake: false,
    laserColor: "red",
    frameColor: "white",
    onReadCode: event => {
      console.log('--- onReadCode');
      sampleStopFunction();

      if (event.nativeEvent.codeStringValue.length === 19 || event.nativeEvent.codeStringValue.length === 20) {
        onBarcodeScanned && onBarcodeScanned(event.nativeEvent.codeStringValue, true);
      } else {
        onBarcodeScanned && onBarcodeScanned('', false);
      }
    },
    hideControls: true,
    cameraRatioOverlay: undefined,
    captureButtonImage: undefined,
    cameraFlipImage: undefined,
    torchOnImage: undefined,
    torchOffImage: undefined
  }))));
};

const styles = _reactNative.StyleSheet.create({
  root: {
    flex: 1
  },
  subContainer: {
    flex: 1
  },
  scanText: {
    color: 'white',
    fontSize: 32,
    lineHeight: 30,
    fontFamily: 'Moderat-Bold'
  },
  midContanier: {
    zIndex: 100
  },
  cameraContainer: {
    flex: 1,
    zIndex: 10
  },
  camera: {
    flex: 1,
    width: '100%',
    // height: '90%',
    // width: '100%',
    backgroundColor: 'red' // marginTop: 100,

  },
  overlayText: {
    width: '100%',
    height: 120,
    // backgroundColor: 't',
    zIndex: 100,
    alignItems: 'center',
    justifyContent: 'flex-end'
  }
});

var _default = BarcodeScannerScreen;
exports.default = _default;
//# sourceMappingURL=BarcodeScannerScreen.js.map