"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "CustomerInitialScreen", {
  enumerable: true,
  get: function () {
    return _navigator.CustomerInitialScreen;
  }
});
Object.defineProperty(exports, "DuOrderDealerInitialScreen", {
  enumerable: true,
  get: function () {
    return _navigator.DuOrderDealerInitialScreen;
  }
});
exports.default = void 0;
exports.getMicroAppContext = getMicroAppContext;
exports.getSalesOrder = getSalesOrder;
exports.isDealer = void 0;

var _native = require("@react-navigation/native");

var _react = _interopRequireDefault(require("react"));

var _navigator = require("./navigator");

var _reactRedux = require("react-redux");

var _store = _interopRequireDefault(require("./redux/store"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

let isDealer = false;
exports.isDealer = isDealer;
var microAppContext;

function getMicroAppContext() {
  return microAppContext;
}

var salesOrder;

function getSalesOrder() {
  return salesOrder;
}

const DuOrder = props => {
  microAppContext = props.appContext;
  salesOrder = props.salesOrder;

  const App = () => {
    if (microAppContext.appType == 'dealer') {
      return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, !props.navContainer ? /*#__PURE__*/_react.default.createElement(_navigator.DealerAppStack, _extends({}, props, {
        initalScreen: props.dealerInitialScreen
      })) : /*#__PURE__*/_react.default.createElement(_native.NavigationContainer, _extends({
        independent: true
      }, props), /*#__PURE__*/_react.default.createElement(_navigator.DealerAppStack, _extends({}, props, {
        initalScreen: props.dealerInitialScreen
      }))));
    } else {
      return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, !props.navContainer ? /*#__PURE__*/_react.default.createElement(_navigator.CustomerAppStack, _extends({}, props, {
        initialScreen: props.customerInitialScreen
      })) : /*#__PURE__*/_react.default.createElement(_native.NavigationContainer, _extends({
        independent: true
      }, props), /*#__PURE__*/_react.default.createElement(_navigator.CustomerAppStack, _extends({}, props, {
        initialScreen: props.customerInitialScreen
      }))));
    }
  };

  return /*#__PURE__*/_react.default.createElement(_reactRedux.Provider, {
    store: _store.default
  }, /*#__PURE__*/_react.default.createElement(App, null));
};

var _default = DuOrder;
exports.default = _default;
//# sourceMappingURL=index.js.map