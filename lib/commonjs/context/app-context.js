"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.OrderAppContext = void 0;
exports.getAppState = getAppState;
exports.mainReducer = void 0;
exports.setAppState = setAppState;

var _react = require("react");

var _contextTypes = require("./context-types");

var _reducer = require("./reducer");

var _storage = require("../utils/storage");

const OrderAppContext = /*#__PURE__*/(0, _react.createContext)({
  state: _contextTypes.initialState,
  dispatch: () => null
});
exports.OrderAppContext = OrderAppContext;

async function getAppState() {
  return new Promise(resolve => {
    (0, _storage.load)('ORDER_APP_STATE').then(value => {
      if (value && value !== '') {
        resolve(value);
      } else {
        resolve(_contextTypes.initialState);
      }
    });
  });
}

async function setAppState(state) {
  (0, _storage.save)('ORDER_APP_STATE', state);
}

const mainReducer = action => {
  console.log('action.payload main', action.payload);
  const reducer = {
    browseMore: (0, _reducer.browseMoreReducer)(_contextTypes.initialState.browseMore, action)
  };
  setAppState(reducer);
  return reducer;
};

exports.mainReducer = mainReducer;
//# sourceMappingURL=app-context.js.map