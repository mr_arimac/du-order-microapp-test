"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.phoneNumberSelected = exports.default = void 0;

var _toolkit = require("@reduxjs/toolkit");

const initialState = {
  phoneNumber: '',
  phoneNumbers: []
};
const phoneNumberSlice = (0, _toolkit.createSlice)({
  name: 'phoneNumbers',
  initialState,
  reducers: {
    phoneNumberSelected: (state, action) => {
      state.phoneNumber = action.payload;
    }
  }
});
const {
  phoneNumberSelected
} = phoneNumberSlice.actions;
exports.phoneNumberSelected = phoneNumberSelected;
var _default = phoneNumberSlice.reducer;
exports.default = _default;
//# sourceMappingURL=phoneNumbersSlice.js.map