"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _toolkit = require("@reduxjs/toolkit");

var _phoneNumbersSlice = _interopRequireDefault(require("./features/phoneNumbersSlice"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = (0, _toolkit.configureStore)({
  reducer: {
    phoneNumberSlice: _phoneNumbersSlice.default
  }
});

exports.default = _default;
//# sourceMappingURL=store.js.map