function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { CustomerAppStack, CustomerInitialScreen, DealerAppStack, DuOrderDealerInitialScreen } from './navigator';
import { Provider } from 'react-redux';
import store from './redux/store';
export { DuOrderDealerInitialScreen, CustomerInitialScreen };
export let isDealer = false;
var microAppContext;
export function getMicroAppContext() {
  return microAppContext;
}
var salesOrder;
export function getSalesOrder() {
  return salesOrder;
}

const DuOrder = props => {
  microAppContext = props.appContext;
  salesOrder = props.salesOrder;

  const App = () => {
    if (microAppContext.appType == 'dealer') {
      return /*#__PURE__*/React.createElement(React.Fragment, null, !props.navContainer ? /*#__PURE__*/React.createElement(DealerAppStack, _extends({}, props, {
        initalScreen: props.dealerInitialScreen
      })) : /*#__PURE__*/React.createElement(NavigationContainer, _extends({
        independent: true
      }, props), /*#__PURE__*/React.createElement(DealerAppStack, _extends({}, props, {
        initalScreen: props.dealerInitialScreen
      }))));
    } else {
      return /*#__PURE__*/React.createElement(React.Fragment, null, !props.navContainer ? /*#__PURE__*/React.createElement(CustomerAppStack, _extends({}, props, {
        initialScreen: props.customerInitialScreen
      })) : /*#__PURE__*/React.createElement(NavigationContainer, _extends({
        independent: true
      }, props), /*#__PURE__*/React.createElement(CustomerAppStack, _extends({}, props, {
        initialScreen: props.customerInitialScreen
      }))));
    }
  };

  return /*#__PURE__*/React.createElement(Provider, {
    store: store
  }, /*#__PURE__*/React.createElement(App, null));
};

export default DuOrder;
//# sourceMappingURL=index.js.map