import { getSalesOrder } from '../../../';
import { DuButton, DuButtonsDock, DuDialog, DuHeader, DuIcon, DuJumbotron, DuOverlay, DuSwitch } from '@du-greenfield/du-ui-toolkit';
import React, { useEffect, useState } from 'react';
import { MaskedText } from 'react-native-mask-text';
import { Platform, StatusBar, StyleSheet, Text, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useDispatch, useSelector } from 'react-redux';
import { phoneNumberSelected } from '../../../redux/features/phoneNumbersSlice';
import { lockPhoneNumber, reservePhoneNumber, getBssListProductOfferings, addExistingOrderItemUnderParent, updateOrderItems } from '../../../utils';

const DealerYourAssignedNumberScreen = _ref => {
  var _phoneNumbers$phoneNu, _phoneNumbers$phoneNu2, _phoneNumbers$phoneNu3, _phoneNumbers$phoneNu4;

  let {
    navigation,
    numberSelectionComplete,
    onBrowseMoreTap,
    gotoDashboardClicked
  } = _ref;
  const safeAreaInsets = useSafeAreaInsets();
  const phoneNumbers = useSelector(state => state.phoneNumberSlice);
  const dispatch = useDispatch();
  const [isEsim, setIsEsim] = useState(false);
  const [error, setError] = useState();
  const [overlayVisibility, setOverlayVisibility] = useState(false);
  const [errorHeader, setErrorHeader] = useState('');
  const [phoneNumbersPayload, setPhoneNumbersPayload] = useState(undefined);
  const [phoneNumberLocked, setPhoneNumberLocked] = useState(false);
  const [noNoPhoneNumbersAvailable, setnoNoPhoneNumbersAvailable] = useState(false);
  const headerProps = {
    left: 'back',
    leftPressed: () => {
      Platform.OS === 'android' && StatusBar.setBackgroundColor('white');

      if (Platform.OS === 'android') {
        StatusBar.setBarStyle('dark-content', true);
      }

      if (navigation.canGoBack()) {
        navigation.goBack();
      }
    },
    right: 'tertiary',
    rightTertiary: {
      text: 'Help',
      disable: true
    },
    navToDashBoard: true,
    rightTertiarySupport: {
      pressed: () => gotoDashboardClicked()
    },
    statusBar: {
      barStyle: 'dark-content',
      backgroundColor: 'white'
    },
    background: 'white'
  };

  function fetchData() {
    lockPhoneNumberOnScreen();
  }

  async function proceedWithAddExistingOrderItemUnderParent(productIdRes, msisdnId, msisdn) {
    var id = productIdRes.bssListProductOfferings[0].id;
    await addExistingOrderItemUnderParent(getSalesOrder().orderItemId, id, msisdnId);
    numberSelectionComplete && numberSelectionComplete(getSalesOrder(), {
      msisdn: msisdn,
      isEsim: isEsim
    });
  }

  async function lockPhoneNumberOnScreen() {
    setOverlayVisibility(false);

    if (!getSalesOrder().customer.id) {
      return;
    }

    const response = await lockPhoneNumber(getSalesOrder().customer.id, 1, []);

    if (response !== null && response !== void 0 && response.errorCodes && (response === null || response === void 0 ? void 0 : response.errorCodes.length) > 0) {
      setError(response === null || response === void 0 ? void 0 : response.errorCodes[0]);
      setOverlayVisibility(true);
      return;
    }

    if ((response === null || response === void 0 ? void 0 : response.status) === 'SUCCESS') {
      if (response.phoneNumber.length === 0) {
        setnoNoPhoneNumbersAvailable(true);
        setPhoneNumbersPayload(response);
        setErrorHeader('There are no available numbers');
        setError({
          message: 'Please try again'
        });
        setOverlayVisibility(true);
        return;
      }

      setnoNoPhoneNumbersAvailable(false);
      setErrorHeader('');
      setError({
        message: ''
      });
      const reservePhoneNumberResponse = await reservePhoneNumber(getSalesOrder().customer.id, [response.phoneNumber[0].id], getSalesOrder().salesOrderId);

      if ((reservePhoneNumberResponse === null || reservePhoneNumberResponse === void 0 ? void 0 : reservePhoneNumberResponse.status) === 'SUCCESS') {
        setPhoneNumbersPayload(response);
        dispatch(phoneNumberSelected({
          id: response.phoneNumber[0].id,
          phoneNumber: response.phoneNumber[0].phoneNumber
        }));

        for (let phoneNumberObject of response.phoneNumber) {
          if (phoneNumberObject.status === 'LOCKED') {
            setPhoneNumberLocked(true);
          }
        }
      }
    }
  }

  useEffect(() => {
    fetchData(); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (!phoneNumbersPayload) {
    return /*#__PURE__*/React.createElement(View, {
      style: {
        width: '100%',
        height: '100%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
      }
    }, /*#__PURE__*/React.createElement(Text, null, "..."));
  }

  const finishNumberSelection = async () => {
    const productIdRes = await getBssListProductOfferings();

    if (isEsim) {
      const updateOrderItemsResponse = await updateOrderItems(getSalesOrder().salesOrderId, getSalesOrder().simCardOrderItemId);
      console.log(555, updateOrderItemsResponse);

      if (updateOrderItemsResponse && productIdRes) {
        await proceedWithAddExistingOrderItemUnderParent(productIdRes, phoneNumbersPayload === null || phoneNumbersPayload === void 0 ? void 0 : phoneNumbersPayload.phoneNumber[0].id, phoneNumbersPayload === null || phoneNumbersPayload === void 0 ? void 0 : phoneNumbersPayload.phoneNumber[0].phoneNumber);
      }
    } else {
      console.log('proceedWithAddExistingOrderItemUnderParent');
      await proceedWithAddExistingOrderItemUnderParent(productIdRes, phoneNumbersPayload === null || phoneNumbersPayload === void 0 ? void 0 : phoneNumbersPayload.phoneNumber[0].id, phoneNumbersPayload === null || phoneNumbersPayload === void 0 ? void 0 : phoneNumbersPayload.phoneNumber[0].phoneNumber);
    }
  };

  return /*#__PURE__*/React.createElement(View, {
    style: styles.root
  }, Platform.OS === 'ios' ? /*#__PURE__*/React.createElement(View, {
    style: styles.iosStackHeaderPadding
  }) : /*#__PURE__*/React.createElement(View, {
    style: {
      marginBottom: safeAreaInsets.top
    }
  }), /*#__PURE__*/React.createElement(DuHeader, headerProps), /*#__PURE__*/React.createElement(View, {
    style: styles.subContainer
  }, /*#__PURE__*/React.createElement(View // eslint-disable-next-line react-native/no-inline-styles
  , {
    style: {
      marginTop: 32
    }
  }, /*#__PURE__*/React.createElement(DuJumbotron, {
    size: "xxlarge",
    alignment: "center",
    mainJumbotron: "Your assigned number"
  })), /*#__PURE__*/React.createElement(View, {
    style: [styles.numberAndButtonContainer // eslint-disable-next-line react-native/no-inline-styles
    // { opacity: expired ? 0.2 : 1 },
    ]
  }, /*#__PURE__*/React.createElement(View, null, /*#__PURE__*/React.createElement(View, {
    style: styles.simCardIconContainer
  }, /*#__PURE__*/React.createElement(DuIcon, {
    iconName: "chat-bubble",
    gradient: true
  }))), /*#__PURE__*/React.createElement(View // eslint-disable-next-line react-native/no-inline-styles
  , {
    style: {
      flex: 1,
      flexDirection: 'column',
      marginLeft: 16
    }
  }, /*#__PURE__*/React.createElement(View, {
    style: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
    }
  }, /*#__PURE__*/React.createElement(View, {
    style: styles.numberContainer
  }, /*#__PURE__*/React.createElement(MaskedText, {
    style: styles.number,
    mask: "999 999 9999"
  }, (phoneNumbers === null || phoneNumbers === void 0 ? void 0 : (_phoneNumbers$phoneNu = phoneNumbers.phoneNumber) === null || _phoneNumbers$phoneNu === void 0 ? void 0 : (_phoneNumbers$phoneNu2 = _phoneNumbers$phoneNu.phoneNumber) === null || _phoneNumbers$phoneNu2 === void 0 ? void 0 : _phoneNumbers$phoneNu2.length) > 0 ? `0${phoneNumbers === null || phoneNumbers === void 0 ? void 0 : (_phoneNumbers$phoneNu3 = phoneNumbers.phoneNumber) === null || _phoneNumbers$phoneNu3 === void 0 ? void 0 : (_phoneNumbers$phoneNu4 = _phoneNumbers$phoneNu3.phoneNumber) === null || _phoneNumbers$phoneNu4 === void 0 ? void 0 : _phoneNumbers$phoneNu4.substring(3)}` : '')), /*#__PURE__*/React.createElement(DuButton, {
    type: "teritary",
    title: "Shuffle",
    size: "small",
    disabled: true,
    icon: {
      iconName: 'refresh'
    },
    containerStyle: styles.shuffleButton,
    onPress: () => {
      lockPhoneNumberOnScreen();
    }
  })), /*#__PURE__*/React.createElement(View, {
    style: styles.browsMoreButton
  }, /*#__PURE__*/React.createElement(DuButton, {
    type: "secondary",
    title: "Browse numbers",
    size: "small",
    onPress: () => {
      onBrowseMoreTap && onBrowseMoreTap(isEsim);
    }
  }), /*#__PURE__*/React.createElement(DuSwitch, {
    title: "eSIM",
    onValueChange: () => setIsEsim(previousState => !previousState)
  })))), /*#__PURE__*/React.createElement(DuOverlay, {
    isVisible: overlayVisibility,
    overlayStyle: styles.overlay,
    onBackdropPress: () => {
      if (noNoPhoneNumbersAvailable) {
        lockPhoneNumberOnScreen();
      } else {
        setOverlayVisibility(false);
      }
    }
  }, /*#__PURE__*/React.createElement(DuDialog, {
    headline: errorHeader,
    body: error === null || error === void 0 ? void 0 : error.message,
    primaryText: noNoPhoneNumbersAvailable ? 'Try again' : 'Ok',
    icon: {
      artWorkWidth: 29,
      artWorkHeight: 26,
      iconName: 'warning',
      iconColor: '#F5C311'
    },
    pressedPrimary: () => {
      if (noNoPhoneNumbersAvailable) {
        lockPhoneNumberOnScreen();
      } else {
        setOverlayVisibility(false);
      }
    }
  }))), /*#__PURE__*/React.createElement(DuButtonsDock, {
    shadow: true,
    items: [/*#__PURE__*/React.createElement(View, {
      style: styles.buttonDockContainer
    }, /*#__PURE__*/React.createElement(View // eslint-disable-next-line react-native/no-inline-styles
    , {
      style: {
        width: '30%'
      }
    }, /*#__PURE__*/React.createElement(AmountDockItem, {
      title: getSalesOrder().product.name,
      titleValue: getSalesOrder().product.plan.price.currencyCode + ' ' + getSalesOrder().product.plan.price.amount.toFixed(2),
      captionValue: 'Incl. VAT',
      caption: 'Your selected plan'
    })), /*#__PURE__*/React.createElement(DuButton, {
      title: "Continue to eligibility check",
      type: "primary",
      onPress: () => {
        if (phoneNumbersPayload) {
          finishNumberSelection();
        }
      },
      disabled: !phoneNumberLocked
    }))]
  }));
};

export default DealerYourAssignedNumberScreen;
const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: 'white'
  },
  subContainer: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: '20%'
  },
  iosStackHeaderPadding: {
    marginBottom: 14
  },
  grayBanner: {
    marginTop: 24,
    padding: 16,
    backgroundColor: '#F5F6F7',
    borderRadius: 12,
    flexDirection: 'row',
    alignItems: 'center'
  },
  grayBannerTitle: {
    fontFamily: 'Moderat-Bold',
    fontSize: 16,
    textAlign: 'left',
    letterSpacing: -0.2,
    lineHeight: 20,
    color: '#041333'
  },
  grayBannerDecription: {
    marginTop: 8,
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    textAlign: 'left',
    letterSpacing: -0.2,
    lineHeight: 20,
    color: '#5E687D'
  },
  purple: {
    color: '#753BBD'
  },
  numberAndButtonContainer: {
    flexDirection: 'row',
    marginTop: 32,
    padding: 16,
    borderColor: '#D7D9DE',
    borderWidth: 1,
    borderRadius: 12
  },
  badgeAndButtonContainerBase: {},
  badgeAndButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 24,
    alignItems: 'center'
  },
  shuffleButton: {
    paddingHorizontal: 0
  },
  numberContainer: {
    marginTop: 8
  },
  number: {
    fontFamily: 'Moderat-Bold',
    fontSize: 22,
    textAlign: 'left',
    letterSpacing: -0.3,
    lineHeight: 26,
    color: '#041333'
  },
  browsMoreButton: {
    marginTop: 32,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  simCardIconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 48,
    height: 48,
    backgroundColor: '#F5F6F7',
    borderRadius: 24
  },
  buttonDockContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  overlay: {
    paddingHorizontal: 16,
    backgroundColor: 'transparent',
    elevation: 0
  }
});

const AmountDockItem = props => {
  const {
    title,
    titleValue,
    caption,
    captionValue
  } = props;
  return /*#__PURE__*/React.createElement(View, {
    style: amount.container
  }, /*#__PURE__*/React.createElement(View, {
    style: amount.subContainer
  }, /*#__PURE__*/React.createElement(Text, {
    style: amount.title
  }, title), /*#__PURE__*/React.createElement(Text, {
    style: amount.title
  }, titleValue)), /*#__PURE__*/React.createElement(View, {
    style: [amount.subContainer, // eslint-disable-next-line react-native/no-inline-styles
    {
      justifyContent: caption ? 'space-between' : 'flex-end'
    }]
  }, caption ? /*#__PURE__*/React.createElement(Text, {
    style: amount.captionLeft
  }, caption) : null, /*#__PURE__*/React.createElement(Text, {
    style: amount.captionRight
  }, captionValue)));
};

const amount = StyleSheet.create({
  container: {
    flexDirection: 'column',
    marginBottom: 8
  },
  subContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  title: {
    fontFamily: 'Moderat-Bold',
    fontSize: 16,
    color: '#121212',
    lineHeight: 20,
    letterSpacing: -0.2
  },
  captionLeft: {
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    color: '#5E687D',
    lineHeight: 20
  },
  captionRight: {
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    color: '#737B8D',
    lineHeight: 20
  }
});
//# sourceMappingURL=YourAssignedNumberScreen.js.map