import { DuButton, DuHeader, DuJumbotron } from '@du-greenfield/du-ui-toolkit';
import React, { useState } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { getSalesOrder } from '../../../';
import EnterICCIDManuallyModal from './EnterICCIDManuallyModal';
import { mask } from 'react-native-mask-text';

const SimActivationScanScreen = _ref => {
  var _getSalesOrder$msisdn, _getSalesOrder$msisdn2, _getSalesOrder$msisdn3;

  let {
    navigation,
    onBarcodeButtonTap,
    gotoDashboardClicked
  } = _ref;
  const [isModalVisible, setIsModalVisible] = useState(false);

  const takeAPhotoButton = () => {
    return /*#__PURE__*/React.createElement(View, {
      style: styles.takeAPhotoButtonContainer
    }, /*#__PURE__*/React.createElement(DuButton, {
      title: "Take a photo of the SIM barcode",
      type: "primary",
      onPress: onBarcodeButtonTap
    }));
  };

  return /*#__PURE__*/React.createElement(View, {
    style: styles.root
  }, /*#__PURE__*/React.createElement(DuHeader, {
    safeArea: true,
    left: "back",
    leftPressed: () => {
      if (navigation.canGoBack()) {
        navigation.goBack();
      }
    },
    navToDashBoard: true,
    rightTertiarySupport: {
      pressed: () => gotoDashboardClicked()
    },
    right: 'tertiary',
    rightTertiary: {
      disable: true,
      text: 'Help'
    },
    statusBar: {
      backgroundColor: 'white',
      barStyle: 'dark-content'
    },
    background: "white"
  }), /*#__PURE__*/React.createElement(View, {
    style: [styles.subContainer]
  }, /*#__PURE__*/React.createElement(DuJumbotron, {
    mainJumbotron: "SIM barcode"
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.middleContainer
  }, /*#__PURE__*/React.createElement(DuJumbotron, {
    mainJumbotron: (_getSalesOrder$msisdn = getSalesOrder().msisdn) !== null && _getSalesOrder$msisdn !== void 0 && _getSalesOrder$msisdn.msisdn ? mask(`0${(_getSalesOrder$msisdn2 = getSalesOrder().msisdn) === null || _getSalesOrder$msisdn2 === void 0 ? void 0 : (_getSalesOrder$msisdn3 = _getSalesOrder$msisdn2.msisdn) === null || _getSalesOrder$msisdn3 === void 0 ? void 0 : _getSalesOrder$msisdn3.substring(3)}`, '999 999 99999') : '',
    overLine: "SIM 1",
    alignment: "center"
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.simCardPlaceholderContainer
  }, /*#__PURE__*/React.createElement(Image, {
    source: require('./sim-card-placeholder.png')
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.simCardOverlayContainer
  }, /*#__PURE__*/React.createElement(View, null, /*#__PURE__*/React.createElement(Image, {
    source: require('./du-logo.png')
  })), /*#__PURE__*/React.createElement(View, {
    style: styles.simCardOverlayContentContainer
  }, /*#__PURE__*/React.createElement(View, null, /*#__PURE__*/React.createElement(Text, {
    style: styles.iccidNumberTitle
  }, "ICCID Number")), /*#__PURE__*/React.createElement(View, {
    style: styles.iccidNumberContainer
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.iccidNumber
  }, "N/A")), /*#__PURE__*/React.createElement(View, {
    style: styles.barcodeContainer
  }, /*#__PURE__*/React.createElement(Image, {
    source: require('./barcode.png')
  })), /*#__PURE__*/React.createElement(View, {
    style: styles.barcodeNumberContainer
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.barcodeNumber
  }, "1248326122483261"))))), takeAPhotoButton()), /*#__PURE__*/React.createElement(EnterICCIDManuallyModal, {
    isVisible: isModalVisible,
    onPressClose: () => {
      setIsModalVisible(false);
    },
    verifyAndContinue: value => {
      setIsModalVisible(false);
      navigation.push('ActivatingSIMScreen', {
        scannedBarcode: value
      });
    }
  })));
};

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  subContainer: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 105,
    paddingVertical: 32
  },
  middleContainer: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  simCardPlaceholderContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  simCardOverlayContainer: {
    position: 'absolute',
    width: 300,
    height: 180
  },
  simCardOverlayContentContainer: {
    width: 180,
    marginTop: 17,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  iccidNumberTitle: {
    fontFamily: 'Moderat-Medium',
    fontSize: 11,
    textAlign: 'center',
    color: '#5E687D',
    lineHeight: 20
  },
  iccidNumberContainer: {
    marginTop: 8
  },
  iccidNumber: {
    fontFamily: 'Moderat-Medium',
    fontSize: 18,
    textAlign: 'center',
    color: '#041333',
    lineHeight: 24,
    letterSpacing: -0.15
  },
  barcodeContainer: {
    marginTop: 24
  },
  barcodeNumberContainer: {
    marginTop: 7
  },
  barcodeNumber: {
    fontFamily: 'Moderat-Regular',
    fontSize: 11,
    textAlign: 'center',
    color: '#233659',
    lineHeight: 20
  },
  enterManuallyButtonContainer: {
    width: 340,
    marginTop: 16
  },
  takeAPhotoButtonContainer: {
    width: 340
  }
});
export default SimActivationScanScreen;
//# sourceMappingURL=SimActivationScanScreen.js.map