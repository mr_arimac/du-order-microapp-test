import { DuButton, DuHeader, DuJumbotron, DuTextInput } from '@du-greenfield/du-ui-toolkit';
import React, { useState } from 'react';
import { Platform, ScrollView, StyleSheet, View } from 'react-native';
import Modal from 'react-native-modal';

const EnterICCIDManuallyModal = _ref => {
  let {
    isVisible,
    onPressClose,
    verifyAndContinue
  } = _ref;
  const [ICCID, setICCID] = useState('');
  const headerProps = {
    left: 'back',
    leftPressed: onPressClose,
    statusBar: Platform.OS === 'ios' ? {
      barStyle: 'default',
      backgroundColor: 'white'
    } : {
      barStyle: 'dark-content',
      backgroundColor: 'white'
    },
    background: 'white',
    handler: true
  };
  return /*#__PURE__*/React.createElement(View, {
    style: styles.centeredView
  }, /*#__PURE__*/React.createElement(Modal, {
    isVisible: isVisible,
    backdropColor: 'rgba(4, 19, 51, 1)',
    propagateSwipe: true,
    onSwipeComplete: onPressClose,
    swipeDirection: "down",
    onBackdropPress: onPressClose,
    onBackButtonPress: onPressClose
  }, /*#__PURE__*/React.createElement(View, {
    style: styles.modalMainView
  }, /*#__PURE__*/React.createElement(DuHeader, headerProps), /*#__PURE__*/React.createElement(ScrollView, {
    contentContainerStyle: styles.scrollViewConntainerStyle
  }, /*#__PURE__*/React.createElement(DuJumbotron, {
    alignment: 'center',
    mainJumbotron: 'Enter ICCID manually',
    description: 'Please fill in the details below',
    size: 'large'
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.inputText
  }, /*#__PURE__*/React.createElement(DuTextInput, {
    title: "ICCID number",
    onChangeText: text => {
      setICCID(text);
    }
  })), /*#__PURE__*/React.createElement(View, {
    style: styles.verifyAndContinueButtonContainer
  }, /*#__PURE__*/React.createElement(DuButton, {
    title: "Verify and continue",
    onPress: () => verifyAndContinue && verifyAndContinue(ICCID),
    containerStyle: styles.dockedButton
  }))))));
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  modalMainView: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: 12,
    paddingTop: 4,
    marginVertical: 30,
    marginHorizontal: 50,
    height: '100%',
    overflow: 'hidden'
  },
  dockedButton: {
    width: '100%'
  },
  inputText: {
    paddingHorizontal: 90,
    marginTop: 32
  },
  scrollViewConntainerStyle: {
    flexGrow: 1
  },
  verifyAndContinueButtonContainer: {
    paddingHorizontal: 28,
    paddingVertical: 32,
    width: '100%',
    height: '100%',
    flex: 1,
    flexGrow: 1,
    justifyContent: 'flex-end'
  }
});
export default EnterICCIDManuallyModal;
//# sourceMappingURL=EnterICCIDManuallyModal.js.map