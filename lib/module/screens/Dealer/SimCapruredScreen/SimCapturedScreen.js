import { DuButton, DuHeader, DuJumbotron, DuBanner, DuButtonsDock } from '@du-greenfield/du-ui-toolkit';
import { mask } from 'react-native-mask-text';
import React, { useState } from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import { getSalesOrder } from '../../../';
import EnterICCIDManuallyModal from '../SimActivationScan/EnterICCIDManuallyModal';

const SimCapturedScreen = _ref => {
  var _getSalesOrder$msisdn, _getSalesOrder$msisdn2, _getSalesOrder$msisdn3;

  let {
    params,
    navigation,
    onSimActivateTap,
    onRescanTap,
    gotoDashboardClicked
  } = _ref;
  const [isModalVisible, setIsModalVisible] = useState(false);
  const {
    simScanned,
    scannedBarcode
  } = params;
  const props = {
    // chevron: chevron,
    icon: {
      iconName: 'info',
      iconColor: simScanned ? '#00933E' : '#BA0023'
    },
    title: simScanned ? 'Sim successfully scanned!' : 'SIM scan failed',
    type: simScanned ? 'positive' : 'danger'
  };

  const takeAPhotoButton = () => {
    return /*#__PURE__*/React.createElement(View, {
      style: styles.takeAPhotoButtonContainer
    }, /*#__PURE__*/React.createElement(DuButton, {
      title: "Scan SIM again",
      type: "primary",
      onPress: () => {
        onRescanTap && onRescanTap();
      }
    }));
  };

  const enterManualyButton = () => {
    if (!simScanned) {
      return /*#__PURE__*/React.createElement(View, {
        style: styles.enterManuallyButtonContainer
      }, /*#__PURE__*/React.createElement(DuButton, {
        title: "Enter ICCID number manually",
        type: "secondary",
        onPress: () => {
          setIsModalVisible(true);
        }
      }));
    } else {
      return /*#__PURE__*/React.createElement(View, null);
    }
  };

  return /*#__PURE__*/React.createElement(View, {
    style: styles.root
  }, /*#__PURE__*/React.createElement(DuHeader, {
    safeArea: true,
    left: "back",
    leftPressed: () => {
      if (navigation.canGoBack()) {
        navigation.goBack();
      }
    },
    right: 'tertiary',
    rightTertiary: {
      disable: true,
      text: 'Help'
    },
    navToDashBoard: true,
    rightTertiarySupport: {
      pressed: () => gotoDashboardClicked()
    },
    statusBar: {
      backgroundColor: 'white',
      barStyle: 'dark-content'
    },
    background: "white"
  }), /*#__PURE__*/React.createElement(ScrollView, {
    style: [styles.subContainer]
  }, /*#__PURE__*/React.createElement(DuJumbotron, {
    mainJumbotron: "SIM barcode"
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.middleContainer
  }, /*#__PURE__*/React.createElement(DuJumbotron, {
    mainJumbotron: (_getSalesOrder$msisdn = getSalesOrder().msisdn) !== null && _getSalesOrder$msisdn !== void 0 && _getSalesOrder$msisdn.msisdn ? mask(`0${(_getSalesOrder$msisdn2 = getSalesOrder().msisdn) === null || _getSalesOrder$msisdn2 === void 0 ? void 0 : (_getSalesOrder$msisdn3 = _getSalesOrder$msisdn2.msisdn) === null || _getSalesOrder$msisdn3 === void 0 ? void 0 : _getSalesOrder$msisdn3.substring(3)}`, '999 999 99999') : '',
    overLine: "SIM 1",
    alignment: "center"
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.simCardPlaceholderContainer
  }, /*#__PURE__*/React.createElement(Image, {
    source: require('../SimActivationScan/sim-card-placeholder.png')
  }), /*#__PURE__*/React.createElement(View, {
    style: styles.simCardOverlayContainer
  }, /*#__PURE__*/React.createElement(View, null, /*#__PURE__*/React.createElement(Image, {
    source: require('../SimActivationScan/du-logo.png')
  })), /*#__PURE__*/React.createElement(View, {
    style: styles.simCardOverlayContentContainer
  }, /*#__PURE__*/React.createElement(View, null, /*#__PURE__*/React.createElement(Text, {
    style: styles.iccidNumberTitle
  }, "ICCID Number")), /*#__PURE__*/React.createElement(View, {
    style: styles.iccidNumberContainer
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.iccidNumber
  }, scannedBarcode ? scannedBarcode : 'N/A')), /*#__PURE__*/React.createElement(View, {
    style: styles.barcodeContainer
  }, /*#__PURE__*/React.createElement(Image, {
    source: require('../SimActivationScan/barcode.png')
  })), /*#__PURE__*/React.createElement(View, {
    style: styles.barcodeNumberContainer
  }, /*#__PURE__*/React.createElement(Text, {
    style: styles.barcodeNumber
  }, scannedBarcode ? scannedBarcode : '1248326122483261'))))), /*#__PURE__*/React.createElement(View, {
    style: {
      width: 340,
      marginBottom: 32
    }
  }, /*#__PURE__*/React.createElement(DuBanner, props)), simScanned ? undefined : takeAPhotoButton(), /*#__PURE__*/React.createElement(View, {
    style: {
      marginBottom: 100
    }
  }, enterManualyButton())), /*#__PURE__*/React.createElement(EnterICCIDManuallyModal, {
    isVisible: isModalVisible,
    onPressClose: () => {
      setIsModalVisible(false);
    },
    verifyAndContinue: () => {
      setIsModalVisible(false);
      onSimActivateTap && onSimActivateTap(scannedBarcode);
    }
  })), /*#__PURE__*/React.createElement(DuButtonsDock, {
    tab: true,
    items: [/*#__PURE__*/React.createElement(DuButton, {
      title: "Activate SIM",
      type: "primary",
      disabled: !simScanned,
      onPress: () => {
        onSimActivateTap && onSimActivateTap(scannedBarcode);
      }
    })]
  }));
};

const styles = StyleSheet.create({
  root: {
    flex: 1
  },
  subContainer: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 105,
    paddingVertical: 32
  },
  middleContainer: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  simCardPlaceholderContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  simCardOverlayContainer: {
    position: 'absolute',
    width: 300,
    height: 180
  },
  simCardOverlayContentContainer: {
    width: 180,
    marginTop: 17,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  iccidNumberTitle: {
    fontFamily: 'Moderat-Medium',
    fontSize: 11,
    textAlign: 'center',
    color: '#5E687D',
    lineHeight: 20
  },
  iccidNumberContainer: {
    marginTop: 8
  },
  iccidNumber: {
    fontFamily: 'Moderat-Medium',
    fontSize: 15,
    textAlign: 'center',
    color: '#041333',
    lineHeight: 24,
    letterSpacing: -0.15
  },
  barcodeContainer: {
    marginTop: 24
  },
  barcodeNumberContainer: {
    marginTop: 7
  },
  barcodeNumber: {
    fontFamily: 'Moderat-Regular',
    fontSize: 11,
    textAlign: 'center',
    color: '#233659',
    lineHeight: 20
  },
  enterManuallyButtonContainer: {
    width: 340,
    marginTop: 16
  },
  takeAPhotoButtonContainer: {
    width: 340
  }
});
export default SimCapturedScreen;
//# sourceMappingURL=SimCapturedScreen.js.map