import { DuBadge, DuButton, DuButtonsDock, DuDialog, DuHeader, DuJumbotron, DuOverlay, DuSheet } from '@du-greenfield/du-ui-toolkit';
import { MaskedText } from 'react-native-mask-text';
import React, { useEffect, useState, useCallback } from 'react';
import { Platform, StatusBar, StyleSheet, Text, View } from 'react-native';
import { DuAnalyticsTagBuilder } from '@du-greenfield/du-analytics-plugin-core';
import DuAnalyticsFirebasePlugin from '@du-greenfield/du-firebase-analytics';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import screen from '../../../api/services/Order';
import { addExistingOrderItemUnderParent, getBssListProductOfferings, lockPhoneNumber, reservePhoneNumber } from '../../../utils';
import { useDispatch, useSelector } from 'react-redux';
import { phoneNumberSelected } from '../../../redux/features/phoneNumbersSlice';
import { AmountDockItem } from './AmountDockItem';
import { getSalesOrder } from '../../../';

const CustomerYourAssignedNumberScreen = _ref => {
  var _orderScreenDynamics$, _orderScreenDynamics$2, _orderScreenDynamics$3, _phoneNumbers$phoneNu, _phoneNumbers$phoneNu2, _phoneNumbers$phoneNu3, _phoneNumbers$phoneNu4;

  let {
    navigation,
    numberSelectionComplete,
    onBrowseMoreTap,
    rootNavigation
  } = _ref;
  const [overlayVisibility, setOverlayVisibility] = useState(false);
  const [orderScreenDynamics, setOrderScreenDynamics] = useState(undefined);
  const [phoneNumbersPayload, setPhoneNumbersPayload] = useState(undefined);
  const [phoneNumberLocked, setPhoneNumberLocked] = useState(false);
  const safeAreaInsets = useSafeAreaInsets();
  const [showSheet, setShowSheet] = useState(false);
  const [error, setError] = useState();
  const phoneNumbers = useSelector(state => state.phoneNumberSlice);
  const dispatch = useDispatch();

  async function fetchData() {
    setPhoneNumbersPayload(undefined);
    setOrderScreenDynamics(await screen.getScreenDynamics());
    await lockPhoneNumberOnScreen();
  }

  async function lockPhoneNumberOnScreen() {
    let response = await lockPhoneNumber(getSalesOrder().customer.id, 1, []);

    if (response !== null && response !== void 0 && response.errorCodes && (response === null || response === void 0 ? void 0 : response.errorCodes.length) > 0) {
      setError(response === null || response === void 0 ? void 0 : response.errorCodes[0]);
      setOverlayVisibility(true);
      return;
    }

    if ((response === null || response === void 0 ? void 0 : response.status) === 'SUCCESS') {
      var _response$phoneNumber;

      setShowSheet(response.phoneNumber.length === 0);
      setPhoneNumbersPayload(response);
      const reservePhoneNumberResponse = await reservePhoneNumber(getSalesOrder().customer.id, [response === null || response === void 0 ? void 0 : (_response$phoneNumber = response.phoneNumber[0]) === null || _response$phoneNumber === void 0 ? void 0 : _response$phoneNumber.id], getSalesOrder().salesOrderId);

      if ((reservePhoneNumberResponse === null || reservePhoneNumberResponse === void 0 ? void 0 : reservePhoneNumberResponse.status) === 'SUCCESS') {
        setPhoneNumbersPayload(response);
        dispatch(phoneNumberSelected({
          id: response.phoneNumber[0].id,
          phoneNumber: response.phoneNumber[0].phoneNumber
        }));

        for (let phoneNumberObject of response.phoneNumber) {
          if (phoneNumberObject.status === 'LOCKED') {
            setPhoneNumberLocked(true);
          }
        }
      }
    }
  }

  const sheetProps = {
    isVisible: showSheet,
    onClose: () => setShowSheet(false),
    standard: {
      title: "We're sorry, there are no available numbers",
      body: 'We’re sorry, there are no available numbers\n  Please try again later.',
      icon: {
        iconName: 'search-off',
        iconColor: '#BA0023',
        artWorkHeight: 36,
        artWorkWidth: 36
      },
      buttons: [{
        text: 'Try again',
        type: 'primary',
        onPress: () => {
          setShowSheet(false);
          fetchData();
        }
      }]
    }
  };
  useEffect(() => {
    fetchData(); // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const analyticLogScreenView = useCallback(() => {
    let analytics = new DuAnalyticsFirebasePlugin();
    let tagBuilder = new DuAnalyticsTagBuilder();
    let params = tagBuilder.pageName('Assign random number').journeyName('GF Explore').pageChannel('Customer App').previousScreenName('Lines section').screenType('pdp').screenHierarchy('Homepage: pdp: Family plan: Assign random number').screenBreadcrumb('Family plan: Assign random number').customerType('new').visitorStatus('returning').recency('5d').customerLoginStatus('logged in').customerEligibility('GF eligible').customerSegment('consumer').productTargetSegment('Prospect').build();
    analytics.logScreenView('GF explore', 'Assign random number', params);
  }, []);
  useEffect(() => {
    analyticLogScreenView();
  }, [analyticLogScreenView]);

  const analyticLogPressEligibilityCheck = () => {
    let analytics = new DuAnalyticsFirebasePlugin();
    analytics.logEvent('Continue_To_Eligibility_Check_Button_Click', {
      event_category: 'Continue to eligibility check button',
      event_label: 'Continue to eligibility check',
      event_name: 'Assigned a number: Continue to eligibility check',
      event_action: 'Continue to eligibility check button Click'
    });
  };

  const analyticLogPressBrowseNumbers = () => {
    let analytics = new DuAnalyticsFirebasePlugin();
    analytics.logEvent('Browse_More_Numbers_Button_Click', {
      event_category: 'Browse more numbers button',
      event_label: 'Browse more numbers',
      event_name: 'Assigned a number: Browse more numbers',
      event_action: 'Browse more numbers button Click'
    });
  };

  const headerProps = {
    left: 'back',
    leftTertiaryText: 'Close',
    safeArea: true,
    leftPressed: () => {
      Platform.OS === 'android' && StatusBar.setBackgroundColor('white');

      if (Platform.OS === 'android') {
        StatusBar.setBarStyle('dark-content', true);
      }

      if (navigation.canGoBack()) {
        navigation.goBack();
      } else {
        if (rootNavigation.canGoBack()) {
          rootNavigation.goBack();
        }
      }
    },
    right: 'tertiary',
    rightTertiary: {
      text: 'Help',
      disable: true
    },
    statusBar: {
      barStyle: 'dark-content',
      backgroundColor: 'white'
    },
    background: 'white'
  };

  if (!phoneNumbersPayload) {
    return /*#__PURE__*/React.createElement(View, {
      style: {
        width: '100%',
        height: '100%',
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
      }
    }, /*#__PURE__*/React.createElement(Text, null, "..."));
  }

  const finishNumberSelection = async () => {
    const productIdRes = await getBssListProductOfferings();

    if (productIdRes) {
      var id = productIdRes.bssListProductOfferings[0].id;
      await addExistingOrderItemUnderParent(getSalesOrder().orderItemId || '', id, phoneNumbersPayload === null || phoneNumbersPayload === void 0 ? void 0 : phoneNumbersPayload.phoneNumber[0].id);
      numberSelectionComplete && numberSelectionComplete(getSalesOrder(), {
        msisdn: phoneNumbersPayload.phoneNumber[0].phoneNumber
      });
    }
  };

  return /*#__PURE__*/React.createElement(View, {
    style: styles.root
  }, Platform.OS === 'ios' ? /*#__PURE__*/React.createElement(View, {
    style: styles.iosStackHeaderPadding
  }) : /*#__PURE__*/React.createElement(View, {
    style: {
      marginBottom: safeAreaInsets.top
    }
  }), /*#__PURE__*/React.createElement(DuHeader, headerProps), /*#__PURE__*/React.createElement(View, {
    style: styles.jumbotronContainer
  }, /*#__PURE__*/React.createElement(DuJumbotron, {
    alignment: 'left',
    mainJumbotron: orderScreenDynamics === null || orderScreenDynamics === void 0 ? void 0 : (_orderScreenDynamics$ = orderScreenDynamics.fields.assigned_number_model.mapValue.fields.heading.mapValue.fields.en.mapValue.fields.plain_text) === null || _orderScreenDynamics$ === void 0 ? void 0 : _orderScreenDynamics$.stringValue,
    contanierStyle: styles.jumbotronContainerStyle,
    size: 'xxlarge'
  })), /*#__PURE__*/React.createElement(View, {
    style: styles.subContainer
  }, /*#__PURE__*/React.createElement(View, {
    style: [styles.numberAndButtonContainer // eslint-disable-next-line react-native/no-inline-styles
    ]
  }, /*#__PURE__*/React.createElement(View, {
    style: styles.badgeAndButtonContainer
  }, /*#__PURE__*/React.createElement(DuBadge, {
    title: orderScreenDynamics === null || orderScreenDynamics === void 0 ? void 0 : (_orderScreenDynamics$2 = orderScreenDynamics.fields.assigned_number_model.mapValue.fields.lines.arrayValue.values[0].mapValue.fields.label.mapValue.fields.en.mapValue.fields.plain_text) === null || _orderScreenDynamics$2 === void 0 ? void 0 : _orderScreenDynamics$2.stringValue // eslint-disable-next-line react-native/no-inline-styles
    // backgroundStyle={expired && { backgroundColor: 'transparent' }}

  }), /*#__PURE__*/React.createElement(DuButton, {
    type: "teritary",
    title: orderScreenDynamics === null || orderScreenDynamics === void 0 ? void 0 : (_orderScreenDynamics$3 = orderScreenDynamics.fields.assigned_number_model.mapValue.fields.lines.arrayValue.values[0].mapValue.fields.shuffle.mapValue.fields.en.mapValue.fields.plain_text) === null || _orderScreenDynamics$3 === void 0 ? void 0 : _orderScreenDynamics$3.stringValue,
    size: "small",
    disabled: true,
    icon: {
      iconName: 'refresh'
    },
    containerStyle: styles.shuffleButton,
    onPress: () => {
      // setExpired(false);
      lockPhoneNumberOnScreen();
    }
  })), /*#__PURE__*/React.createElement(View, {
    style: styles.numberContainer
  }, /*#__PURE__*/React.createElement(MaskedText, {
    style: styles.number,
    mask: "999 999 9999"
  }, (phoneNumbers === null || phoneNumbers === void 0 ? void 0 : (_phoneNumbers$phoneNu = phoneNumbers.phoneNumber) === null || _phoneNumbers$phoneNu === void 0 ? void 0 : (_phoneNumbers$phoneNu2 = _phoneNumbers$phoneNu.phoneNumber) === null || _phoneNumbers$phoneNu2 === void 0 ? void 0 : _phoneNumbers$phoneNu2.length) > 0 ? `0${phoneNumbers === null || phoneNumbers === void 0 ? void 0 : (_phoneNumbers$phoneNu3 = phoneNumbers.phoneNumber) === null || _phoneNumbers$phoneNu3 === void 0 ? void 0 : (_phoneNumbers$phoneNu4 = _phoneNumbers$phoneNu3.phoneNumber) === null || _phoneNumbers$phoneNu4 === void 0 ? void 0 : _phoneNumbers$phoneNu4.substring(3)}` : '')), /*#__PURE__*/React.createElement(View, {
    style: styles.browsMoreButton
  }, /*#__PURE__*/React.createElement(DuButton, {
    type: "secondary",
    title: "Browse numbers",
    size: "small",
    onPress: () => {
      analyticLogPressBrowseNumbers();
      onBrowseMoreTap && onBrowseMoreTap(false);
    }
  })))), /*#__PURE__*/React.createElement(DuSheet, sheetProps), /*#__PURE__*/React.createElement(DuButtonsDock, {
    shadow: true,
    items: [/*#__PURE__*/React.createElement(AmountDockItem, {
      title: 'Total',
      titleValue: getSalesOrder().product.plan.price.currencyCode + ' ' + getSalesOrder().product.plan.price.amount.toFixed(2),
      captionValue: 'Incl 5% VAT',
      caption: getSalesOrder().product.name
    }), /*#__PURE__*/React.createElement(DuButton, {
      title: "Continue to eligibility check",
      type: "primary",
      onPress: () => {
        analyticLogPressEligibilityCheck();

        if (phoneNumbersPayload) {
          finishNumberSelection();
        }
      },
      disabled: !phoneNumberLocked
    })]
  }), /*#__PURE__*/React.createElement(DuOverlay, {
    isVisible: overlayVisibility,
    overlayStyle: styles.overlay,
    onBackdropPress: () => {
      setOverlayVisibility(false);
    }
  }, /*#__PURE__*/React.createElement(DuDialog, {
    headline: '',
    body: error === null || error === void 0 ? void 0 : error.message,
    primaryText: 'Ok',
    icon: {
      artWorkWidth: 29,
      artWorkHeight: 26,
      iconName: 'warning',
      iconColor: '#F5C311'
    },
    pressedPrimary: () => {
      setOverlayVisibility(false);
    }
  })));
};

export default CustomerYourAssignedNumberScreen;
const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: 'white'
  },
  subContainer: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 16
  },
  iosStackHeaderPadding: {
    marginBottom: 14
  },
  grayBanner: {
    marginTop: 24,
    padding: 16,
    backgroundColor: '#F5F6F7',
    borderRadius: 12,
    flexDirection: 'row',
    alignItems: 'center'
  },
  grayBannerTitle: {
    fontFamily: 'Moderat-Bold',
    fontSize: 16,
    textAlign: 'left',
    letterSpacing: -0.2,
    lineHeight: 20,
    color: '#041333'
  },
  grayBannerDecription: {
    marginTop: 8,
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    textAlign: 'left',
    lineHeight: 20,
    color: '#5E687D'
  },
  purple: {
    color: '#753BBD'
  },
  numberAndButtonContainer: {
    // marginTop: 24,
    padding: 16,
    borderColor: '#D7D9DE',
    borderWidth: 1,
    borderRadius: 12
  },
  badgeAndButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 24,
    alignItems: 'center'
  },
  shuffleButton: {
    paddingHorizontal: 0
  },
  numberContainer: {
    marginTop: 8
  },
  number: {
    fontFamily: 'Moderat-Bold',
    fontSize: 22,
    textAlign: 'left',
    letterSpacing: -0.3,
    lineHeight: 26,
    color: '#041333'
  },
  browsMoreButton: {
    marginTop: 32,
    flexDirection: 'row'
  },
  overlay: {
    paddingHorizontal: 16,
    backgroundColor: 'transparent',
    elevation: 0
  },
  jumbotronContainer: {
    marginTop: 16,
    marginBottom: 18,
    width: '90%'
  },
  jumbotronContainerStyle: {
    padding: 16
  }
});
//# sourceMappingURL=YourAssignedNumberScreen.js.map