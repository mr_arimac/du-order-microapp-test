import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
export const AmountDockItem = props => {
  const {
    title,
    titleValue,
    caption,
    captionValue
  } = props;
  return /*#__PURE__*/React.createElement(View, {
    style: amount.container
  }, /*#__PURE__*/React.createElement(View, {
    style: amount.subContainer
  }, /*#__PURE__*/React.createElement(Text, {
    style: amount.title
  }, title), /*#__PURE__*/React.createElement(Text, {
    style: amount.title
  }, titleValue)), /*#__PURE__*/React.createElement(View, {
    style: [amount.subContainer, // eslint-disable-next-line react-native/no-inline-styles
    {
      justifyContent: caption ? 'space-between' : 'flex-end'
    }]
  }, caption ? /*#__PURE__*/React.createElement(Text, {
    style: amount.captionLeft
  }, caption) : null, /*#__PURE__*/React.createElement(Text, {
    style: amount.captionRight
  }, captionValue)));
};
const amount = StyleSheet.create({
  container: {
    flexDirection: 'column',
    marginBottom: 8
  },
  subContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  title: {
    fontFamily: 'Moderat-Bold',
    fontSize: 16,
    fontWeight: '700',
    color: '#121212',
    lineHeight: 20,
    letterSpacing: -0.2
  },
  captionLeft: {
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    fontWeight: '400',
    color: '#5E687D',
    lineHeight: 20
  },
  captionRight: {
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    fontWeight: '400',
    color: '#737B8D',
    lineHeight: 20
  }
});
//# sourceMappingURL=AmountDockItem.js.map