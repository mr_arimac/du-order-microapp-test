export * from './gql';
export * from './storage';
export function addZero(elemet, size) {
  var s = String(elemet);

  while (s.length < (size || 2)) {
    s = '0' + s;
  }

  return s;
}
//# sourceMappingURL=index.js.map