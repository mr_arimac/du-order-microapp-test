import { gqlClient } from './gql_client';
import { ADD_EXISTING_ORDER_ITEM, ADD_EXISTING_ORDER_ITEM_UNDER_PARENT, LOCK_PHONE_NUMBER, RELEASE_PHONE_NUMBERS, RESERVE_PHONE_NUMBER, SUBMIT_SALES_ORDER, UPDATE_ORDER_ITEM, UPDATE_SALES_ORDER } from './mutations';
import { BSS_LIST_PRODUCT_OFFERINGS, GET_SALSE_ORDER } from './queries';
export async function lockPhoneNumber(customerId, qty, excludeNumberIds) {
  try {
    var _response$jsonBody$da;

    const response = await gqlClient().mutation({
      gql: LOCK_PHONE_NUMBER,
      variables: {
        input: {
          quantity: qty,
          customerId: customerId,
          excludeNumberIds: excludeNumberIds
        }
      }
    });
    console.log('res------111', response);
    return Promise.resolve((_response$jsonBody$da = response.jsonBody.data) === null || _response$jsonBody$da === void 0 ? void 0 : _response$jsonBody$da.lockPhoneNumber);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}
export async function reservePhoneNumber(customerId, reserveNumberIds, contextId) {
  try {
    var _response$jsonBody$da2;

    const response = await gqlClient().mutation({
      gql: RESERVE_PHONE_NUMBER,
      variables: {
        input: {
          unlockForContext: true,
          customerId: customerId,
          reserveNumberIds: reserveNumberIds,
          contextId: contextId
        }
      }
    });
    return Promise.resolve((_response$jsonBody$da2 = response.jsonBody.data) === null || _response$jsonBody$da2 === void 0 ? void 0 : _response$jsonBody$da2.reservePhoneNumber);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}
export async function releasePhoneNumbers(customerId, salesOrderId, releasePhoneNumberId) {
  try {
    var _response$jsonBody$da3;

    const response = await gqlClient().mutation({
      gql: RELEASE_PHONE_NUMBERS,
      variables: {
        input: {
          customerId: customerId,
          releaseForContext: true,
          releaseForCustomer: true,
          contextId: salesOrderId,
          releaseNumberIds: [releasePhoneNumberId]
        }
      }
    });
    return Promise.resolve((_response$jsonBody$da3 = response.jsonBody.data) === null || _response$jsonBody$da3 === void 0 ? void 0 : _response$jsonBody$da3.releasePhoneNumbers);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}
export async function addExistingOrderItem(parentSalesOrderId, locationId, productOfferingId) {
  try {
    const response = await gqlClient().mutation({
      gql: ADD_EXISTING_ORDER_ITEM,
      variables: {
        input: {
          parentSalesOrderId: parentSalesOrderId,
          locationId: locationId,
          productOffering: {
            id: productOfferingId,
            characteristics: []
          }
        }
      }
    });
    return Promise.resolve(response.jsonBody.data);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}
export async function addExistingOrderItemUnderParent(orderItemId, productOfferingId, value) {
  try {
    var _response$jsonBody$da4;

    const response = await gqlClient().mutation({
      gql: ADD_EXISTING_ORDER_ITEM_UNDER_PARENT,
      variables: {
        input: {
          orderItemId: orderItemId,
          productOffering: {
            id: productOfferingId,
            characteristics: {
              name: 'MSISDN',
              value: value
            }
          }
        }
      }
    });
    return Promise.resolve((_response$jsonBody$da4 = response.jsonBody.data) === null || _response$jsonBody$da4 === void 0 ? void 0 : _response$jsonBody$da4.addOrderItemUnderParent);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}
export async function getSalesOrder(id, reloadShopCart) {
  try {
    var _response$jsonBody$da5;

    const response = await gqlClient().query({
      gql: GET_SALSE_ORDER,
      variables: {
        input: {
          id: id,
          reloadShopCart: reloadShopCart
        }
      }
    });
    return Promise.resolve((_response$jsonBody$da5 = response.jsonBody.data) === null || _response$jsonBody$da5 === void 0 ? void 0 : _response$jsonBody$da5.bssGetSalesOrder);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}
export async function updateOrderItems(salesOrderId, simCardOrderId) {
  try {
    var _response$jsonBody$da6;

    const response = await gqlClient().query({
      gql: UPDATE_SALES_ORDER,
      variables: {
        input: {
          salesOrderId: salesOrderId,
          itemCharacteristics: {
            orderItemId: simCardOrderId,
            characteristics: {
              name: 'SIM card type',
              value: 'eSIM'
            }
          }
        }
      }
    });
    return Promise.resolve((_response$jsonBody$da6 = response.jsonBody.data) === null || _response$jsonBody$da6 === void 0 ? void 0 : _response$jsonBody$da6.updateOrderItems);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}
export async function getBssListProductOfferings() {
  try {
    const response = await gqlClient().query({
      gql: BSS_LIST_PRODUCT_OFFERINGS,
      variables: {}
    });
    return Promise.resolve(response.jsonBody.data);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}
export async function updateOrderItem(salesOrderId, orderItemId, msisdn) {
  try {
    const response = await gqlClient().mutation({
      gql: UPDATE_ORDER_ITEM,
      variables: {
        input: {
          salesOrderId: salesOrderId,
          itemCharacteristics: {
            orderItemId: orderItemId,
            characteristics: {
              name: 'ICCID',
              value: msisdn
            }
          }
        }
      }
    });
    return Promise.resolve(response.jsonBody.data);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}
export async function submitSalesOrder(input) {
  let gql = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : SUBMIT_SALES_ORDER;

  try {
    const response = await gqlClient().mutation({
      gql: gql,
      variables: {
        input: input
      }
    });
    return Promise.resolve(response.jsonBody.data);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}
export async function getCustomer(id, gql) {
  try {
    const response = await gqlClient().mutation({
      gql: gql,
      variables: {
        id: id
      }
    });
    return Promise.resolve(response.jsonBody.data.bssGetCustomer);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}
export async function getListProductOffering(gql) {
  try {
    const response = await gqlClient().mutation({
      gql: gql
    });
    return Promise.resolve(response);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}
//# sourceMappingURL=index.js.map