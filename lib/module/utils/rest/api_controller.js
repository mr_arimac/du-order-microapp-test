import { duRequest } from '@du-greenfield/du-rest';
export async function request(config) {
  return new Promise(resolve => {
    duRequest(config).then(value => {
      console.log('data', value.data);
      return resolve(value.data);
    }).catch(e => {
      console.log(e);
      return resolve(undefined);
    });
  });
}
//# sourceMappingURL=api_controller.js.map