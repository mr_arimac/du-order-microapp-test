import { createContext } from 'react';
import { initialState } from './context-types';
import { browseMoreReducer } from './reducer';
import { load, save } from '../utils/storage';
export const OrderAppContext = /*#__PURE__*/createContext({
  state: initialState,
  dispatch: () => null
});
export async function getAppState() {
  return new Promise(resolve => {
    load('ORDER_APP_STATE').then(value => {
      if (value && value !== '') {
        resolve(value);
      } else {
        resolve(initialState);
      }
    });
  });
}
export async function setAppState(state) {
  save('ORDER_APP_STATE', state);
}
export const mainReducer = action => {
  console.log('action.payload main', action.payload);
  const reducer = {
    browseMore: browseMoreReducer(initialState.browseMore, action)
  };
  setAppState(reducer);
  return reducer;
};
//# sourceMappingURL=app-context.js.map