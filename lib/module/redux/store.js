import { configureStore } from '@reduxjs/toolkit';
import phoneNumberSlice from './features/phoneNumbersSlice';
export default configureStore({
  reducer: {
    phoneNumberSlice: phoneNumberSlice
  }
});
//# sourceMappingURL=store.js.map