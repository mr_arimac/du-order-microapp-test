function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import BrowseMoreNumberScreen from '../screens/Dealer/BrowseMoreNumbers/BrowseMoreNumberScreen';
import ActivatingSIMScreen from '../screens/Dealer/ActivatingSIM/ActivatingSIMScreen';
import SimActivationScanScreen from '../screens/Dealer/SimActivationScan/SimActivationScanScreen';
import YourAssignedNumberScreen from '../screens/Dealer/YourAssignedNumber/YourAssignedNumberScreen';
import BarcodeScannerScreen from '../screens/Dealer/BarcodeScanner/BarcodeScannerScreen';
import SimCapturedScreen from '../screens/Dealer/SimCapruredScreen/SimCapturedScreen';
export let DuOrderDealerInitialScreen;

(function (DuOrderDealerInitialScreen) {
  DuOrderDealerInitialScreen["YOUR_ASSIGNED_NUMBER"] = "YourAssignedNumberScreen";
  DuOrderDealerInitialScreen["BROWSE_MORE_NUMBERS"] = "BrowseMoreNumberScreen";
  DuOrderDealerInitialScreen["SIM_ACTIVATION_SCAN"] = "SimActivationScanScreen";
  DuOrderDealerInitialScreen["BARCODE_SCANNER"] = "BarcodeScannerScreen";
  DuOrderDealerInitialScreen["ACTIVATING_SIM"] = "ActivatingSIMScreen";
  DuOrderDealerInitialScreen["SIM_CAPTURED"] = "SimCapturedScreen";
})(DuOrderDealerInitialScreen || (DuOrderDealerInitialScreen = {}));

const Stack = createNativeStackNavigator();
export const DealerAppStack = props => {
  return /*#__PURE__*/React.createElement(Stack.Navigator, {
    screenListeners: {
      state: e => {
        props.globalEventListener && props.globalEventListener('screen_state_changes', e);
      }
    },
    screenOptions: {
      headerShown: false
    },
    initialRouteName: props.initalScreen
  }, /*#__PURE__*/React.createElement(Stack.Screen, {
    name: "YourAssignedNumberScreen"
  }, screenProps => /*#__PURE__*/React.createElement(YourAssignedNumberScreen, _extends({}, props, screenProps))), /*#__PURE__*/React.createElement(Stack.Screen, {
    name: "BrowseMoreNumberScreen"
  }, screenProps => /*#__PURE__*/React.createElement(BrowseMoreNumberScreen, _extends({}, props, screenProps))), /*#__PURE__*/React.createElement(Stack.Screen, {
    name: "SimActivationScanScreen"
  }, screenProps => /*#__PURE__*/React.createElement(SimActivationScanScreen, _extends({}, props, screenProps))), /*#__PURE__*/React.createElement(Stack.Screen, {
    name: "ActivatingSIMScreen"
  }, screenProps => /*#__PURE__*/React.createElement(ActivatingSIMScreen, _extends({}, props, screenProps, {
    cancel: () => {//do nothing
    }
  }))), /*#__PURE__*/React.createElement(Stack.Screen, {
    name: "BarcodeScannerScreen"
  }, screenProps => /*#__PURE__*/React.createElement(BarcodeScannerScreen, _extends({}, props, screenProps))), /*#__PURE__*/React.createElement(Stack.Screen, {
    name: "SimCapturedScreen"
  }, screenProps => /*#__PURE__*/React.createElement(SimCapturedScreen, _extends({}, props, screenProps))));
};
//# sourceMappingURL=dealer-app-navigator.js.map