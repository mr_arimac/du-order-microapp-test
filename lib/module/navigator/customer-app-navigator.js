function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import BrowseMoreNumberScreen from '../screens/Customer/BrowseMoreNumbers/BrowseMoreNumberScreen';
import YourAssignedNumberScreen from '../screens/Customer/YourAssignedNumber/YourAssignedNumberScreen';
export let CustomerInitialScreen;

(function (CustomerInitialScreen) {
  CustomerInitialScreen["YOUR_ASSIGNED_NUMBER"] = "YourAssignedNumberScreen";
  CustomerInitialScreen["BROWSE_MORE_NUMBERS"] = "BrowseMoreNumberScreen";
})(CustomerInitialScreen || (CustomerInitialScreen = {}));

const Stack = createNativeStackNavigator();
export const CustomerAppStack = stackProps => {
  return /*#__PURE__*/React.createElement(Stack.Navigator, {
    screenListeners: {
      state: e => {
        stackProps.globalEventListener && stackProps.globalEventListener('screen_state_changes', e);
      }
    },
    screenOptions: {
      headerShown: false
    },
    initialRouteName: stackProps.initialScreen
  }, /*#__PURE__*/React.createElement(Stack.Screen, {
    name: "YourAssignedNumberScreen"
  }, screenProps => /*#__PURE__*/React.createElement(YourAssignedNumberScreen, _extends({}, stackProps, screenProps))), /*#__PURE__*/React.createElement(Stack.Group, {
    screenOptions: {
      presentation: 'modal'
    }
  }, /*#__PURE__*/React.createElement(Stack.Screen, {
    name: "BrowseMoreNumberScreen"
  }, screenProps => /*#__PURE__*/React.createElement(BrowseMoreNumberScreen, _extends({}, stackProps, screenProps)))));
};
//# sourceMappingURL=customer-app-navigator.js.map