import { CustomerLogin } from '@du-greenfield/du-auth-helper';
import type { GraphQLClient } from '@du-greenfield/du-microapps-core';
import type { Request, Response } from '@du-greenfield/du-native-gql-plugin';
import DuGraphQLClient from '@du-greenfield/du-native-gql-plugin';
import { getMicroAppContext } from './App';

export default class CommongGraphQLClient implements GraphQLClient {
  async getHeaders() {
    return {
      Authorization: getMicroAppContext().tokens.access_token,
      user_token: getMicroAppContext().tokens.id_token,
      client_id: getMicroAppContext().graphQLConfig.client_id,
    };
  }

  async query(request: Request): Promise<Response> {
    var primaryResponse = await new DuGraphQLClient({
      url: getMicroAppContext().graphQLConfig.base_url,
      headers: await this.getHeaders(),
    }).query(request);
    if (primaryResponse.code == 401) {
      var accessToken = getMicroAppContext().tokens.access_token;
      var refreshToken = getMicroAppContext().tokens.refresh_token;
      var tokenRes = await new CustomerLogin({
        base_url: getMicroAppContext().restConfig.base_url,
        client_id: getMicroAppContext().restConfig.client_id,
        client_secret: getMicroAppContext().restConfig.client_secret,
      }).renewAccessToken(accessToken, refreshToken!);
      if (tokenRes.success) {
        var tokens = tokenRes.tokens!;
        getMicroAppContext().tokens.access_token =
          'Bearer ' + tokens.access_token;
        getMicroAppContext().tokens.id_token = tokens.id_token;
        getMicroAppContext().tokens.refresh_token = tokens.refresh_token;
        var secondaryResponse = await new DuGraphQLClient({
          url: getMicroAppContext().graphQLConfig.base_url,
          headers: {
            Authorization: getMicroAppContext().tokens.access_token,
            user_token: getMicroAppContext().tokens.id_token,
            client_id: getMicroAppContext().graphQLConfig.client_id,
          },
        }).query({
          url: request.url,
          gql: request.gql,
          variables: request.variables,
        });
        return secondaryResponse;
      } else {
        return {
          code: tokenRes.statusCode,
          jsonBody: undefined,
          rawBody: 'Token refresh failed',
        };
      }
    } else {
      return primaryResponse;
    }
  }

  async mutation(request: Request): Promise<Response> {
    var primaryResponse = await new DuGraphQLClient({
      url: getMicroAppContext().graphQLConfig.base_url,
      headers: await this.getHeaders(),
    }).mutation(request);
    if (primaryResponse.code == 401) {
      var accessToken = getMicroAppContext().tokens.access_token;
      var refreshToken = getMicroAppContext().tokens.refresh_token;
      var tokenRes = await new CustomerLogin({
        base_url: getMicroAppContext().restConfig.base_url,
        client_id: getMicroAppContext().restConfig.client_id,
        client_secret: getMicroAppContext().restConfig.client_secret,
      }).renewAccessToken(accessToken, refreshToken!);
      if (tokenRes.success) {
        var tokens = tokenRes.tokens!;
        getMicroAppContext().tokens.access_token =
          'Bearer ' + tokens.access_token;
        getMicroAppContext().tokens.id_token = tokens.id_token;
        getMicroAppContext().tokens.refresh_token = tokens.refresh_token;
        var secondaryResponse = await new DuGraphQLClient({
          url: getMicroAppContext().graphQLConfig.base_url,
          headers: {
            Authorization: getMicroAppContext().tokens.access_token,
            user_token: getMicroAppContext().tokens.id_token,
            client_id: getMicroAppContext().graphQLConfig.client_id,
          },
        }).mutation({
          url: request.url,
          gql: request.gql,
          variables: request.variables,
        });
        return secondaryResponse;
      } else {
        return {
          code: tokenRes.statusCode,
          jsonBody: undefined,
          rawBody: 'Token refresh failed',
        };
      }
    } else {
      return primaryResponse;
    }
  }
}
