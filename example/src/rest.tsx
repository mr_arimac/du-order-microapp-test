import type { RestClient } from '@du-greenfield/du-microapps-core';
import type {
  FormRequest,
  Response,
  JsonRequest,
  RawRequest,
  MultipartRequest,
} from '@du-greenfield/du-native-rest-plugin';
import DuRestClient from '@du-greenfield/du-native-rest-plugin';
import { getMicroAppContext } from './App';

export default class CommonRestClient implements RestClient {
  encodedFormRequest(request: FormRequest): Promise<Response> {
    return new DuRestClient({
      baseUrl: getMicroAppContext().restConfig.base_url,
    }).encodedFormRequest(request);
  }
  jsonRequest(request: JsonRequest): Promise<Response> {
    return new DuRestClient({
      baseUrl: getMicroAppContext().restConfig.base_url,
    }).jsonRequest(request);
  }
  rawRequest(request: RawRequest): Promise<Response> {
    return new DuRestClient({
      baseUrl: getMicroAppContext().restConfig.base_url,
    }).rawRequest(request);
  }
  multipartFormRequest(request: MultipartRequest): Promise<Response> {
    return new DuRestClient({
      baseUrl: getMicroAppContext().restConfig.base_url,
    }).multipartFormRequest(request);
  }
}
