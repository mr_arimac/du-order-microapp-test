import * as React from 'react';
import type { MicroAppContext } from '@du-greenfield/du-microapps-core';
import CommonRestClient from './rest';
import CommongGraphQLClient from './graphql';
import { useEffect, useState } from 'react';
import { CustomerLogin, DealerLogin } from '@du-greenfield/du-auth-helper';
import { Text, View } from 'react-native';
import DuOrder, {
  DuOrderDealerInitialScreen,
  CustomerInitialScreen,
} from '../../src/';
import type { Msisdn, Order, Tokens } from '@du-greenfield/du-commons';

var microAppContext: MicroAppContext;

const isCustomerApp = false;

export function getMicroAppContext(): MicroAppContext {
  var base;
  var clientId;
  var clientSecret;
  if (isCustomerApp) {
    // base = 'https://staging-dugfnonprod.menalab.corp.local:9105';
    base = 'https://mirror-dugfnonprod.menalab.corp.local:9106';
    clientId = 'HX0joq0Q1TW0hCBziXqva4UoAgOwhmIj';
    clientSecret = 'czX9YaVRnHqWAF3M';
  } else {
    base =
      'https://ppm-intapi-ppm-ngil-tibco-mashery.apps.ocp-lab.menalab.corp.local';
    clientId = '8bbphswq6r5vmjm8d9wca68x';
    clientSecret = 'dHSkGSP9dQ';
  }

  if (!microAppContext) {
    microAppContext = {
      graphQLConfig: {
        base_url: `${base}/ngil/v1/graphql/`,
        client_id: clientId,
      },
      restConfig: {
        base_url: base,
        client_id: clientId,
        client_secret: clientSecret,
      },
      appType: isCustomerApp ? 'customer' : 'dealer',
      appData: {},
      userData: { ca_id: '9162998311113728284' },
      tokens: {
        access_token: '',
        scope: '',
        id_token: '',
        token_type: '',
        expires_in: 0,
        refresh_token: '',
        token_mode: 'pre-login',
      },
      restClient: () => new CommonRestClient(),
      graphQLClinet: () => new CommongGraphQLClient(),
    };
  }
  return microAppContext;
}

export default function App() {
  const [loading, setLoading] = useState<boolean>(true);

  async function getPreLoginToken() {
    var authClient;
    if (isCustomerApp) {
      authClient = new CustomerLogin({
        base_url: getMicroAppContext().restConfig.base_url,
        client_id: getMicroAppContext().restConfig.client_id,
        client_secret: getMicroAppContext().restConfig.client_secret,
      });
    } else {
      authClient = new DealerLogin({
        base_url: getMicroAppContext().restConfig.base_url,
        client_id: getMicroAppContext().restConfig.client_id,
        client_secret: getMicroAppContext().restConfig.client_secret,
      });
    }
    var tokenRes;
    if (isCustomerApp) {
      tokenRes = await authClient.getPreLoginToken();
    } else {
      tokenRes = await authClient.login('salesagents2', 'U@nm4jdw');
    }
    if (tokenRes.success) {
      var tokens = tokenRes.tokens as Tokens;
      getMicroAppContext().tokens = {
        ...tokens,
        access_token: 'Bearer ' + tokens.access_token,
      };
      setLoading(false);
    }
  }

  useEffect(() => {
    getPreLoginToken();
  }, []);

  if (loading) {
    return (
      <View
        style={{
          width: '100%',
          height: '100%',
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Text>Loading...</Text>
      </View>
    );
  }

  return (
    <DuOrder
      dealerInitialScreen={DuOrderDealerInitialScreen.ACTIVATING_SIM}
      customerInitialScreen={CustomerInitialScreen.YOUR_ASSIGNED_NUMBER}
      appContext={getMicroAppContext()}
      salesOrder={{
        type: 'payg',
        localId: '',
        salesOrderId: '9163331736613887208',
        orderItemId: '',
        productOffering: { id: '' },
        product: {
          type: 'payg',
          id: 'payg',
          name: '',
          plan: {
            data: '0',
            voice: '0',
            duration: '0',
            price: { type: 'payg', amount: 0, tax: 0, currencyCode: 'AED' },
          },
        },
        customer: {
          type: 'anon',
          id: '9163331737513887208',
          firstName: 'Faaris Khan Mohammad',
          lastName: 'Faaris Khan Mohammad',
          accountNumber: '1000164311',
          customerLocations: [{ id: '9163331733613887208' }],
          email: 'faaris.khanmohammad11111@hotmail.com',
          eid: '7843722233331115736',
          nationality: '',
          dob: '',
          eidExpiryDate: '',
        },
        simCardOrderItemId: '9163331738813887208',
        msisdn: { msisdn: '971123456789' },
      }}
      numberSelectionComplete={(order: Order, msisdn: Msisdn) => {
        console.log(
          'numberSelectionComplete ' +
            JSON.stringify(order) +
            ' ' +
            JSON.stringify(msisdn)
        );
      }}
      rootNavigation={false}
      navContainer={true}
      globalEventListener={(_name: string, _params: any) => {
        // do nothing
      }}
      onBrowseMoreTap={() => {
        // do nothing
      }}
      onBarcodeButtonTap={function (): void {
        // do nothing
      }}
      onBarcodeScanned={function (
        _barcode: string,
        _simScanned: boolean
      ): void {
        console.log(1111111);
      }}
      onRescanTap={function (): void {
        // do nothing
      }}
      onSimActivateTap={function (_barcode: string | undefined): void {
        // do nothing
      }}
      onSimActivateSuccess={function (_value: any): void {
        // do nothing
      }}
      params={undefined}
      gotoDashboardClicked={function (): void {
        console.log('gotodashboard cliked: ');
      }}
    />
  );
}
