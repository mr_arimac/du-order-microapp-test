import React, { createContext } from 'react';
import { initialState, InitialStateType } from './context-types';
import { browseMoreReducer } from './reducer';
import { load, save } from '../utils/storage';

export const OrderAppContext = createContext<{
  state: InitialStateType;
  dispatch: React.Dispatch<any>;
}>({
  state: initialState,
  dispatch: () => null,
});

export async function getAppState(): Promise<InitialStateType> {
  return new Promise((resolve) => {
    load('ORDER_APP_STATE').then((value) => {
      if (value && value !== '') {
        resolve(value);
      } else {
        resolve(initialState);
      }
    });
  });
}

export async function setAppState(state: InitialStateType) {
  save('ORDER_APP_STATE', state);
}

export const mainReducer = (action: { type: string; payload: {} }) => {
  console.log('action.payload main', action.payload);
  const reducer = {
    browseMore: browseMoreReducer(initialState.browseMore, action),
  };
  setAppState(reducer as never);
  return reducer;
};
