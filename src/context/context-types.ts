import type { PhoneNumber } from '../utils/gql/models';

export type BrowseMoreState = {
  checkedNumbers: PhoneNumber[];
};

export type InitialStateType = {
  browseMore: BrowseMoreState;
};

export const initialState: InitialStateType = {
  browseMore: {
    checkedNumbers: [],
  },
};
