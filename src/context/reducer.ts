import { browseMore } from './actions';
import type { BrowseMoreState } from './context-types';

export const browseMoreReducer = (
  state: BrowseMoreState,
  action: { type: string; payload: {} }
) => {
  console.log('action.payload', action.payload);
  switch (action.type) {
    case browseMore.UPDATE_PREFERED_NUMBER:
      console.log('action.payload 000000', action.payload);
      return {
        ...state,
        checkedNumbers: action.payload,
      };
    default:
      console.log('action.payload ddddddd', action.payload);
      return state;
  }
};
