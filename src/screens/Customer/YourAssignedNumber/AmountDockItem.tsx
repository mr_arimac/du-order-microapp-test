import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

type AmountDockItemProp = {
  title: string;
  titleValue: string;
  caption?: string;
  captionValue: string;
};

export const AmountDockItem = (props: AmountDockItemProp) => {
  const { title, titleValue, caption, captionValue } = props;
  return (
    <View style={amount.container}>
      <View style={amount.subContainer}>
        <Text style={amount.title}>{title}</Text>
        <Text style={amount.title}>{titleValue}</Text>
      </View>
      <View
        style={[
          amount.subContainer,
          // eslint-disable-next-line react-native/no-inline-styles
          { justifyContent: caption ? 'space-between' : 'flex-end' },
        ]}
      >
        {caption ? <Text style={amount.captionLeft}>{caption}</Text> : null}
        <Text style={amount.captionRight}>{captionValue}</Text>
      </View>
    </View>
  );
};

const amount = StyleSheet.create({
  container: { flexDirection: 'column', marginBottom: 8 },
  subContainer: { flexDirection: 'row', justifyContent: 'space-between' },
  title: {
    fontFamily: 'Moderat-Bold',
    fontSize: 16,
    fontWeight: '700',
    color: '#121212',
    lineHeight: 20,
    letterSpacing: -0.2,
  },
  captionLeft: {
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    fontWeight: '400',
    color: '#5E687D',
    lineHeight: 20,
  },
  captionRight: {
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    fontWeight: '400',
    color: '#737B8D',
    lineHeight: 20,
  },
});
