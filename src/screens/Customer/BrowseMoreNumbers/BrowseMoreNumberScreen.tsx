import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import {
  DuButton,
  DuButtonsDock,
  DuDialog,
  DuHeader,
  DuHeaderProps,
  DuListItemStatic,
  DuOverlay,
  DuSheet,
  DuSheetProps,
} from '@du-greenfield/du-ui-toolkit';
import { mask } from 'react-native-mask-text';
import React, { FC, useEffect, useRef, useState, useCallback } from 'react';
import {
  FlatList,
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  DuAnalyticsPlugin,
  DuAnalyticsTagBuilder,
} from '@du-greenfield/du-analytics-plugin-core';
import DuAnalyticsFirebasePlugin from '@du-greenfield/du-firebase-analytics';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
// import { useDispatch } from 'react-redux';
// import { phoneNumberSelected } from '../../../redux/features/phoneNumbersSlice';
import type { CustomerNavigatorParamList } from '../../../navigator';
import {
  addExistingOrderItemUnderParent,
  addZero,
  getBssListProductOfferings,
  lockPhoneNumber,
  releasePhoneNumbers,
  reservePhoneNumber,
} from '../../../utils';
import type { LockPhoneNumber } from '../../../utils/gql/models';
import { getSalesOrder } from '../../../';
import type { Msisdn, Order } from '@du-greenfield/du-commons';
import type { MicroAppsProps } from '@du-greenfield/du-microapps-core';
import { RootStateOrAny, useSelector } from 'react-redux';

export type BrowseMoreNumberScreenProps = NativeStackScreenProps<
  CustomerNavigatorParamList,
  'BrowseMoreNumberScreen'
> &
  MicroAppsProps & {
    numberSelectionComplete: (order: Order, selectedMsisdn: Msisdn) => void;
  };

const BrowseMoreNumberScreen: FC<BrowseMoreNumberScreenProps> = ({
  navigation,
  numberSelectionComplete,
}) => {
  const timeout = 720;
  const [overlayVisibility, setOverlayVisibility] = useState<boolean>(false);
  const intervalRef = useRef<NodeJS.Timer | null>(null);
  const [seconds, setSeconds] = useState(timeout);
  const [expired, setExpired] = useState<boolean>(false);
  const safeAreaInsets = useSafeAreaInsets();
  const [showSheet, setShowSheet] = useState<boolean>(false);
  const [checkedPhoneNumber, setCheckedPhoneNumber] = useState<{
    id: string;
    number: string;
  }>({ id: '', number: '' });
  const phoneNumberState = useSelector(
    (state: RootStateOrAny) => state.phoneNumberSlice
  );
  const [phoneNumbersPayload, setPhoneNumbersPayload] = useState<
    LockPhoneNumber | undefined
  >(undefined);

  async function fetchNumbers() {
    const response = await lockPhoneNumber(getSalesOrder().customer.id, 5, []);
    if (response?.status === 'SUCCESS') {
      setShowSheet(response.phoneNumber.length === 0);
      setPhoneNumbersPayload(response);
      setCheckedPhoneNumber({ id: '', number: '' });
    }
  }

  useEffect(() => {
    fetchNumbers();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (seconds <= 0) {
      fetchNumbers();
      setExpired(true);
      setTimeout(() => {
        refreshNumbers();
      }, 500);
      return;
    }
    const timer = setTimeout(() => setSeconds(seconds - 1), 1000);
    intervalRef.current = timer;
    return () => {
      clearInterval(intervalRef.current!);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [seconds]);

  // const props: DuMobileNumberProps = {
  //   incomplete: false,
  //   disableClick: true,
  // };

  function refreshNumbers() {
    setPhoneNumbersPayload(undefined);
    setExpired(false);
    setSeconds(timeout);
    fetchNumbers();
  }

  function handleNumberSelection(id: string) {
    for (let number of phoneNumbersPayload?.phoneNumber!) {
      if (id === number.id) {
        setCheckedPhoneNumber({ number: number.phoneNumber, id: number.id });
        // dispatch(
        //   phoneNumberSelected({
        //     phoneNumber: number.phoneNumber,
        //   })
        // );
      }
    }
  }

  const headerProps: DuHeaderProps = {
    left: 'back',
    leftPressed: () => {
      Platform.OS === 'android' && StatusBar.setBackgroundColor('white');
      if (Platform.OS === 'android') {
        StatusBar.setBarStyle('dark-content', true);
      }
      if (navigation.canGoBack()) {
        navigation.goBack();
      }
    },
    statusBar: { barStyle: 'dark-content', backgroundColor: 'white' },
    title: 'Browse more number',
    background: 'white',
  };

  const _getTime = (_seconds: number): string => {
    _seconds = Number(_seconds);
    var m = Math.floor((_seconds % 3600) / 60);
    var s = Math.floor((_seconds % 3600) % 60);
    return `${addZero(m, 2)}:${addZero(s, 2)}`;
  };

  const sheetProps: DuSheetProps = {
    isVisible: showSheet,
    onClose: () => setShowSheet(false),
    standard: {
      title: "We're sorry, there are no available numbers",
      body: 'We’re sorry, there are no available numbers\n  Please try again later.',
      icon: {
        iconName: 'search-off',
        iconColor: '#BA0023',
        artWorkHeight: 36,
        artWorkWidth: 36,
      },
      buttons: [
        {
          text: 'Try again',
          type: 'primary',
          onPress: () => {
            setShowSheet(false);
            refreshNumbers();
          },
        },
      ],
    },
  };

  const analyticLogScreenView = useCallback(() => {
    let analytics: DuAnalyticsPlugin = new DuAnalyticsFirebasePlugin();
    let tagBuilder = new DuAnalyticsTagBuilder();
    let params = tagBuilder
      .pageName('Browse more free numbers')
      .journeyName('GF Explore')
      .pageChannel('Customer App')
      .previousScreenName('Assign random number')
      .screenType('pdp')
      .screenHierarchy(
        'Homepage: pdp: Family plan: Assign free number: Browse more free numbers'
      )
      .screenBreadcrumb('Assign free number: Browse more free numbers')
      .customerType('new')
      .visitorStatus('returning')
      .recency('5d')
      .customerLoginStatus('logged in')
      .customerEligibility('GF eligible')
      .customerSegment('consumer')
      .productTargetSegment('Prospect')
      .build();
    analytics.logScreenView('GF explore', 'Browse more free numbers', params);
  }, []);

  useEffect(() => {
    analyticLogScreenView();
  }, [analyticLogScreenView]);

  const analyticLogPressShuffle = () => {
    let analytics: DuAnalyticsPlugin = new DuAnalyticsFirebasePlugin();
    analytics.logEvent('Shuffle_Button_Click', {
      event_category: 'Shuffle button',
      event_label: 'Shuffle',
      event_name: 'Browse more numbers: Shuffle numbers',
      event_action: 'Shuffle button click',
    });
  };

  const analyticLogPressConfirmAndProceed = () => {
    let analytics: DuAnalyticsPlugin = new DuAnalyticsFirebasePlugin();
    analytics.logEvent('Confirm_And_Proceed_Button_Click', {
      event_category: 'Confirm and proceed button',
      event_label: 'Confirm and proceed ',
      event_name: 'Browse more numbers: Confirm and proceed',
      event_action: 'Confirm and proceed  button click',
    });
  };

  if (!phoneNumbersPayload) {
    return (
      <View
        style={{
          width: '100%',
          height: '100%',
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Text>...</Text>
      </View>
    );
  }

  const proceedWithSelectedNumber = async () => {
    const releasePhoneNumbersResponse = await releasePhoneNumbers(
      getSalesOrder().customer.id,
      getSalesOrder().salesOrderId,
      phoneNumberState.phoneNumber.id
    );

    if (releasePhoneNumbersResponse?.status === 'SUCCESS') {
      const reservePhoneNumberResponse = await reservePhoneNumber(
        getSalesOrder().customer.id,
        [checkedPhoneNumber.id],
        getSalesOrder().salesOrderId
      );

      console.log('reservePhoneNumberResponse:', reservePhoneNumberResponse);
      finishNumberSelection();
    }
  };

  const finishNumberSelection = async () => {
    const productIdRes = await getBssListProductOfferings();
    if (productIdRes) {
      var id = productIdRes.bssListProductOfferings[0].id;
      await addExistingOrderItemUnderParent(
        getSalesOrder()!.orderItemId || '',
        id,
        checkedPhoneNumber.id
      );
      numberSelectionComplete &&
        numberSelectionComplete(getSalesOrder(), {
          msisdn: checkedPhoneNumber.number,
        });
    }
  };

  return (
    <View style={styles.root}>
      {Platform.OS === 'ios' ? (
        <View style={styles.iosStackHeaderPadding} />
      ) : (
        <View style={{ marginBottom: safeAreaInsets.top }} />
      )}
      <DuHeader {...headerProps} />
      <View style={styles.subContainer}>
        <View style={{ alignItems: 'center' }}>
          {/* TODO: Update toolkit later */}
          {/* <DuMobileNumber
            // digits={`0${params.phoneNumber.substring(3)}`}
            digits={`--${params.phoneNumber}`}
            disabled={expired}
            {...props}
          /> */}
          <Text style={styles.selectedNumber}>
            {phoneNumberState.phoneNumber.phoneNumber
              ? mask(
                  `0${phoneNumberState.phoneNumber.phoneNumber?.substring(3)}`,
                  '999 999 99999'
                )
              : ''}
          </Text>
        </View>
        <View style={styles.grayBanner}>
          <View
            style={{
              flex: 3,
              paddingRight: 30,
            }}
          >
            <View>
              {expired ? (
                <Text style={styles.grayBannerTitle}>Numbers have expired</Text>
              ) : (
                <Text style={styles.grayBannerTitle}>
                  Numbers are locked for you for{' '}
                  <Text style={styles.purple}>{_getTime(seconds)}</Text>
                </Text>
              )}
            </View>
            <View>
              <Text style={styles.grayBannerDecription}>
                {expired
                  ? 'The numbers have expired, shuffle to get a new set'
                  : 'These numbers are reserved for you until the timer ends'}
              </Text>
            </View>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-end',
            }}
          >
            <DuButton
              type="teritary"
              title="Shuffle"
              size="small"
              icon={{ iconName: 'refresh' }}
              containerStyle={[styles.shuffleButton]}
              onPress={() => {
                console.log(checkedPhoneNumber);
                analyticLogPressShuffle();
                refreshNumbers();
              }}
            />
          </View>
        </View>
        <View style={styles.listContainer}>
          <FlatList
            style={{ marginBottom: 200 }}
            data={phoneNumbersPayload.phoneNumber}
            renderItem={({ item }) => {
              return (
                //TODO::Update the tool kit
                <TouchableOpacity
                  onPress={() => {
                    handleNumberSelection(item.id);
                  }}
                >
                  <DuListItemStatic
                    disabled={expired}
                    key={item.id}
                    title={mask(
                      `0${item?.phoneNumber?.substring(3)}`,
                      '999 999 99999'
                    )}
                    bottomDivider={true}
                    onCheckListValueChange={() => {
                      handleNumberSelection(item.id);
                    }}
                    checkListValue={
                      checkedPhoneNumber.number === item.phoneNumber
                        ? true
                        : false
                    }
                    enableCheckList={!expired}
                    checkListType={'radio'}
                  />
                </TouchableOpacity>
              );
            }}
          />
        </View>
      </View>
      <DuSheet {...sheetProps} />
      <DuButtonsDock
        shadow
        items={[
          <DuButton
            title="Confirm and proceed"
            type="primary"
            onPress={() => {
              analyticLogPressConfirmAndProceed();
              if (checkedPhoneNumber.number.length > 0) {
                if (phoneNumbersPayload) {
                  proceedWithSelectedNumber();
                }
              }
            }}
            disabled={expired || checkedPhoneNumber.number.length === 0}
          />,
        ]}
      />
      <DuOverlay
        isVisible={overlayVisibility}
        overlayStyle={styles.overlay}
        onBackdropPress={() => {
          setOverlayVisibility(false);
        }}
      >
        <DuDialog
          headline={' '}
          body={' '}
          primaryText={'Ok'}
          icon={{
            artWorkWidth: 29,
            artWorkHeight: 26,
            iconName: 'warning',
            iconColor: '#F5C311',
          }}
          pressedPrimary={() => {
            setOverlayVisibility(false);
          }}
        />
      </DuOverlay>
    </View>
  );
};

export default BrowseMoreNumberScreen;

const styles = StyleSheet.create({
  root: { flex: 1, backgroundColor: 'white' },
  subContainer: { flex: 1, backgroundColor: 'white' },
  iosStackHeaderPadding: { marginBottom: 14 },
  grayBanner: {
    // marginTop: 24,
    padding: 16,
    backgroundColor: '#F5F6F7',
    borderRadius: 12,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 16,
  },
  grayBannerTitle: {
    fontFamily: 'Moderat-Bold',
    fontSize: 16,
    textAlign: 'left',
    letterSpacing: -0.2,
    lineHeight: 20,
    color: '#041333',
  },
  grayBannerDecription: {
    marginTop: 8,
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    textAlign: 'left',
    lineHeight: 20,
    color: '#5E687D',
  },
  purple: { color: '#753BBD' },
  numberAndButtonContainer: {
    marginTop: 24,
    padding: 16,
    borderColor: '#D7D9DE',
    borderWidth: 1,
    borderRadius: 12,
  },
  badgeAndButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 24,
    alignItems: 'center',
  },
  shuffleButton: { paddingHorizontal: 0 },
  numberContainer: { marginTop: 8 },
  number: {
    fontFamily: 'Moderat-Bold',
    fontSize: 22,
    textAlign: 'left',
    letterSpacing: -0.3,
    lineHeight: 26,
    color: '#041333',
  },
  browsMoreButton: {
    marginTop: 32,
    flexDirection: 'row',
  },
  overlay: {
    paddingHorizontal: 16,
    backgroundColor: 'transparent',
    elevation: 0,
  },
  listContainer: {
    width: '100%',
    marginTop: 24,
    marginBottom: 50,
  },
  selectedNumber: {
    fontSize: 40,
    marginVertical: 30,
    fontFamily: 'Moderat-Medium',
    color: '#000',
  },
});
