import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { DuHeader } from '@du-greenfield/du-ui-toolkit';
import React, { FC } from 'react';
import { StyleSheet, View } from 'react-native';
import { CameraScreen } from 'react-native-camera-kit';
import type { DealerNavigatorParamList } from '../../../navigator';
// import screen from '../../../api/services/Order';

export type BarcodeScannerScreenScreenProps = NativeStackScreenProps<
  DealerNavigatorParamList,
  'BarcodeScannerScreen'
> & {
  onBarcodeScanned: (barcode: string, simScanned: boolean) => void;
  gotoDashboardClicked: () => void;
};

const BarcodeScannerScreen: FC<BarcodeScannerScreenScreenProps> = ({
  navigation,
  onBarcodeScanned,
  gotoDashboardClicked,
}) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  // const [scannedBarcode, setScannedBarcode] = useState<string>('');

  // useEffect(() => {
  //   console.log('scannedBarcode', scannedBarcode);
  //   if (scannedBarcode !== '' && scannedBarcode !== undefined) {
  //     onBarcodeScanned && onBarcodeScanned(scannedBarcode, true);
  //   }
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, [scannedBarcode]);

  // const cameraTimeout = (timeoutValue: string) =>
  //   (timer = setTimeout(function () {
  //     onBarcodeScanned && onBarcodeScanned(scannedBarcode, false);
  //   }, parseInt(timeoutValue, 10)));

  // let timer: any;

  // screen.getScreenDynamics().then((scannedBarcodeConfig) => {
  //   const timeoutValue: string | undefined =
  //     scannedBarcodeConfig?.fields.barcode_scanner_timer.stringValue;
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  //   timer = setTimeout(() => {
  //     console.log('This will run after 1 second!');
  //     onBarcodeScanned && onBarcodeScanned('', false);
  //   }, parseInt(timeoutValue as string, 10));
  // });

  var sampleVar: NodeJS.Timeout;
  function sampleFunction() {
    sampleVar = setTimeout(() => {
      console.log('--- Timeout');
      onBarcodeScanned && onBarcodeScanned('', false);
    }, 15000);
  }

  function sampleStopFunction() {
    clearTimeout(sampleVar);
  }

  sampleFunction();

  // useEffect(() => {
  //   screen.getScreenDynamics().then((scannedBarcodeConfig) => {
  //     const timeoutValue: string | undefined =
  //       scannedBarcodeConfig?.fields.barcode_scanner_timer.stringValue;
  //     // eslint-disable-next-line react-hooks/exhaustive-deps
  //     timer = setTimeout(() => {
  //       console.log('This will run after 1 second!');
  //       onBarcodeScanned && onBarcodeScanned('', false);
  //     }, parseInt(timeoutValue as string, 10));
  //     return () => clearTimeout(timer);
  //   });

  //   // .then((scannedBarcodeConfig) => {
  //   //   const timeoutValue: string | undefined =
  //   //     scannedBarcodeConfig?.fields.barcode_scanner_timer.stringValue;

  //   //   if (scannedBarcode.length > 0) clearTimeout(timer);
  //   //   if (timeoutValue) {
  //   //     cameraTimeout(timeoutValue);
  //   //   }
  //   // });
  // }, []);

  return (
    <View style={[styles.root, { zIndex: 100 }]}>
      <View style={[styles.subContainer]}>
        <View style={styles.midContanier}>
          <DuHeader
            safeArea={true}
            left="back"
            leftPressed={() => {
              if (navigation.canGoBack()) {
                navigation.goBack();
              }
            }}
            right={'tertiary'}
            rightTertiary={{
              disable: true,
              text: 'Help',
            }}
            statusBar={{
              backgroundColor: '#041333',
              barStyle: 'light-content',
            }}
            navToDashBoard={true}
            rightTertiarySupport={{
              pressed: () => gotoDashboardClicked(),
            }}
            background="#041333"
          />
        </View>
        <View style={styles.cameraContainer}>
          <CameraScreen
            onBottomButtonPressed={() => {}}
            scanBarcode
            showFrame
            allowCaptureRetake={false}
            laserColor="red"
            frameColor="white"
            onReadCode={(event) => {
              console.log('--- onReadCode');
              sampleStopFunction();

              if (
                event.nativeEvent.codeStringValue.length === 19 ||
                event.nativeEvent.codeStringValue.length === 20
              ) {
                onBarcodeScanned &&
                  onBarcodeScanned(event.nativeEvent.codeStringValue, true);
              } else {
                onBarcodeScanned && onBarcodeScanned('', false);
              }
            }}
            hideControls
            cameraRatioOverlay={undefined}
            captureButtonImage={undefined}
            cameraFlipImage={undefined}
            torchOnImage={undefined}
            torchOffImage={undefined}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  subContainer: {
    flex: 1,
  },
  scanText: {
    color: 'white',
    fontSize: 32,
    lineHeight: 30,
    fontFamily: 'Moderat-Bold',
  },
  midContanier: {
    zIndex: 100,
  },
  cameraContainer: {
    flex: 1,
    zIndex: 10,
  },
  camera: {
    flex: 1,
    width: '100%',
    // height: '90%',
    // width: '100%',
    backgroundColor: 'red',
    // marginTop: 100,
  },
  overlayText: {
    width: '100%',
    height: 120,
    // backgroundColor: 't',
    zIndex: 100,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});

export default BarcodeScannerScreen;
