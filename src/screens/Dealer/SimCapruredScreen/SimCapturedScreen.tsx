import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import {
  DuButton,
  DuHeader,
  DuJumbotron,
  DuBanner,
  DuBannerProps,
  DuIconProps,
  DuButtonsDock,
} from '@du-greenfield/du-ui-toolkit';
import { mask } from 'react-native-mask-text';
import React, { FC, useState } from 'react';
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native';
import { getSalesOrder } from '../../../';
import type { DealerNavigatorParamList } from '../../../navigator';
import EnterICCIDManuallyModal from '../SimActivationScan/EnterICCIDManuallyModal';

export type SimCapturedScreenProps = NativeStackScreenProps<
  DealerNavigatorParamList,
  'SimCapturedScreen'
> & {
  params: any;
  onRescanTap: () => void;
  onSimActivateTap: (barcode: string | undefined) => void;
  gotoDashboardClicked: () => void;
};

const SimCapturedScreen: FC<SimCapturedScreenProps> = ({
  params,
  navigation,
  onSimActivateTap,
  onRescanTap,
  gotoDashboardClicked,
}) => {
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const { simScanned, scannedBarcode } = params;

  const props: DuBannerProps = {
    // chevron: chevron,
    icon: {
      iconName: 'info',
      iconColor: simScanned ? '#00933E' : '#BA0023',
    } as DuIconProps,
    title: simScanned ? 'Sim successfully scanned!' : 'SIM scan failed',
    type: simScanned ? 'positive' : 'danger',
  };

  const takeAPhotoButton = () => {
    return (
      <View style={styles.takeAPhotoButtonContainer}>
        <DuButton
          title="Scan SIM again"
          type="primary"
          onPress={() => {
            onRescanTap && onRescanTap();
          }}
        />
      </View>
    );
  };

  const enterManualyButton = () => {
    if (!simScanned) {
      return (
        <View style={styles.enterManuallyButtonContainer}>
          <DuButton
            title="Enter ICCID number manually"
            type="secondary"
            onPress={() => {
              setIsModalVisible(true);
            }}
          />
        </View>
      );
    } else {
      return <View />;
    }
  };

  return (
    <View style={styles.root}>
      <DuHeader
        safeArea={true}
        left="back"
        leftPressed={() => {
          if (navigation.canGoBack()) {
            navigation.goBack();
          }
        }}
        right={'tertiary'}
        rightTertiary={{
          disable: true,
          text: 'Help',
        }}
        navToDashBoard={true}
        rightTertiarySupport={{
          pressed: () => gotoDashboardClicked(),
        }}
        statusBar={{
          backgroundColor: 'white',
          barStyle: 'dark-content',
        }}
        background="white"
      />
      <ScrollView style={[styles.subContainer]}>
        <DuJumbotron mainJumbotron="SIM barcode" />
        <View style={styles.middleContainer}>
          <DuJumbotron
            mainJumbotron={
              getSalesOrder().msisdn?.msisdn
                ? mask(
                    `0${getSalesOrder().msisdn?.msisdn?.substring(3)}`,
                    '999 999 99999'
                  )
                : ''
            }
            overLine="SIM 1"
            alignment="center"
          />
          <View style={styles.simCardPlaceholderContainer}>
            <Image
              source={require('../SimActivationScan/sim-card-placeholder.png')}
            />
            <View style={styles.simCardOverlayContainer}>
              <View>
                <Image source={require('../SimActivationScan/du-logo.png')} />
              </View>
              <View style={styles.simCardOverlayContentContainer}>
                <View>
                  <Text style={styles.iccidNumberTitle}>ICCID Number</Text>
                </View>
                <View style={styles.iccidNumberContainer}>
                  <Text style={styles.iccidNumber}>
                    {scannedBarcode ? scannedBarcode : 'N/A'}
                  </Text>
                </View>
                <View style={styles.barcodeContainer}>
                  <Image source={require('../SimActivationScan/barcode.png')} />
                </View>
                <View style={styles.barcodeNumberContainer}>
                  <Text style={styles.barcodeNumber}>
                    {scannedBarcode ? scannedBarcode : '1248326122483261'}
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <View style={{ width: 340, marginBottom: 32 }}>
            <DuBanner {...props} />
          </View>
          {simScanned ? undefined : takeAPhotoButton()}
          <View style={{ marginBottom: 100 }}>{enterManualyButton()}</View>
        </View>
        <EnterICCIDManuallyModal
          isVisible={isModalVisible}
          onPressClose={() => {
            setIsModalVisible(false);
          }}
          verifyAndContinue={() => {
            setIsModalVisible(false);
            onSimActivateTap && onSimActivateTap(scannedBarcode);
          }}
        />
      </ScrollView>
      <DuButtonsDock
        tab
        items={[
          <DuButton
            title="Activate SIM"
            type="primary"
            disabled={!simScanned}
            onPress={() => {
              onSimActivateTap && onSimActivateTap(scannedBarcode);
            }}
          />,
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  subContainer: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 105,
    paddingVertical: 32,
  },
  middleContainer: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  simCardPlaceholderContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  simCardOverlayContainer: {
    position: 'absolute',
    width: 300,
    height: 180,
  },
  simCardOverlayContentContainer: {
    width: 180,
    marginTop: 17,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iccidNumberTitle: {
    fontFamily: 'Moderat-Medium',
    fontSize: 11,
    textAlign: 'center',
    color: '#5E687D',
    lineHeight: 20,
  },
  iccidNumberContainer: { marginTop: 8 },
  iccidNumber: {
    fontFamily: 'Moderat-Medium',
    fontSize: 15,
    textAlign: 'center',
    color: '#041333',
    lineHeight: 24,
    letterSpacing: -0.15,
  },
  barcodeContainer: { marginTop: 24 },
  barcodeNumberContainer: { marginTop: 7 },
  barcodeNumber: {
    fontFamily: 'Moderat-Regular',
    fontSize: 11,
    textAlign: 'center',
    color: '#233659',
    lineHeight: 20,
  },
  enterManuallyButtonContainer: { width: 340, marginTop: 16 },
  takeAPhotoButtonContainer: { width: 340 },
});

export default SimCapturedScreen;
