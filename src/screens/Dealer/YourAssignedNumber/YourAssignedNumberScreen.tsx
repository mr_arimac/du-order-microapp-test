import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { getSalesOrder } from '../../../';
import {
  DuButton,
  DuButtonsDock,
  DuDialog,
  DuHeader,
  DuHeaderProps,
  DuIcon,
  DuJumbotron,
  DuOverlay,
  DuSwitch,
} from '@du-greenfield/du-ui-toolkit';
import React, { FC, useEffect, useState } from 'react';
import { MaskedText } from 'react-native-mask-text';
import { Platform, StatusBar, StyleSheet, Text, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import type { DealerNavigatorParamList } from '../../../navigator';
import { phoneNumberSelected } from '../../../redux/features/phoneNumbersSlice';
import {
  lockPhoneNumber,
  reservePhoneNumber,
  getBssListProductOfferings,
  addExistingOrderItemUnderParent,
  updateOrderItems,
} from '../../../utils';
import type { ErrorCode, LockPhoneNumber } from '../../../utils/gql/models';
import type { Msisdn, Order } from '@du-greenfield/du-commons';

type AmountDockItemProp = {
  title: string;
  titleValue: string;
  caption?: string;
  captionValue: string;
};

export type DealerYourAssignedNumberScreenProps = NativeStackScreenProps<
  DealerNavigatorParamList,
  'YourAssignedNumberScreen'
> & {
  onBrowseMoreTap: (isEsim: boolean | undefined) => void;
  numberSelectionComplete: (order: Order, msisdn: Msisdn) => void;
  gotoDashboardClicked: () => void;
};

const DealerYourAssignedNumberScreen: FC<
  DealerYourAssignedNumberScreenProps
> = ({
  navigation,
  numberSelectionComplete,
  onBrowseMoreTap,
  gotoDashboardClicked,
}) => {
  const safeAreaInsets = useSafeAreaInsets();
  const phoneNumbers = useSelector(
    (state: RootStateOrAny) => state.phoneNumberSlice
  );
  const dispatch = useDispatch();
  const [isEsim, setIsEsim] = useState<boolean>(false);
  const [error, setError] = useState<ErrorCode>();
  const [overlayVisibility, setOverlayVisibility] = useState<boolean>(false);
  const [errorHeader, setErrorHeader] = useState<string>('');
  const [phoneNumbersPayload, setPhoneNumbersPayload] = useState<
    LockPhoneNumber | undefined
  >(undefined);
  const [phoneNumberLocked, setPhoneNumberLocked] = useState<boolean>(false);
  const [noNoPhoneNumbersAvailable, setnoNoPhoneNumbersAvailable] =
    useState<boolean>(false);

  const headerProps: DuHeaderProps = {
    left: 'back',
    leftPressed: () => {
      Platform.OS === 'android' && StatusBar.setBackgroundColor('white');
      if (Platform.OS === 'android') {
        StatusBar.setBarStyle('dark-content', true);
      }
      if (navigation.canGoBack()) {
        navigation.goBack();
      }
    },
    right: 'tertiary',
    rightTertiary: {
      text: 'Help',
      disable: true,
    },
    navToDashBoard: true,
    rightTertiarySupport: {
      pressed: () => gotoDashboardClicked(),
    },
    statusBar: { barStyle: 'dark-content', backgroundColor: 'white' },
    background: 'white',
  };

  function fetchData() {
    lockPhoneNumberOnScreen();
  }

  async function proceedWithAddExistingOrderItemUnderParent(
    productIdRes: any,
    msisdnId: string,
    msisdn: string
  ) {
    var id = productIdRes.bssListProductOfferings[0].id;
    await addExistingOrderItemUnderParent(
      getSalesOrder().orderItemId!,
      id,
      msisdnId
    );

    numberSelectionComplete &&
      numberSelectionComplete(getSalesOrder(), {
        msisdn: msisdn,
        isEsim: isEsim,
      });
  }

  async function lockPhoneNumberOnScreen() {
    setOverlayVisibility(false);
    if (!getSalesOrder().customer.id) {
      return;
    }
    const response = await lockPhoneNumber(getSalesOrder().customer.id, 1, []);
    if (response?.errorCodes && response?.errorCodes.length > 0) {
      setError(response?.errorCodes[0]);
      setOverlayVisibility(true);
      return;
    }
    if (response?.status === 'SUCCESS') {
      if (response.phoneNumber.length === 0) {
        setnoNoPhoneNumbersAvailable(true);
        setPhoneNumbersPayload(response);
        setErrorHeader('There are no available numbers');
        setError({ message: 'Please try again' });
        setOverlayVisibility(true);
        return;
      }

      setnoNoPhoneNumbersAvailable(false);
      setErrorHeader('');
      setError({ message: '' });
      const reservePhoneNumberResponse = await reservePhoneNumber(
        getSalesOrder().customer.id,
        [response.phoneNumber[0].id],
        getSalesOrder().salesOrderId
      );

      if (reservePhoneNumberResponse?.status === 'SUCCESS') {
        setPhoneNumbersPayload(response);

        dispatch(
          phoneNumberSelected({
            id: response.phoneNumber[0].id,
            phoneNumber: response.phoneNumber[0].phoneNumber,
          })
        );

        for (let phoneNumberObject of response.phoneNumber) {
          if (phoneNumberObject.status === 'LOCKED') {
            setPhoneNumberLocked(true);
          }
        }
      }
    }
  }

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (!phoneNumbersPayload) {
    return (
      <View
        style={{
          width: '100%',
          height: '100%',
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Text>...</Text>
      </View>
    );
  }

  const finishNumberSelection = async () => {
    const productIdRes = await getBssListProductOfferings();

    if (isEsim) {
      const updateOrderItemsResponse = await updateOrderItems(
        getSalesOrder().salesOrderId,
        getSalesOrder().simCardOrderItemId!
      );

      console.log(555, updateOrderItemsResponse);

      if (updateOrderItemsResponse && productIdRes) {
        await proceedWithAddExistingOrderItemUnderParent(
          productIdRes,
          phoneNumbersPayload?.phoneNumber[0].id,
          phoneNumbersPayload?.phoneNumber[0].phoneNumber
        );
      }
    } else {
      console.log('proceedWithAddExistingOrderItemUnderParent');
      await proceedWithAddExistingOrderItemUnderParent(
        productIdRes,
        phoneNumbersPayload?.phoneNumber[0].id,
        phoneNumbersPayload?.phoneNumber[0].phoneNumber
      );
    }
  };

  return (
    <View style={styles.root}>
      {Platform.OS === 'ios' ? (
        <View style={styles.iosStackHeaderPadding} />
      ) : (
        <View style={{ marginBottom: safeAreaInsets.top }} />
      )}
      <DuHeader {...headerProps} />
      <View style={styles.subContainer}>
        <View
          // eslint-disable-next-line react-native/no-inline-styles
          style={{ marginTop: 32 }}
        >
          <DuJumbotron
            size="xxlarge"
            alignment="center"
            mainJumbotron="Your assigned number"
          />
        </View>
        {/* <Banner isExpired={setExpired} seconds={seconds} /> */}
        <View
          style={[
            styles.numberAndButtonContainer,
            // eslint-disable-next-line react-native/no-inline-styles
            // { opacity: expired ? 0.2 : 1 },
          ]}
        >
          <View>
            <View style={styles.simCardIconContainer}>
              <DuIcon iconName="chat-bubble" gradient={true} />
            </View>
          </View>
          <View
            // eslint-disable-next-line react-native/no-inline-styles
            style={{ flex: 1, flexDirection: 'column', marginLeft: 16 }}
          >
            {/* <View style={styles.badgeAndButtonContainer}>
              <Text> </Text>
              <DuBadge
                title="SIM 1 number"
                // backgroundStyle={expired && { backgroundColor: 'transparent' }}
              />
              <DuButton
                type="teritary"
                title="Shuffle"
                size="small"
                disabled={true}
                icon={{ iconName: 'refresh' }}
                containerStyle={styles.shuffleButton}
                onPress={() => {
                  lockPhoneNumberOnScreen();
                }}
              />
            </View> */}
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}
            >
              <View style={styles.numberContainer}>
                <MaskedText style={styles.number} mask="999 999 9999">
                  {phoneNumbers?.phoneNumber?.phoneNumber?.length > 0
                    ? `0${phoneNumbers?.phoneNumber?.phoneNumber?.substring(3)}`
                    : ''}
                </MaskedText>
              </View>
              <DuButton
                type="teritary"
                title="Shuffle"
                size="small"
                disabled={true}
                icon={{ iconName: 'refresh' }}
                containerStyle={styles.shuffleButton}
                onPress={() => {
                  lockPhoneNumberOnScreen();
                }}
              />
            </View>

            <View style={styles.browsMoreButton}>
              <DuButton
                type="secondary"
                title="Browse numbers"
                size="small"
                onPress={() => {
                  onBrowseMoreTap && onBrowseMoreTap(isEsim);
                }}
              />
              <DuSwitch
                title="eSIM"
                onValueChange={() =>
                  setIsEsim((previousState: boolean) => !previousState)
                }
              />
            </View>
          </View>
        </View>
        <DuOverlay
          isVisible={overlayVisibility}
          overlayStyle={styles.overlay}
          onBackdropPress={() => {
            if (noNoPhoneNumbersAvailable) {
              lockPhoneNumberOnScreen();
            } else {
              setOverlayVisibility(false);
            }
          }}
        >
          <DuDialog
            headline={errorHeader}
            body={error?.message}
            primaryText={noNoPhoneNumbersAvailable ? 'Try again' : 'Ok'}
            icon={{
              artWorkWidth: 29,
              artWorkHeight: 26,
              iconName: 'warning',
              iconColor: '#F5C311',
            }}
            pressedPrimary={() => {
              if (noNoPhoneNumbersAvailable) {
                lockPhoneNumberOnScreen();
              } else {
                setOverlayVisibility(false);
              }
            }}
          />
        </DuOverlay>
      </View>

      <DuButtonsDock
        shadow
        items={[
          <View style={styles.buttonDockContainer}>
            <View
              // eslint-disable-next-line react-native/no-inline-styles
              style={{ width: '30%' }}
            >
              <AmountDockItem
                title={getSalesOrder().product.name}
                titleValue={
                  getSalesOrder().product.plan.price.currencyCode +
                  ' ' +
                  getSalesOrder().product.plan.price.amount.toFixed(2)
                }
                captionValue={'Incl. VAT'}
                caption={'Your selected plan'}
              />
            </View>
            <DuButton
              title="Continue to eligibility check"
              type="primary"
              onPress={() => {
                if (phoneNumbersPayload) {
                  finishNumberSelection();
                }
              }}
              disabled={!phoneNumberLocked}
            />
          </View>,
        ]}
      />
    </View>
  );
};

export default DealerYourAssignedNumberScreen;

const styles = StyleSheet.create({
  root: { flex: 1, backgroundColor: 'white' },
  subContainer: { flex: 1, backgroundColor: 'white', paddingHorizontal: '20%' },
  iosStackHeaderPadding: { marginBottom: 14 },
  grayBanner: {
    marginTop: 24,
    padding: 16,
    backgroundColor: '#F5F6F7',
    borderRadius: 12,
    flexDirection: 'row',
    alignItems: 'center',
  },
  grayBannerTitle: {
    fontFamily: 'Moderat-Bold',
    fontSize: 16,
    textAlign: 'left',
    letterSpacing: -0.2,
    lineHeight: 20,
    color: '#041333',
  },
  grayBannerDecription: {
    marginTop: 8,
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    textAlign: 'left',
    letterSpacing: -0.2,
    lineHeight: 20,
    color: '#5E687D',
  },
  purple: { color: '#753BBD' },
  numberAndButtonContainer: {
    flexDirection: 'row',
    marginTop: 32,
    padding: 16,
    borderColor: '#D7D9DE',
    borderWidth: 1,
    borderRadius: 12,
  },
  badgeAndButtonContainerBase: {},
  badgeAndButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 24,
    alignItems: 'center',
  },
  shuffleButton: { paddingHorizontal: 0 },
  numberContainer: { marginTop: 8 },
  number: {
    fontFamily: 'Moderat-Bold',
    fontSize: 22,
    textAlign: 'left',
    letterSpacing: -0.3,
    lineHeight: 26,
    color: '#041333',
  },
  browsMoreButton: {
    marginTop: 32,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  simCardIconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 48,
    height: 48,
    backgroundColor: '#F5F6F7',
    borderRadius: 24,
  },
  buttonDockContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  overlay: {
    paddingHorizontal: 16,
    backgroundColor: 'transparent',
    elevation: 0,
  },
});

const AmountDockItem = (props: AmountDockItemProp) => {
  const { title, titleValue, caption, captionValue } = props;
  return (
    <View style={amount.container}>
      <View style={amount.subContainer}>
        <Text style={amount.title}>{title}</Text>
        <Text style={amount.title}>{titleValue}</Text>
      </View>
      <View
        style={[
          amount.subContainer,
          // eslint-disable-next-line react-native/no-inline-styles
          { justifyContent: caption ? 'space-between' : 'flex-end' },
        ]}
      >
        {caption ? <Text style={amount.captionLeft}>{caption}</Text> : null}
        <Text style={amount.captionRight}>{captionValue}</Text>
      </View>
    </View>
  );
};

const amount = StyleSheet.create({
  container: { flexDirection: 'column', marginBottom: 8 },
  subContainer: { flexDirection: 'row', justifyContent: 'space-between' },
  title: {
    fontFamily: 'Moderat-Bold',
    fontSize: 16,
    color: '#121212',
    lineHeight: 20,
    letterSpacing: -0.2,
  },
  captionLeft: {
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    color: '#5E687D',
    lineHeight: 20,
  },
  captionRight: {
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    color: '#737B8D',
    lineHeight: 20,
  },
});
