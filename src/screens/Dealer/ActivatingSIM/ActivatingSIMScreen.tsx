import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import {
  DuHeader,
  DuHeaderProps,
  DuJumbotron,
  DuSpinner,
  DuSpinnerProps,
} from '@du-greenfield/du-ui-toolkit';
import React, { FC, useEffect, useRef, useState } from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import {
  addExistingOrderItem,
  getCustomer,
  getListProductOffering,
  submitSalesOrder,
  updateOrderItem,
} from '../../../utils/gql';
import type { DealerNavigatorParamList } from '../../../navigator';
import { getSalesOrder } from '../../../';
import type { DocumentNode } from 'graphql';

import {
  SUBMIT_SALES_ORDER,
  SUBMIT_SALES_ORDER_WITH_BILLING,
} from './../../../utils/gql/mutations';
import {
  BSS_GET_CUSTOMER,
  BSS_LIST_PRODUCT_OFFERING,
} from './../../../utils/gql/queries';
import firebaseData from '../../../../firebaseData.json';
// import screen from './../../../api/services/Order';

export type ActivatingSIMScreenProps = NativeStackScreenProps<
  DealerNavigatorParamList,
  'ActivatingSIMScreen'
> & {
  params: any;
  cancel: () => void;
  onSimActivateSuccess: (value: any) => void;
  gotoDashboardClicked: () => void;
};

const ActivatingSIMScreen: FC<ActivatingSIMScreenProps> = ({
  params,
  navigation,
  onSimActivateSuccess,
  gotoDashboardClicked,
}) => {
  const [progressValue, setProgressValue] = useState<number>(0.0);
  const { scannedBarcode } = params;
  const intervalRef = useRef<NodeJS.Timer | null>(null);
  const spinnerProps: DuSpinnerProps = {
    state: 'in-progress',
    size: 'large',
    progressValue: progressValue,
    loadingMessage: null,
  };
  const safeAreaInsets = useSafeAreaInsets();
  const headerProps: DuHeaderProps = {
    left: 'back',
    right: 'tertiary',
    rightTertiary: {
      disable: true,
      text: 'Help',
    },
    navToDashBoard: true,
    rightTertiarySupport: {
      pressed: () => {
        gotoDashboardClicked();
      },
    },
    leftPressed: () => {
      if (Platform.OS === 'android') {
        StatusBar.setBarStyle('dark-content', true);
        StatusBar.setBackgroundColor('white');
      }
      if (navigation.canGoBack()) {
        navigation.goBack();
      }
    },
    statusBar: { barStyle: 'dark-content', backgroundColor: 'white' },
    background: 'white',
  };

  useEffect(() => {
    StatusBar.setBackgroundColor('white');
    StatusBar.setBarStyle('dark-content', true);
  }, []);

  //TODO: remove after testing the flow

  // async function proceedToSubmitSalesOrder(
  //   dynamics: any | undefined,
  //   sumbitSalesOrder: () => Promise<void>
  // ) {
  //   if (dynamics?.fields.in_store_delivery.booleanValue) {
  //     if (!getSalesOrder().msisdn?.isEsim) {
  //       const productOfferingResponse = await getListProductOffering(
  //         BSS_LIST_PRODUCT_OFFERING
  //       );

  //       if (
  //         productOfferingResponse.code === 200 &&
  //         productOfferingResponse.jsonBody.data.bssListProductOfferings
  //           .length !== 0
  //       ) {
  //         let inStoreOfferingId =
  //           productOfferingResponse.jsonBody.data.bssListProductOfferings[0].id;
  //         let addExistingOrderRes = await addExistingOrderItemOld(
  //           getSalesOrder().salesOrderId,
  //           getSalesOrder().customer.customerLocations[0].id,
  //           inStoreOfferingId
  //         );

  //         if (addExistingOrderRes?.addExistingOrderItem.status === 'SUCCESS') {
  //           await sumbitSalesOrder();
  //         }
  //       }
  //     }
  //   } else {
  //     await sumbitSalesOrder();
  //   }
  // }

  // async function updateOrderItemWithBarcodeOld() {
  //   //TODO::REMOVE redundant API calls :: extract to redux store
  //   const dynamics = await screen.getScreenDynamics();

  //   // Call updateOrderItem for pSIM
  //   if (!getSalesOrder().msisdn?.isEsim) {
  //     const updateOrderItemResponse = await updateOrderItem(
  //       getSalesOrder().salesOrderId,
  //       //8997103122044565973 TODO::use this value as a pre hard coded one
  //       getSalesOrder().simCardOrderItemId!,
  //       scannedBarcode
  //     );

  //     if (updateOrderItemResponse.updateOrderItems.status == 'SUCCESS') {
  //       await proceedToSubmitSalesOrder(dynamics, sumbitSalesOrder);
  //     }
  //   } else {
  //     await proceedToSubmitSalesOrder(dynamics, sumbitSalesOrder);
  //   }

  //   async function sumbitSalesOrder() {
  //     let customer: any = null;
  //     let input: any = {
  //       salesOrderId: getSalesOrder().salesOrderId,
  //     };
  //     let gql: DocumentNode = SUBMIT_SALES_ORDER;

  //     if (dynamics?.fields.associate_billing_account.booleanValue) {
  //       customer = await getCustomer(
  //         getSalesOrder().customer.id,
  //         BSS_GET_CUSTOMER
  //       );
  //       input[`${dynamics?.fields.billing_account_field.stringValue}`] =
  //         customer?.billingAccounts[0]?.id;
  //       gql = SUBMIT_SALES_ORDER_WITH_BILLING(
  //         dynamics?.fields.billing_account_field.stringValue
  //       );
  //     }

  //     const submitRes = await submitSalesOrder(input, gql);
  //     if (submitRes.submitSalesOrder.status == 'SUCCESS') {
  //       setProgressValue(1);
  //       //TODO: Pass required data
  //       onSimActivateSuccess && onSimActivateSuccess({});
  //     }
  //   }
  // }

  // Submit sales order
  async function sumbitSalesOrder(dynamics: any) {
    let customer: any = null;
    let input: any = {
      salesOrderId: getSalesOrder().salesOrderId,
    };
    let gql: DocumentNode = SUBMIT_SALES_ORDER;

    if (dynamics?.fields.associate_billing_account.booleanValue) {
      customer = await getCustomer(
        getSalesOrder().customer.id,
        BSS_GET_CUSTOMER
      );
      input[`${dynamics?.fields.billing_account_field.stringValue}`] =
        customer?.billingAccounts[0]?.id;
      gql = SUBMIT_SALES_ORDER_WITH_BILLING(
        dynamics?.fields.billing_account_field.stringValue
      );
    }

    const submitRes = await submitSalesOrder(input, gql);
    if (submitRes.submitSalesOrder.status == 'SUCCESS') {
      setProgressValue(1);
      //TODO: Pass required data
      onSimActivateSuccess && onSimActivateSuccess({});
    }
  }

  // Update orderitem
  async function updateOrderItemWithBarcode() {
    const updateOrderItemResponse = await updateOrderItem(
      getSalesOrder().salesOrderId,
      //8997103122044565973 TODO::use this value as a pre hard coded one
      getSalesOrder().simCardOrderItemId!,
      scannedBarcode
    );
    return updateOrderItemResponse;
  }

  // List product offerings
  async function listProductOfferings() {
    const productOfferingResponse = await getListProductOffering(
      BSS_LIST_PRODUCT_OFFERING
    );

    return productOfferingResponse;
  }

  // Add existing order item
  async function addIngExistingOrderItem(inStoreOfferingId: string) {
    let addExistingOrderResponse = await addExistingOrderItem(
      getSalesOrder().salesOrderId,
      getSalesOrder().customer.customerLocations[0].id,
      inStoreOfferingId
    );

    return addExistingOrderResponse;
  }

  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //

  const proceedWithSimActivation = async () => {
    // const dynamics = await screen.getScreenDynamics();
    const dynamics = firebaseData;
    console.log(11, dynamics);

    if (getSalesOrder().type === 'payg') {
      if (getSalesOrder().msisdn?.isEsim) {
        // sumbitSalesOrder
        sumbitSalesOrder(dynamics);
      } else {
        // updateOrderItem
        const updateOrderItemResponse = await updateOrderItemWithBarcode();

        if (updateOrderItemResponse.updateOrderItems.status == 'SUCCESS') {
          // getListProductOffering
          const listProductOfferingResponse = await listProductOfferings();

          // addExistingOrderItem
          if (
            listProductOfferingResponse.code === 200 &&
            listProductOfferingResponse.jsonBody.data.bssListProductOfferings
              .length !== 0
          ) {
            const addExistingOrderItemResponse = await addIngExistingOrderItem(
              listProductOfferingResponse.jsonBody.data
                .bssListProductOfferings[0].id
            );

            // sumbitSalesOrder
            if (
              addExistingOrderItemResponse?.addExistingOrderItem.status ===
              'SUCCESS'
            ) {
              await sumbitSalesOrder(dynamics);
            }
          }
        }
      }
    } else if (getSalesOrder().type === 'predefined') {
      if (getSalesOrder().msisdn?.isEsim) {
        // sumbitSalesOrder
        sumbitSalesOrder(dynamics);
      } else {
        // updateOrderItem
        const updateOrderItemResponse = await updateOrderItemWithBarcode();
        if (updateOrderItemResponse.updateOrderItems.status == 'SUCCESS') {
          // getListProductOffering
          const listProductOfferingResponse = await listProductOfferings();

          // addExistingOrderItem
          if (
            listProductOfferingResponse.code === 200 &&
            listProductOfferingResponse.jsonBody.data.bssListProductOfferings
              .length !== 0
          ) {
            const addExistingOrderItemResponse = await addIngExistingOrderItem(
              listProductOfferingResponse.jsonBody.data
                .bssListProductOfferings[0].id
            );

            // sumbitSalesOrder
            if (
              addExistingOrderItemResponse?.addExistingOrderItem.status ===
              'SUCCESS'
            ) {
              await sumbitSalesOrder(dynamics);
            }
          }
        }
      }
    }
  };

  useEffect(() => {
    proceedWithSimActivation();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const id = setInterval(() => {
      if (progressValue >= 1) {
        clearInterval(intervalRef.current!);
      } else {
        setProgressValue(progressValue + 0.005);
      }
    }, 1);
    intervalRef.current = id;
    return () => {
      clearInterval(intervalRef.current!);
    };
  });

  const getScreenMiddleContent = () => {
    return (
      <View style={styles.mainView}>
        <View style={styles.spinnerContainer}>
          <View style={styles.positioning}>
            <DuSpinner {...spinnerProps} />
            <View style={styles.jumbotronContainer}>
              <DuJumbotron
                alignment={'center'}
                mainJumbotron={'Activating SIM...'}
                size={'large'}
              />
            </View>
          </View>
        </View>
      </View>
    );
  };

  return (
    <View style={styles.root}>
      {Platform.OS === 'ios' ? (
        <View style={styles.iosStackHeaderPadding} />
      ) : (
        <View style={{ marginBottom: safeAreaInsets.top }} />
      )}
      <DuHeader {...headerProps} />
      <View style={styles.midContainer}>
        <DuJumbotron
          mainJumbotron={
            getSalesOrder().msisdn?.isEsim ? '' : 'Scan SIM barcode '
          }
          size={'xlarge'}
        />
        {getScreenMiddleContent()}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: { flex: 1, backgroundColor: 'white', paddingTop: 4 },
  mainView: {
    flex: 1,
    backgroundColor: 'white',
    padding: 16,
  },
  spinnerContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    justifyContent: 'center',
  },
  jumbotronContainerStyle: {
    padding: 6,
    marginBottom: 18,
  },
  positioning: {
    bottom: '10%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  iosStackHeaderPadding: {
    marginBottom: 14,
  },
  dockedButton: {
    width: '100%',
  },
  midContainer: {
    paddingHorizontal: 105,
    flex: 1,
    marginTop: 36,
  },
  jumbotronContainer: {
    marginTop: 32,
  },
  jumbotronContainerWithIcon: {
    marginTop: 21,
  },
});

export default ActivatingSIMScreen;
