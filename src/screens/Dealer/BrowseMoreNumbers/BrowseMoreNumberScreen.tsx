import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import {
  DuButton,
  DuButtonsDock,
  DuHeader,
  DuIcon,
  DuHeaderProps,
  DuListItemStatic,
  DuJumbotron,
} from '@du-greenfield/du-ui-toolkit';
import React, { FC, useEffect, useRef, useState } from 'react';
import { mask } from 'react-native-mask-text';
import {
  FlatList,
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import type { CustomerNavigatorParamList } from '../../../navigator';
import {
  addExistingOrderItemUnderParent,
  addZero,
  getBssListProductOfferings,
  lockPhoneNumber,
  releasePhoneNumbers,
  reservePhoneNumber,
  updateOrderItems,
} from '../../../utils';
import type { LockPhoneNumber } from '../../../utils/gql/models';
import { getSalesOrder } from '../../../';
import type { Msisdn, Order } from '@du-greenfield/du-commons';
import { RootStateOrAny, useSelector } from 'react-redux';

export type BrowseMoreNumberScreenProps = NativeStackScreenProps<
  CustomerNavigatorParamList,
  'BrowseMoreNumberScreen'
> & {
  params: any;
  numberSelectionComplete: (order: Order, selectedMsisdn: Msisdn) => void;
  gotoDashboardClicked: () => void;
};

const BrowseMoreNumberScreen: FC<BrowseMoreNumberScreenProps> = ({
  navigation,
  numberSelectionComplete,
  gotoDashboardClicked,
  params,
}) => {
  console.log('Params from assign number screen: ', params);

  const timeout = 720;
  const intervalRef = useRef<NodeJS.Timer | null>(null);
  const [seconds, setSeconds] = useState(timeout);
  const [expired, setExpired] = useState<boolean>(false);
  const safeAreaInsets = useSafeAreaInsets();
  const [checkedPhoneNumber, setCheckedPhoneNumber] = useState<{
    id: string;
    number: string;
  }>({ id: '', number: '' });
  const [phoneNumbersPayload, setPhoneNumbersPayload] = useState<
    LockPhoneNumber | undefined
  >(undefined);
  const phoneNumberState = useSelector(
    (state: RootStateOrAny) => state.phoneNumberSlice
  );

  async function fetchNumbers() {
    if (!getSalesOrder().customer.id) {
      return;
    }
    const response = await lockPhoneNumber(getSalesOrder().customer.id, 5, []);
    if (response?.status === 'SUCCESS') {
      setPhoneNumbersPayload(response);

      // Use this to test no numbers availbale senario

      // setPhoneNumbersPayload({
      //   phoneNumber: [],
      //   status: 'ss',
      //   errorCodes: [],
      // });
      setCheckedPhoneNumber({ id: '', number: '' });
    }
  }

  useEffect(() => {
    fetchNumbers();
  }, []);

  useEffect(() => {
    if (seconds <= 0) {
      fetchNumbers();
      setExpired(true);
      setTimeout(() => {
        refreshNumbers();
      }, 500);
      return;
    }
    const timer = setTimeout(() => setSeconds(seconds - 1), 1000);
    intervalRef.current = timer;
    return () => {
      clearInterval(intervalRef.current!);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [seconds]);

  function refreshNumbers() {
    setPhoneNumbersPayload(undefined);
    setExpired(false);
    setSeconds(timeout);
    fetchNumbers();
  }

  // const props: DuMobileNumberProps = {
  //   incomplete: false,
  //   disableClick: true,
  // };

  function handleNumberSelection(id: string) {
    for (let number of phoneNumbersPayload?.phoneNumber!) {
      if (id === number.id) {
        setCheckedPhoneNumber({ number: number.phoneNumber, id: number.id });
        // dispatch(
        //   phoneNumberSelected({
        //     phoneNumber: number.phoneNumber,
        //   })
        // );
      }
    }
  }

  const headerProps: DuHeaderProps = {
    left: 'back',
    leftPressed: () => {
      Platform.OS === 'android' && StatusBar.setBackgroundColor('white');
      if (Platform.OS === 'android') {
        StatusBar.setBarStyle('dark-content', true);
      }
      if (navigation.canGoBack()) {
        navigation.goBack();
      }
    },
    navToDashBoard: true,
    rightTertiarySupport: {
      pressed: () => gotoDashboardClicked(),
    },
    right: 'tertiary',
    rightTertiary: {
      text: 'Help',
      disable: true,
    },
  };

  const _getTime = (_seconds: number): string => {
    _seconds = Number(_seconds);
    var m = Math.floor((_seconds % 3600) / 60);
    var s = Math.floor((_seconds % 3600) % 60);
    return `${addZero(m, 2)}:${addZero(s, 2)}`;
  };

  async function proceedWithAddExistingOrderItemUnderParent(
    productIdRes: any,
    msisdnId: string,
    msisdn: string
  ) {
    var id = productIdRes.bssListProductOfferings[0].id;
    const addExistingOrderItemUnderParentResponse =
      await addExistingOrderItemUnderParent(
        getSalesOrder().orderItemId!,
        id,
        msisdnId
      );

    if (addExistingOrderItemUnderParentResponse?.status === 'SUCCESS') {
      const simCardData =
        addExistingOrderItemUnderParentResponse?.salesOrder?.orderItems[0].orderItems.find(
          (orderItem: any) => orderItem.offer.name === 'SIM Card'
        );

      getSalesOrder().simCardOrderItemId = simCardData?.orderItemId;

      numberSelectionComplete &&
        numberSelectionComplete(getSalesOrder(), {
          msisdn: msisdn,
          isEsim: params.isEsim,
        });
    }
  }

  const proceedWithSelectedNumber = async () => {
    const releasePhoneNumbersResponse = await releasePhoneNumbers(
      getSalesOrder().customer.id,
      getSalesOrder().salesOrderId,
      phoneNumberState.phoneNumber.id
    );

    if (releasePhoneNumbersResponse?.status === 'SUCCESS') {
      await reservePhoneNumber(
        getSalesOrder().customer.id,
        [checkedPhoneNumber.id],
        getSalesOrder().salesOrderId
      );

      const productIdRes = await getBssListProductOfferings();
      if (productIdRes) {
        if (params.isEsim) {
          const updateOrderItemsResponse = await updateOrderItems(
            getSalesOrder().salesOrderId,
            getSalesOrder().simCardOrderItemId!
          );

          if (updateOrderItemsResponse && productIdRes) {
            await proceedWithAddExistingOrderItemUnderParent(
              productIdRes,
              checkedPhoneNumber.id,
              checkedPhoneNumber.number
            );
          }
        } else {
          await proceedWithAddExistingOrderItemUnderParent(
            productIdRes,
            checkedPhoneNumber.id,
            checkedPhoneNumber.number
          );
        }
      }
    }
  };

  if (!phoneNumbersPayload) {
    return (
      <View
        style={{
          width: '100%',
          height: '100%',
          backgroundColor: 'white',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Text>...</Text>
      </View>
    );
  }

  const RenderUserData = () => {
    return (
      <View style={{ alignItems: 'center', marginTop: 76 }}>
        <View
          style={{ backgroundColor: '#F5F6F7', borderRadius: 200, padding: 22 }}
        >
          <DuIcon iconName="search-off" iconSize={35} />
        </View>

        <DuJumbotron
          mainJumbotron={'We’re sorry, there are no available numbers'}
          alignment={'center'}
          description={'Please try again later!'}
        />
      </View>
    );
  };

  return (
    <View style={styles.root}>
      {Platform.OS === 'ios' ? (
        <View style={styles.iosStackHeaderPadding} />
      ) : (
        <View style={{ marginBottom: safeAreaInsets.top }} />
      )}
      <DuHeader {...headerProps} />
      <ScrollView contentContainerStyle={styles.scrollViewConntainerStyle}>
        <DuJumbotron
          size="xxlarge"
          alignment="center"
          mainJumbotron="Browse more numbers"
        />
        <View style={styles.subContainer}>
          <View style={{ alignItems: 'center' }}>
            {/* TODO: Update toolkit later */}
            {/* <DuMobileNumber
            // digits={`0${params.phoneNumber.substring(3)}`}
            digits={`--${params.phoneNumber}`}
            disabled={expired}
            {...props}
          /> */}
            <Text style={styles.selectedNumber}>
              {mask(
                phoneNumberState?.phoneNumber?.phoneNumber?.length > 0
                  ? `0${phoneNumberState.phoneNumber?.phoneNumber?.substring(
                      3
                    )}`
                  : '',
                '999 99999999'
              )}
            </Text>
          </View>
          <View style={styles.grayBanner}>
            <View
              style={{
                flex: 3,
                paddingRight: 30,
              }}
            >
              {expired ? (
                <Text style={styles.grayBannerTitle}>Numbers have expired</Text>
              ) : (
                <View style={styles.bannerInner}>
                  <DuIcon iconName="warning" iconSize={16} />
                  <View style={styles.bannerDescriptionContainer}>
                    <Text style={styles.grayBannerTitle}>
                      Numbers are locked for you for{' '}
                      <Text style={styles.purple}>{_getTime(seconds)}</Text>
                    </Text>
                    <Text style={styles.grayBannerDecription}>
                      {expired
                        ? 'The numbers have expired, shuffle to get a new set'
                        : 'These numbers are reserved for you until the timer ends'}
                    </Text>
                  </View>
                </View>
              )}
            </View>
            <View
              style={{
                flex: 1,
                alignItems: 'flex-end',
              }}
            >
              <DuButton
                type="teritary"
                title="Shuffle"
                size="small"
                icon={{ iconName: 'refresh' }}
                containerStyle={[styles.shuffleButton]}
                onPress={() => {
                  refreshNumbers();
                }}
              />
            </View>
          </View>
          <View style={styles.listContainer}>
            {phoneNumbersPayload.phoneNumber.length === 0 ? (
              RenderUserData()
            ) : (
              <FlatList
                data={phoneNumbersPayload.phoneNumber}
                renderItem={({ item }) => {
                  return (
                    <DuListItemStatic
                      disabled={expired}
                      key={item.id}
                      title={mask(
                        `0${item?.phoneNumber?.substring(3)}`,
                        '999 999 99999'
                      )}
                      bottomDivider={true}
                      onCheckListValueChange={() => {
                        handleNumberSelection(item.id);
                      }}
                      checkListValue={
                        checkedPhoneNumber.number === item.phoneNumber
                          ? true
                          : false
                      }
                      enableCheckList={!expired}
                      checkListType={'radio'}
                    />
                  );
                }}
              />
            )}
          </View>
        </View>
      </ScrollView>
      <DuButtonsDock
        shadow
        items={[
          <DuButton
            title={
              phoneNumbersPayload.phoneNumber.length === 0
                ? 'Try again'
                : 'Confirm and proceed'
            }
            type="primary"
            onPress={() => {
              if (checkedPhoneNumber.number.length > 0) {
                proceedWithSelectedNumber();
              } else {
                fetchNumbers();
              }
            }}
            disabled={
              phoneNumbersPayload.phoneNumber.length === 0
                ? false
                : expired || checkedPhoneNumber.number.length === 0
            }
          />,
        ]}
      />
    </View>
  );
};

export default BrowseMoreNumberScreen;

const styles = StyleSheet.create({
  root: { flex: 1, backgroundColor: 'white' },
  subContainer: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    marginTop: 24,
  },
  iosStackHeaderPadding: { marginBottom: 14 },
  grayBanner: {
    marginTop: 24,
    padding: 16,
    backgroundColor: '#F5F6F7',
    borderRadius: 12,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 16,
    maxWidth: 869,
  },
  bannerInner: {
    flexDirection: 'row',
  },
  bannerDescriptionContainer: {
    marginLeft: 16,
  },
  grayBannerTitle: {
    fontFamily: 'Moderat-Bold',
    fontSize: 16,
    textAlign: 'left',
    letterSpacing: -0.2,
    lineHeight: 20,
    color: '#041333',
  },
  grayBannerDecription: {
    marginTop: 8,
    fontFamily: 'Moderat-Regular',
    fontSize: 14,
    textAlign: 'left',
    lineHeight: 20,
    color: '#5E687D',
  },
  purple: { color: '#753BBD' },
  numberAndButtonContainer: {
    marginTop: 24,
    padding: 16,
    borderColor: '#D7D9DE',
    borderWidth: 1,
    borderRadius: 12,
  },
  badgeAndButtonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 24,
    alignItems: 'center',
  },
  shuffleButton: { paddingHorizontal: 0 },
  numberContainer: { marginTop: 8 },
  number: {
    fontFamily: 'Moderat-Bold',
    fontSize: 22,
    textAlign: 'left',
    letterSpacing: -0.3,
    lineHeight: 26,
    color: '#041333',
  },
  browsMoreButton: {
    marginTop: 32,
    flexDirection: 'row',
  },
  overlay: {
    paddingHorizontal: 16,
    backgroundColor: 'transparent',
    elevation: 0,
  },
  listContainer: {
    width: '100%',
    maxWidth: 869,
    marginTop: 24,
    marginBottom: 50,
  },
  selectedNumber: {
    fontSize: 40,
    marginVertical: 30,
    fontFamily: 'Moderat-Medium',
    color: '#000',
  },
  scrollViewConntainerStyle: {
    flexGrow: 1,
  },
});
