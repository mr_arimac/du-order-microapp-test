import {
  DuButton,
  DuHeader,
  DuHeaderProps,
  DuJumbotron,
  DuTextInput,
} from '@du-greenfield/du-ui-toolkit';
import React, { FC, useState } from 'react';
import { Platform, ScrollView, StyleSheet, View } from 'react-native';
import Modal from 'react-native-modal';

export type EnterICCIDManuallyModalProps = {
  isVisible?: boolean;
  onPressClose?: () => void;
  verifyAndContinue?: (iccid: any) => void;
};

const EnterICCIDManuallyModal: FC<EnterICCIDManuallyModalProps> = ({
  isVisible,
  onPressClose,
  verifyAndContinue,
}) => {
  const [ICCID, setICCID] = useState('');
  const headerProps: DuHeaderProps = {
    left: 'back',
    leftPressed: onPressClose,
    statusBar:
      Platform.OS === 'ios'
        ? { barStyle: 'default', backgroundColor: 'white' }
        : { barStyle: 'dark-content', backgroundColor: 'white' },
    background: 'white',
    handler: true,
  };

  return (
    <View style={styles.centeredView}>
      <Modal
        isVisible={isVisible}
        backdropColor={'rgba(4, 19, 51, 1)'}
        propagateSwipe={true}
        onSwipeComplete={onPressClose}
        swipeDirection="down"
        onBackdropPress={onPressClose}
        onBackButtonPress={onPressClose}
      >
        <View style={styles.modalMainView}>
          <DuHeader {...headerProps} />
          <ScrollView contentContainerStyle={styles.scrollViewConntainerStyle}>
            <DuJumbotron
              alignment={'center'}
              mainJumbotron={'Enter ICCID manually'}
              description={'Please fill in the details below'}
              size={'large'}
            />
            <View style={styles.inputText}>
              <DuTextInput
                title="ICCID number"
                onChangeText={(text) => {
                  setICCID(text);
                }}
              />
            </View>
            <View style={styles.verifyAndContinueButtonContainer}>
              <DuButton
                title="Verify and continue"
                onPress={() => verifyAndContinue && verifyAndContinue(ICCID)}
                containerStyle={styles.dockedButton}
              />
            </View>
          </ScrollView>
        </View>
      </Modal>
    </View>
  );
};
const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalMainView: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: 12,
    paddingTop: 4,
    marginVertical: 30,
    marginHorizontal: 50,
    height: '100%',
    overflow: 'hidden',
  },
  dockedButton: {
    width: '100%',
  },
  inputText: { paddingHorizontal: 90, marginTop: 32 },
  scrollViewConntainerStyle: { flexGrow: 1 },
  verifyAndContinueButtonContainer: {
    paddingHorizontal: 28,
    paddingVertical: 32,
    width: '100%',
    height: '100%',
    flex: 1,
    flexGrow: 1,
    justifyContent: 'flex-end',
  },
});

export default EnterICCIDManuallyModal;
