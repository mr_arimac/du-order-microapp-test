import type { NativeStackScreenProps } from '@react-navigation/native-stack';
import { DuButton, DuHeader, DuJumbotron } from '@du-greenfield/du-ui-toolkit';
import React, { FC, useState } from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { getSalesOrder } from '../../../';
import type { DealerNavigatorParamList } from '../../../navigator';
import EnterICCIDManuallyModal from './EnterICCIDManuallyModal';
import { mask } from 'react-native-mask-text';

export type DealerSimActivationScreenProps = NativeStackScreenProps<
  DealerNavigatorParamList,
  'SimActivationScanScreen'
> & {
  onBarcodeButtonTap: () => void;
  gotoDashboardClicked: () => void;
};

const SimActivationScanScreen: FC<DealerSimActivationScreenProps> = ({
  navigation,
  onBarcodeButtonTap,
  gotoDashboardClicked,
}) => {
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);

  const takeAPhotoButton = () => {
    return (
      <View style={styles.takeAPhotoButtonContainer}>
        <DuButton
          title="Take a photo of the SIM barcode"
          type="primary"
          onPress={onBarcodeButtonTap}
        />
      </View>
    );
  };

  return (
    <View style={styles.root}>
      <DuHeader
        safeArea={true}
        left="back"
        leftPressed={() => {
          if (navigation.canGoBack()) {
            navigation.goBack();
          }
        }}
        navToDashBoard={true}
        rightTertiarySupport={{
          pressed: () => gotoDashboardClicked(),
        }}
        right={'tertiary'}
        rightTertiary={{
          disable: true,
          text: 'Help',
        }}
        statusBar={{
          backgroundColor: 'white',
          barStyle: 'dark-content',
        }}
        background="white"
      />
      <View style={[styles.subContainer]}>
        <DuJumbotron mainJumbotron="SIM barcode" />
        <View style={styles.middleContainer}>
          <DuJumbotron
            mainJumbotron={
              getSalesOrder().msisdn?.msisdn
                ? mask(
                    `0${getSalesOrder().msisdn?.msisdn?.substring(3)}`,
                    '999 999 99999'
                  )
                : ''
            }
            overLine="SIM 1"
            alignment="center"
          />
          <View style={styles.simCardPlaceholderContainer}>
            <Image source={require('./sim-card-placeholder.png')} />
            <View style={styles.simCardOverlayContainer}>
              <View>
                <Image source={require('./du-logo.png')} />
              </View>
              <View style={styles.simCardOverlayContentContainer}>
                <View>
                  <Text style={styles.iccidNumberTitle}>ICCID Number</Text>
                </View>
                <View style={styles.iccidNumberContainer}>
                  <Text style={styles.iccidNumber}>N/A</Text>
                </View>
                <View style={styles.barcodeContainer}>
                  <Image source={require('./barcode.png')} />
                </View>
                <View style={styles.barcodeNumberContainer}>
                  <Text style={styles.barcodeNumber}>1248326122483261</Text>
                </View>
              </View>
            </View>
          </View>
          {/* {banner()} */}
          {takeAPhotoButton()}
          {/* {enterManualyButton()} */}
        </View>
        <EnterICCIDManuallyModal
          isVisible={isModalVisible}
          onPressClose={() => {
            setIsModalVisible(false);
          }}
          verifyAndContinue={(value) => {
            setIsModalVisible(false);
            navigation.push('ActivatingSIMScreen', {
              scannedBarcode: value,
            } as never);
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  subContainer: {
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 105,
    paddingVertical: 32,
  },
  middleContainer: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  simCardPlaceholderContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  simCardOverlayContainer: {
    position: 'absolute',
    width: 300,
    height: 180,
  },
  simCardOverlayContentContainer: {
    width: 180,
    marginTop: 17,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iccidNumberTitle: {
    fontFamily: 'Moderat-Medium',
    fontSize: 11,
    textAlign: 'center',
    color: '#5E687D',
    lineHeight: 20,
  },
  iccidNumberContainer: { marginTop: 8 },
  iccidNumber: {
    fontFamily: 'Moderat-Medium',
    fontSize: 18,
    textAlign: 'center',
    color: '#041333',
    lineHeight: 24,
    letterSpacing: -0.15,
  },
  barcodeContainer: { marginTop: 24 },
  barcodeNumberContainer: { marginTop: 7 },
  barcodeNumber: {
    fontFamily: 'Moderat-Regular',
    fontSize: 11,
    textAlign: 'center',
    color: '#233659',
    lineHeight: 20,
  },
  enterManuallyButtonContainer: { width: 340, marginTop: 16 },
  takeAPhotoButtonContainer: { width: 340 },
});

export default SimActivationScanScreen;
