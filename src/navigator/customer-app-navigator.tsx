import type { Msisdn, Order } from '@du-greenfield/du-commons';
import type { MicroAppsProps } from '@du-greenfield/du-microapps-core';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import BrowseMoreNumberScreen from '../screens/Customer/BrowseMoreNumbers/BrowseMoreNumberScreen';
import YourAssignedNumberScreen from '../screens/Customer/YourAssignedNumber/YourAssignedNumberScreen';

export type CustomerNavigatorParamList = {
  YourAssignedNumberScreen: undefined;
  BrowseMoreNumberScreen: undefined;
};

export enum CustomerInitialScreen {
  YOUR_ASSIGNED_NUMBER = 'YourAssignedNumberScreen',
  BROWSE_MORE_NUMBERS = 'BrowseMoreNumberScreen',
}

const Stack = createNativeStackNavigator<CustomerNavigatorParamList>();

type CustomerAppStackProps = MicroAppsProps & {
  onBrowseMoreTap: (isEsim: boolean | undefined) => void;
  numberSelectionComplete: (order: Order, selectedMsisdn: Msisdn) => void;
  initialScreen?: CustomerInitialScreen;
  params: any;
};

export const CustomerAppStack = (stackProps: CustomerAppStackProps) => {
  return (
    <Stack.Navigator
      screenListeners={{
        state: (e) => {
          stackProps.globalEventListener &&
            stackProps.globalEventListener('screen_state_changes', e);
        },
      }}
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName={stackProps.initialScreen}
    >
      <Stack.Screen name="YourAssignedNumberScreen">
        {(screenProps) => (
          <YourAssignedNumberScreen {...stackProps} {...screenProps} />
        )}
      </Stack.Screen>
      <Stack.Group screenOptions={{ presentation: 'modal' }}>
        <Stack.Screen name="BrowseMoreNumberScreen">
          {(screenProps) => (
            <BrowseMoreNumberScreen {...stackProps} {...screenProps} />
          )}
        </Stack.Screen>
      </Stack.Group>
    </Stack.Navigator>
  );
};
