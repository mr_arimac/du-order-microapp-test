import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';
import type { Msisdn, Order } from '@du-greenfield/du-commons';
import BrowseMoreNumberScreen from '../screens/Dealer/BrowseMoreNumbers/BrowseMoreNumberScreen';
import ActivatingSIMScreen from '../screens/Dealer/ActivatingSIM/ActivatingSIMScreen';
import SimActivationScanScreen from '../screens/Dealer/SimActivationScan/SimActivationScanScreen';
import YourAssignedNumberScreen from '../screens/Dealer/YourAssignedNumber/YourAssignedNumberScreen';
import BarcodeScannerScreen from '../screens/Dealer/BarcodeScanner/BarcodeScannerScreen';
import SimCapturedScreen from '../screens/Dealer/SimCapruredScreen/SimCapturedScreen';
import type { MicroAppsProps } from '@du-greenfield/du-microapps-core';

export type DealerNavigatorParamList = {
  YourAssignedNumberScreen: undefined;
  BrowseMoreNumberScreen: undefined;
  SimActivationScanScreen: undefined;
  ActivatingSIMScreen: undefined;
  BarcodeScannerScreen: undefined;
  SimCapturedScreen: undefined;
};

export enum DuOrderDealerInitialScreen {
  YOUR_ASSIGNED_NUMBER = 'YourAssignedNumberScreen',
  BROWSE_MORE_NUMBERS = 'BrowseMoreNumberScreen',
  SIM_ACTIVATION_SCAN = 'SimActivationScanScreen',
  BARCODE_SCANNER = 'BarcodeScannerScreen',
  ACTIVATING_SIM = 'ActivatingSIMScreen',
  SIM_CAPTURED = 'SimCapturedScreen',
}

const Stack = createNativeStackNavigator<DealerNavigatorParamList>();

type DealerAppStackProps = MicroAppsProps & {
  onBrowseMoreTap: (isEsim: boolean | undefined) => void;
  numberSelectionComplete: (order: Order, selectedMsisdn: Msisdn) => void;
  initalScreen?: DuOrderDealerInitialScreen;
  onBarcodeButtonTap: () => void;
  gotoDashboardClicked: () => void;
  onBarcodeScanned: (barcode: string, simScanned: boolean) => void;
  onRescanTap: () => void;
  onSimActivateTap: (barcode: string | undefined) => void;
  onSimActivateSuccess: (value: any) => void;
  customerId?: string;
  params: any;
};

export const DealerAppStack = (props: DealerAppStackProps) => {
  return (
    <Stack.Navigator
      screenListeners={{
        state: (e) => {
          props.globalEventListener &&
            props.globalEventListener('screen_state_changes', e);
        },
      }}
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName={props.initalScreen}
    >
      <Stack.Screen name="YourAssignedNumberScreen">
        {(screenProps) => (
          <YourAssignedNumberScreen {...props} {...screenProps} />
        )}
      </Stack.Screen>
      <Stack.Screen name="BrowseMoreNumberScreen">
        {(screenProps) => (
          <BrowseMoreNumberScreen {...props} {...screenProps} />
        )}
      </Stack.Screen>
      <Stack.Screen name="SimActivationScanScreen">
        {(screenProps) => (
          <SimActivationScanScreen {...props} {...screenProps} />
        )}
      </Stack.Screen>
      <Stack.Screen name="ActivatingSIMScreen">
        {(screenProps) => (
          <ActivatingSIMScreen
            {...props}
            {...screenProps}
            cancel={() => {
              //do nothing
            }}
          />
        )}
      </Stack.Screen>
      <Stack.Screen name="BarcodeScannerScreen">
        {(screenProps) => <BarcodeScannerScreen {...props} {...screenProps} />}
      </Stack.Screen>
      <Stack.Screen name="SimCapturedScreen">
        {(screenProps) => <SimCapturedScreen {...props} {...screenProps} />}
      </Stack.Screen>
    </Stack.Navigator>
  );
};
