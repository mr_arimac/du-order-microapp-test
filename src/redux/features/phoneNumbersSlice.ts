import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  phoneNumber: '',
  phoneNumbers: [],
};

const phoneNumberSlice = createSlice({
  name: 'phoneNumbers',
  initialState,
  reducers: {
    phoneNumberSelected: (state, action) => {
      state.phoneNumber = action.payload;
    },
  },
});

export const { phoneNumberSelected } = phoneNumberSlice.actions;

export default phoneNumberSlice.reducer;
