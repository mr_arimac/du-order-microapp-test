import { duGet } from '@du-greenfield/du-rest';
import type { OrderScreenDynamics } from '../../models';

class Eligibility {
  async getScreenDynamics(): Promise<OrderScreenDynamics | undefined> {
    console.log('Eligibility getScreenDynamics ======================>');
    return new Promise((resolve) => {
      duGet(
        'https://firestore.googleapis.com/v1/projects/gf-du-customer-mobile-app-dev/databases/(default)/documents/microapp/005'
      )
        .then((value) => {
          return resolve(value.data as OrderScreenDynamics);
        })
        .catch((e) => {
          console.log(e);
          return resolve(undefined);
        });
    });
  }
}

const screen = new Eligibility();
export default screen;
