// Common

export interface StringMapValue {
  mapValue: LanguageMapValueFields;
}
export interface IntegerValue {
  integerValue: number;
}
export interface LanguageMapValueFields {
  fields: LanguageKey;
}
export interface LanguageKey {
  ar: Language;
  en: Language;
}
export interface Language {
  mapValue: PlatformMapValueFields;
}

export interface PlatformMapValueFields {
  fields: PlatformKey;
}

export interface PlatformKey {
  rn: StringValue;
  plain_text: StringValue;
  html: StringValue;
}
export interface StringValue {
  stringValue: string;
}

export interface OrderScreenDynamics {}
export interface OrderScreenDynamics {
  name: string;
  fields: OrderScreenDynamicsFields;
  createTime: Date;
  updateTime: Date;
}

export interface OrderScreenDynamicsFields {
  assigned_number_model: AssignedNumberModel;
  step_text: Description;
  lines: FluffyLines;
  heading: Description;
  name: Name;
  description: Description;
  associate_billing_account: booleanValue;
  barcode_scanner_timer: StringValue;
  billing_account_field: StringValue;
  in_store_delivery: booleanValue;
}

export interface AssignedNumberModel {
  mapValue: AssignedNumberModelMapValue;
}

export interface AssignedNumberModelMapValue {
  fields: PurpleFields;
}

export interface PurpleFields {
  lines: PurpleLines;
  heading: Description;
  topics: Topics;
}

export interface Description {
  mapValue: DescriptionMapValue;
}

export interface DescriptionMapValue {
  fields: FluffyFields;
}

export interface FluffyFields {
  ar: PurpleAr;
  en: PurpleAr;
}

export interface PurpleAr {
  mapValue: ArMapValue;
}

export interface ArMapValue {
  fields: TentacledFields;
}

export interface TentacledFields {
  html?: Name;
  rn: Name;
  plain_text?: Name;
  plain_test?: Name;
  hrml?: Name;
}

export interface Name {
  stringValue: string;
}

export interface PurpleLines {
  arrayValue: PurpleArrayValue;
}

export interface PurpleArrayValue {
  values: PurpleValue[];
}

export interface PurpleValue {
  mapValue: PurpleMapValue;
}

export interface PurpleMapValue {
  fields: StickyFields;
}

export interface StickyFields {
  shuffle: Description;
  label: Description;
}

export interface Topics {
  mapValue: TopicsMapValue;
}

export interface TopicsMapValue {
  fields: IndigoFields;
}

export interface IndigoFields {
  expired: Expired;
  active: Active;
}

export interface Active {
  mapValue: ActiveMapValue;
}

export interface ActiveMapValue {
  fields: IndecentFields;
}

export interface IndecentFields {
  description: Description;
  heading: Description;
}

export interface Expired {
  mapValue: ExpiredMapValue;
}

export interface ExpiredMapValue {
  fields: HilariousFields;
}

export interface HilariousFields {
  heading: Description;
  description: Description;
  shuffle: Description;
}

export interface FluffyLines {
  arrayValue: FluffyArrayValue;
}

export interface FluffyArrayValue {
  values: FluffyValue[];
}

export interface FluffyValue {
  mapValue: FluffyMapValue;
}

export interface FluffyMapValue {
  fields: AmbitiousFields;
}

export interface AmbitiousFields {
  line_rate: Description;
  status: Status;
  number_of_line: NumberOfLine;
}

export interface NumberOfLine {
  mapValue: NumberOfLineMapValue;
}

export interface NumberOfLineMapValue {
  fields: CunningFields;
}

export interface CunningFields {
  en: FluffyAr;
  ar: FluffyAr;
}

export interface FluffyAr {
  integerValue: string;
}

export interface Status {
  booleanValue: boolean;
}

export interface booleanValue {
  booleanValue: boolean;
}
