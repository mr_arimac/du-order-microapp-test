import { AxiosRequestConfig, duRequest } from '@du-greenfield/du-rest';

export async function request<T>(
  config: AxiosRequestConfig
): Promise<T | undefined> {
  return new Promise((resolve) => {
    duRequest(config)
      .then((value) => {
        console.log('data', value.data);
        return resolve(value.data);
      })
      .catch((e) => {
        console.log(e);
        return resolve(undefined);
      });
  });
}
