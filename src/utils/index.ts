export * from './gql';
export * from './storage';
export function addZero(elemet: number, size: number): string {
  var s = String(elemet);
  while (s.length < (size || 2)) {
    s = '0' + s;
  }
  return s;
}
