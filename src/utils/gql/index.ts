import { gqlClient } from './gql_client';
import type {
  ExistingOrderItemResponse,
  LockPhoneNumber,
  ReleasePhoneNumbersResponcePayload,
} from './models';
import {
  ADD_EXISTING_ORDER_ITEM,
  ADD_EXISTING_ORDER_ITEM_UNDER_PARENT,
  LOCK_PHONE_NUMBER,
  RELEASE_PHONE_NUMBERS,
  RESERVE_PHONE_NUMBER,
  SUBMIT_SALES_ORDER,
  UPDATE_ORDER_ITEM,
  UPDATE_SALES_ORDER,
} from './mutations';
import type { DocumentNode } from 'graphql';
import { BSS_LIST_PRODUCT_OFFERINGS, GET_SALSE_ORDER } from './queries';

export async function lockPhoneNumber(
  customerId: string,
  qty: number,
  excludeNumberIds: string[]
): Promise<LockPhoneNumber | undefined> {
  try {
    const response = await gqlClient().mutation({
      gql: LOCK_PHONE_NUMBER,
      variables: {
        input: {
          quantity: qty,
          customerId: customerId,
          excludeNumberIds: excludeNumberIds,
        },
      },
    });
    console.log('res------111', response);
    return Promise.resolve(response.jsonBody.data?.lockPhoneNumber);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}

export async function reservePhoneNumber(
  customerId: string,
  reserveNumberIds: string[],
  contextId: string
): Promise<any | undefined> {
  try {
    const response = await gqlClient().mutation({
      gql: RESERVE_PHONE_NUMBER,
      variables: {
        input: {
          unlockForContext: true,
          customerId: customerId,
          reserveNumberIds: reserveNumberIds,
          contextId: contextId,
        },
      },
    });
    return Promise.resolve(response.jsonBody.data?.reservePhoneNumber);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}

export async function releasePhoneNumbers(
  customerId: string,
  salesOrderId: string,
  releasePhoneNumberId: string
): Promise<ReleasePhoneNumbersResponcePayload | undefined> {
  try {
    const response = await gqlClient().mutation({
      gql: RELEASE_PHONE_NUMBERS,
      variables: {
        input: {
          customerId: customerId,
          releaseForContext: true,
          releaseForCustomer: true,
          contextId: salesOrderId,
          releaseNumberIds: [releasePhoneNumberId],
        },
      },
    });
    return Promise.resolve(response.jsonBody.data?.releasePhoneNumbers);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}

export async function addExistingOrderItem(
  parentSalesOrderId: string,
  locationId: string,
  productOfferingId: string
): Promise<any | undefined> {
  try {
    const response = await gqlClient().mutation({
      gql: ADD_EXISTING_ORDER_ITEM,
      variables: {
        input: {
          parentSalesOrderId: parentSalesOrderId,
          locationId: locationId,
          productOffering: {
            id: productOfferingId,
            characteristics: [],
          },
        },
      },
    });
    return Promise.resolve(response.jsonBody.data);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}

export async function addExistingOrderItemUnderParent(
  orderItemId: string,
  productOfferingId: string,
  value: string
): Promise<ExistingOrderItemResponse | undefined> {
  try {
    const response = await gqlClient().mutation({
      gql: ADD_EXISTING_ORDER_ITEM_UNDER_PARENT,
      variables: {
        input: {
          orderItemId: orderItemId,
          productOffering: {
            id: productOfferingId,
            characteristics: {
              name: 'MSISDN',
              value: value,
            },
          },
        },
      },
    });
    return Promise.resolve(response.jsonBody.data?.addOrderItemUnderParent);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}

export async function getSalesOrder(
  id: string,
  reloadShopCart: boolean
): Promise<any | undefined> {
  try {
    const response = await gqlClient().query({
      gql: GET_SALSE_ORDER,
      variables: {
        input: {
          id: id,
          reloadShopCart: reloadShopCart,
        },
      },
    });
    return Promise.resolve(response.jsonBody.data?.bssGetSalesOrder);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}

export async function updateOrderItems(
  salesOrderId: string,
  simCardOrderId: string
): Promise<any | undefined> {
  try {
    const response = await gqlClient().query({
      gql: UPDATE_SALES_ORDER,
      variables: {
        input: {
          salesOrderId: salesOrderId,
          itemCharacteristics: {
            orderItemId: simCardOrderId,
            characteristics: {
              name: 'SIM card type',
              value: 'eSIM',
            },
          },
        },
      },
    });
    return Promise.resolve(response.jsonBody.data?.updateOrderItems);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}

export async function getBssListProductOfferings(): Promise<any | undefined> {
  try {
    const response = await gqlClient().query({
      gql: BSS_LIST_PRODUCT_OFFERINGS,
      variables: {},
    });
    return Promise.resolve(response.jsonBody.data);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}

export async function updateOrderItem(
  salesOrderId: string,
  orderItemId: string | undefined,
  msisdn: string
): Promise<any | undefined> {
  try {
    const response = await gqlClient().mutation({
      gql: UPDATE_ORDER_ITEM,
      variables: {
        input: {
          salesOrderId: salesOrderId,
          itemCharacteristics: {
            orderItemId: orderItemId,
            characteristics: {
              name: 'ICCID',
              value: msisdn,
            },
          },
        },
      },
    });
    return Promise.resolve(response.jsonBody.data);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}

export async function submitSalesOrder(
  input: {
    salesOrderId: string;
    [key: string]: string;
  },
  gql: DocumentNode = SUBMIT_SALES_ORDER
): Promise<any | undefined> {
  try {
    const response = await gqlClient().mutation({
      gql: gql,
      variables: {
        input: input,
      },
    });
    return Promise.resolve(response.jsonBody.data);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}

export async function getCustomer(id: string, gql: DocumentNode): Promise<any> {
  try {
    const response = await gqlClient().mutation({
      gql: gql,
      variables: {
        id: id,
      },
    });
    return Promise.resolve(response.jsonBody.data.bssGetCustomer);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}

export async function getListProductOffering(gql: DocumentNode): Promise<any> {
  try {
    const response = await gqlClient().mutation({
      gql: gql,
    });
    return Promise.resolve(response);
  } catch (error) {
    return Promise.resolve(undefined);
  }
}
