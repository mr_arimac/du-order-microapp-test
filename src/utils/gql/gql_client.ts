import { getMicroAppContext } from '../..';
import type { GraphQLClient } from '@du-greenfield/du-microapps-core';

export function gqlClient(): GraphQLClient {
  return getMicroAppContext().graphQLClinet();
}
