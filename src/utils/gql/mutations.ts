import gql from 'graphql-tag';

export const LOCK_PHONE_NUMBER = gql`
  mutation LockPhoneNumber($input: PhoneNumberLockInput!) {
    lockPhoneNumber(input: $input) {
      phoneNumber {
        id
        phoneNumber
        reservationDate
        status
        reservationPeriod
        lockDate
      }
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;

export const RELEASE_PHONE_NUMBERS = gql`
  mutation releasePhoneNumbers($input: PhoneNumbersReleaseInput!) {
    releasePhoneNumbers(input: $input) {
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;

export const RESERVE_PHONE_NUMBER = gql`
  mutation ReservePhoneNumber($input: PhoneNumberReservationInput!) {
    reservePhoneNumber(input: $input) {
      phoneNumber {
        id
        phoneNumber
        reservationDate
        status
        reservationPeriod
      }
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;

export const ADD_EXISTING_ORDER_ITEM = gql`
  mutation addExistingOrderItem($input: ExistingOrderItemInput!) {
    addExistingOrderItem(input: $input) {
      salesOrder {
        salesOrderId
      }
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;

export const ADD_EXISTING_ORDER_ITEM_UNDER_PARENT = gql`
  mutation addOrderItemUnderParent($input: OrderItemUnderParentInput!) {
    addOrderItemUnderParent(input: $input) {
      salesOrder {
        salesOrderId
        orderItems {
          orderItemId
          offer {
            isRoot
          }
          orderItems {
            orderItemId
            offer {
              name
            }
          }
        }
      }
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;

//change
export const UPDATE_ORDER_ITEM = gql`
  mutation updateOrderItems($input: OrderItemInput!) {
    updateOrderItems(input: $input) {
      salesOrder {
        salesOrderId
      }
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;

export const SUBMIT_SALES_ORDER = gql`
  mutation submitSalesOrder($input: SubmitSalesOrderInput!) {
    submitSalesOrder(input: $input) {
      salesOrder {
        salesOrderId
      }
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;

export const SUBMIT_SALES_ORDER_WITH_BILLING = (fieldName: string) => gql`
  mutation submitSalesOrder($input: SubmitSalesOrderInput!) {
    submitSalesOrder(input: $input) {
      salesOrder {
        salesOrderId
        ${fieldName}
      }
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;

export const UPDATE_SALES_ORDER = gql`
  mutation updateOrderItems($input: OrderItemInput!) {
    updateOrderItems(input: $input) {
      salesOrder {
        salesOrderId
      }
      status
      errorCodes {
        code
        type
        message
      }
    }
  }
`;
