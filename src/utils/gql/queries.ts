import gql from 'graphql-tag';

export const ELIGIBILITY_CHECK = gql`
  query EligibilityCheck($input: EligibilityCheckInput!) {
    eligibilityCheck(input: $input) {
      errorCodes {
        errorCode
        errormessage
      }
      failureReason
      FMSPayload {
        systemDecision
      }
      requestedDate
      retrievedDate
      simCapPayload {
        linesAtDU
        linesAtGF
        remainingLines
        totalActiveLines
      }
      status
    }
  }
`;

export const GET_SALSE_ORDER = gql`
  query BssGetSalesOrder($input: BssGetSalesOrderInput!) {
    bssGetSalesOrder(input: $input) {
      orderItems {
        id
        chars
      }
      errorCodes {
        errorCode
        errormessage
      }
      status
    }
  }
`;

export const BSS_LIST_PRODUCT_OFFERINGS = gql`
  query bssListProductOfferings {
    bssListProductOfferings
      @filter(filters: ["isRoot=false", "name.eq=Phone number"]) {
      id
    }
  }
`;

export const BSS_GET_CUSTOMER = gql`
  query bssGetCustomer($id: String!) {
    bssGetCustomer(id: $id) {
      billingAccounts {
        id
      }
    }
  }
`;

export const BSS_LIST_PRODUCT_OFFERING = gql`
  query bssListProductOfferings {
    bssListProductOfferings
      @filter(filters: ["isRoot=true", "name.eq=In Store"]) {
      id
    }
  }
`;
