import { NavigationContainer } from '@react-navigation/native';
import type {
  MicroAppContext,
  MicroAppsProps,
} from '@du-greenfield/du-microapps-core';
import React from 'react';
import {
  CustomerAppStack,
  CustomerInitialScreen,
  DealerAppStack,
  DuOrderDealerInitialScreen,
} from './navigator';
import type { PhoneNumber } from './utils/gql/models';
import type { Msisdn, Order } from '@du-greenfield/du-commons';
import { Provider } from 'react-redux';
import store from './redux/store';
export { DuOrderDealerInitialScreen, CustomerInitialScreen, PhoneNumber };

export let isDealer = false;

export type DuOrderProps = MicroAppsProps & {
  salesOrder: Order;
  onBrowseMoreTap: (isEsim: boolean | undefined) => void;
  numberSelectionComplete: (order: Order, selectedMsisdn: Msisdn) => void;
  dealerInitialScreen?: DuOrderDealerInitialScreen;
  customerInitialScreen?: CustomerInitialScreen;
  onBarcodeButtonTap: () => void;
  gotoDashboardClicked: () => void;
  onBarcodeScanned: (barcode: string, simScanned: boolean) => void;
  onRescanTap: () => void;
  onSimActivateTap: (barcode: string | undefined) => void;
  onSimActivateSuccess: (value: any) => void;
  params: any;
};

var microAppContext: MicroAppContext;

export function getMicroAppContext() {
  return microAppContext;
}

var salesOrder: Order;

export function getSalesOrder() {
  return salesOrder;
}

const DuOrder = (props: DuOrderProps) => {
  microAppContext = props.appContext;
  salesOrder = props.salesOrder;

  const App = () => {
    if (microAppContext.appType == 'dealer') {
      return (
        <>
          {!props.navContainer ? (
            <DealerAppStack
              {...props}
              initalScreen={props.dealerInitialScreen}
            />
          ) : (
            <NavigationContainer independent={true} {...props}>
              <DealerAppStack
                {...props}
                initalScreen={props.dealerInitialScreen}
              />
            </NavigationContainer>
          )}
        </>
      );
    } else {
      return (
        <>
          {!props.navContainer ? (
            <CustomerAppStack
              {...props}
              initialScreen={props.customerInitialScreen}
            />
          ) : (
            <NavigationContainer independent={true} {...props}>
              <CustomerAppStack
                {...props}
                initialScreen={props.customerInitialScreen}
              />
            </NavigationContainer>
          )}
        </>
      );
    }
  };
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};

export default DuOrder;
